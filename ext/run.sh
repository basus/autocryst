#!/bin/bash

export opt_miniconda=/opt/pxsoft/pybes/vdefault/linux-x86_64/miniconda3
export PYTHONPATH="$HOME/scripts/edna2:$opt_miniconda/lib:$opt_miniconda/lib/python3.7/site-packages"
export EDNA2_SITE="ESRF_ID30A2"

$opt_miniconda/bin/python $HOME/scripts/edna2/edna2/lib/autocryst/src/run_crystfel.py \
	--image_directory "/data/visitor/mx2364/id23eh2/20211122/RAW_DATA/pbp2a/pbp2a-PBP-9/MeshScan_01" \
	--detectorType "pilatus" --prefix "mesh" --suffix "cbf" \
	--maxchunksize 100 --indexing_method=xgandalf-latt-nocell,mosflm,asdf,xds \
        --highres "3.0" --int_radius "2,4,6" --peak_radius "2,3,5" \
	--min_peaks "30" --min_snr "4.0" --unit_cell_file "/data/visitor/mx2364/id23eh2/20211122/shibom/3ZG0.pdb"
     
