#
# Copyright (c) European Molecular Biology Laboratory (EMBL)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = ['S. Basu']
__license__ = 'MIT'
__date__ = '2020/05/10'

import pathlib
import json
import jsonschema
import logging
import random

from deap import base
from deap import creator
from deap import tools
from deap import algorithms

from autocryst.run_crystfel import AutoCrystFEL

logger = logging.getLogger('autoCryst')


class GAScale(AutoCrystFEL):
    def __init__(self, jData):
        super(GAScale, self).__init__(jData)
        self.indata_dict = dict()

    @staticmethod
    def getInDataSchema():
        return {
            "type": "object",
            "required": ["stream_directory", "point_group"],
            "properties": {
                "stream_directory": "string",
                "point_group": "string",
                "nproc": "string",
                "ngroups": "integer",
                "generations": "integer",
                "populations": "integer",
                "mutation_prob": "float",
                "crossover_prob": "float",
                "completeness_weight": "float",
                "Isigma_weight": "integer",
                "CChalf_weight": "integer",
                "anom_weight": "integer"
            }
        }

    @staticmethod
    def getOutDataSchema():
        return {
            "type": "object",
            "properties": {
                "GA_score": "string",
                "hall_of_fame": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "final_dataset": "string"
            }
        }

    def datafinder(self):
        streamdir = pathlib.Path(self.jshandle['stream_directory'])
        ngroups = self.jshandle.get('ngroups', 4)
        if streamdir.exists():
            listofstreamfiles = list(streamdir.glob('*.stream'))
            # sizeofgroup = len(listofstreamfiles) // ngroups
            # group_dict = dict()
            # groups = [listofstreamfiles[x: x+sizeofgroup] for x in range(ngroups)]
            for fname in listofstreamfiles:
                self.indata_dict[fname] = random.randint(1, ngroups)
                # self.listofgroupedData.append(group_dict)
        else:
            self.setFailure()
            logger.error('dataError:{}'.format('no data found'))
        return

    def stat_complete_set(self):
        for k, v in self.indata_dict.items():
            self.run_as_command('cat k >> allcomb.stream')
        streampath = self.getOutputDirectory() / 'allcomb.stream'
        stat_all = self.report_stats(streampath)
        self.write_cell_file(stat_all)
        stat_all.update(self.scale_merge(streampath))
        return stat_all

    def gasetup(self):
        # set up GA individuals, chromosomes, populations..

        '''
        If there are 300 stream files, which will be split into 4 groups, then each individual in GA will get 300/4 = 75
        datapoints. The population is of size 20. It means that there will be 20 individuals, each containing 75 datapoints
        In order to map datapoint to stream file in the group_dictionary, we use a list of grouped-dictionary.
        :return:
        '''

        Isigma_weight = self.jshandle.get('Isigma_weight', 2)
        CChalf_weight = self.jshandle.get('CChalf_weight', 1)
        anom_weight = self.jshandle.get('anom_weight', 1)
        completeness_weight = self.jshandle.get('completeness_weight', 0.2)
        nproc = self.jshandle('nproc', 20)
        point_group = self.jshandle('point_group')
        ngroups = self.jshandle.get('ngroups', 4)
        pop_size = self.jshandle.get('populations', 20)
        listofstats = []

        creator.create("FitnessMax", base.Fitness, weights=(Isigma_weight, CChalf_weight, anom_weight,
                                                              completeness_weight))
        creator.create("individual", list, fitness=creator.FitnessMax)

        toolbox = base.Toolbox()
        num_of_individuals_in_group = len(self.indata_dict) // ngroups
        toolbox.register('attr_int', random.randint, 1, ngroups) # total number of streams as max limit
        toolbox.register('individual', tools.initRepeat, creator.individual, toolbox.attr_int,
                         len(self.indata_dict.keys())//ngroups)
        toolbox.register('population', tools.initRepeat, list, toolbox.individual)
        population = toolbox.population(n=pop_size)

        toolbox.register("evaluate", self.evalOneMax)
        toolbox.register("mutate", tools.mutUniformInt, low=1, up=ngroups, indpb=0.05)

        toolbox.register("mate", tools.cxUniform, indpb=0.05)
        toolbox.register("select", tools.selTournament, tournsize=3)

        print("****************** Starting pre-optimisation ******************")
        crossover_prob = self.jshandle('crossover_prob', 0.3)
        mutation_prob = self.jshandle('mutation_prob', 0.6)
        ngen = self.jshandle('generations', 25)
        hof = []
        pop2, log = algorithms.eaSimple(population, toolbox, cxpb=float(crossover_prob), mutpb=float(mutation_prob),
                                        ngen=1, stats=listofstats,
                                        halloffame=hof, verbose=True)
        print("****************** Done with pre-optimisation ******************")

    def fitness(self, stat_dict):
        Isigma_weight = self.jshandle.get('Isigma_weight', 2)
        CChalf_weight = self.jshandle.get('CChalf_weight', 1)
        anom_weight = self.jshandle.get('anom_weight', 1)
        completeness_weight = self.jshandle.get('completeness_weight', 0.2)
        I_term = Isigma_weight * stat_dict['overall_snr']
        cc_term = CChalf_weight * stat_dict['overall_ccstar']
        C_term = completeness_weight * stat_dict['overall_completeness']

        score = I_term + C_term + cc_term
        return score

    def evalOneMax(self, individual):
        ngroups = self.jshandle.get('ngroups', 4)
        nproc = self.jshandle('nproc', 20)
        point_group = self.jshandle('point_group')
        best = {}
        allstat = self.stat_complete_set()
        score_all = self.fitness(allstat)

        for groupid in range(1, ngroups + 1):
            listofstream = []
            for i in range(len(self.indata_dict.keys())):
                if individual[i] == groupid:
                    listofstream.append(self.indata_dict.keys()[i])
                    self.run_as_command('cat %s >> selected.stream' % self.indata_dict.keys()[i])

            stats = self.report_stats('selected.stream')
            self.write_cell_file(stats)
            merging_stats = self.scale_merge('selected.stream')
            stats.update(merging_stats)
            score = self.fitness(stats)
            if score >= score_all:
                best['streamlist'] = listofstream
                best['metrics'] = stats
            else:
                best['metrics'] = allstat

        return
