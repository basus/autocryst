
dim0 = %
dim1 = ss
dim2 = fs
data = /entry_0000/instrument/jungfrau4m/data
photon_energy = 11560
clen = 0.17592
coffset = 0.0
adu_per_eV = 8.650519031141868e-05
res = 13333.333333333334
0/min_fs = 0
0/max_fs = 2067
0/min_ss = 0
0/max_ss = 2163
0/fs = x
0/ss = y
0/corner_x = -1037.799
0/corner_y = -1134.46


bad_v_0/min_ss = 513.0
bad_v_0/max_ss = 549.0
bad_v_0/min_fs = 0
bad_v_0/max_fs = 2066

bad_v_1/min_ss = 1062.0
bad_v_1/max_ss = 1098.0
bad_v_1/min_fs = 0
bad_v_1/max_fs = 2066

bad_v_2/min_ss = 1611.0
bad_v_2/max_ss = 1647.0
bad_v_2/min_fs = 0
bad_v_2/max_fs = 2066

bad_h_0/min_fs = 1028.5
bad_h_0/max_fs = 1037.5
bad_h_0/min_ss = 0
bad_h_0/max_ss = 2162

