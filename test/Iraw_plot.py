import os
import sys
import numpy as np
import math
import tkinter
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

hklfile = "/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_10um/Proteinase-K_10um_geoptimiser.hkl"

def create_bin(nbins, rmax, rmin):
    dmax = (1.0/(2*rmax))**3; dmin = (1.0/(2*rmin))**3;
    d2 = dmin #math.exp(math.log(dmin)*2/3.0)
    bin_arr = np.empty((nbins,)); resol_arr = np.empty((nbins))
    dstep = (dmax - dmin)/nbins
    for i in range(nbins):
        d_inc = (dmin + i*dstep);
        bin_arr[i] = math.exp(math.log(d_inc)*2./3.0) #1/np.sqrt(d_inc)
        resol_arr[i] = math.sqrt(1.0/bin_arr[i])/2.0
    return bin_arr, resol_arr

def get_bins(dstar, q_sq_array, nbins, ibin=0):

    for i in range(0,nbins):
        if dstar <= q_sq_array[i]:
           break
        else: ibin = 0 
    ibin = i 

    return ibin

def binning(q_arr, fom_arr, nbins, rmax, rmin):
    q_bin_arr, resol_arr = create_bin(nbins, rmax, rmin)
    
    binned_data = np.empty((q_bin_arr.shape[0],3))
    print(q_arr.size)
    if q_arr.size != fom_arr.size:
        print ("array sizes are different")
        return
    for i in range(nbins):
        #indx = np.where(q_arr < q_bin_arr[i])
        indx = get_bins(q_arr, q_bin_arr, nbins)
        shell_vals = fom_arr[indx]
        binned_data[i,0] = q_bin_arr[i]
        #binned_data[i,1] = 1.0/(np.sqrt(bin_arr[i]))/2.0
        binned_data[i,1] = np.mean(shell_vals)

    return binned_data

def extract_(filename):

    fh = open(filename, 'r')
    all = fh.readlines()
    fh.close()
    useful = []
    for lines in all:
        if "!" in lines:
           pass
        else:
           line = lines.split()
           if len(line) > 0:
              useful.append(line)
    return useful

def read_integrate(filename): #to read scale factor from integrate.LP file

    fh = open(filename, 'r')
    all = fh.readlines()
    fh.close()
    scale_fac = []; integrate_lines = []
    image_cnt = []; 

    string1 = " IMAGE IER  SCALE     NBKG NOVL NEWALD NSTRONG  NREJ   SIGMAB   SIGMAR\n"
    for ii in range(len(all)):
        if string1 in all[ii]:
           integrate_lines.append(all[ii+1:ii+26])
    
    for lines in integrate_lines:
        for line in lines:
            line = line.split()
            image_cnt.append(int(line[0]))
            scale_fac.append(float(line[2]))
    return image_cnt, scale_fac

def read_crystfel_hkl(hklfilename):
    fh = open(hklfilename, 'r')
    _all = fh.readlines()
    fh.close()
    string1 = "   h    k    l          I    phase   sigma(I)   nmeas\n"
    string2 = "End of reflections\n"

    start_chunk = _all.index(string1)
    stop_chunk = _all.index(string2)
    useful = _all[start_chunk+1:stop_chunk]
    reflist = []
    for lines in useful:
        line = lines.split()
        try:
            line.pop(line.index('-'))
        except (ValueError, IndexError) as err:
            pass
        reflist.append(line)
    return reflist


def get_data(data_list):
    data_arr = np.empty((len(data_list), 3))
    
    a = 68.07; b = 68.07; c = 107.92

    for ii in range(len(data_list)):
        if float(data_list[ii][3]) > 0:
           h = int(data_list[ii][0])
           k = int(data_list[ii][1])
           l = int(data_list[ii][2])
           q = ((h/a)**2 + (k/b)**2 + (l/c)**2)
           data_arr[ii, 0] = q
           data_arr[ii, 1] = float(data_list[ii][3])
           data_arr[ii, 2] = float(data_list[ii][4])
    return data_arr

def prep(fname, nbins, rmax, rmin):
    if not os.path.exists(fname):
       print("File not exists\n")
       sys.exit()
#    data_chunk = extract_(fname)
    data_chunk = read_crystfel_hkl(fname)
    data_arr = get_data(data_chunk)
    Ibin = binning(np.sqrt(data_arr[:,0]), data_arr[:,1], nbins, rmax, rmin)
    sigI_bin = binning(np.sqrt(data_arr[:,0]), data_arr[:,2], nbins, rmax, rmin)
    
    return Ibin, sigI_bin


def main():

    #I, sigI = prep(sys.argv[1], int(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]))
    I, sigI = prep(hklfile, 100, 10, 2.2) 
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(1,1,1)

    ax1.plot(I[:,0], I[:,1],'-o', linewidth=2)
    ax1.set_xlabel("q (1/$\AA^2$)", fontsize=20, fontweight='bold')
    ax1.set_ylabel("Diffracted Intensity (counts)", fontsize=20, fontweight='bold')
    #ax1.set_xlim([0.04, 0.14])   
    plt.show()

if __name__ == '__main__':
   main()

   #reflist = read_crystfel_hkl(hklfile)
   #data_arr = get_data(reflist)
   


