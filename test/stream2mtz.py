from autocryst.stream import Stream
from autocryst.run_crystfel import AutoCrystFEL
from cctbx.array_family import flex
from cctbx import crystal, miller
from iotbx import mtz

def get_unit_cell(streampath):

    unit_cell_param = AutoCrystFEL.report_cell(streampath)
    flex_symm = crystal.symmetry(unit_cell=unit_cell_param['unit_cell'], space_group=int(unit_cell_param['space_group_number']))
    return flex_symm

def handle_reflist(listofrefdict, CS):
    crystal_all = dict()
    crystal_all['indices'] = []
    crystal_all['iobs'] = []
    crystal_all['sigI'] = []
    crystal_all['mtz_dataset'] = []

    num_crystals = len(listofrefdict)
    if num_crystals > 0:
       for i in range(num_crystals):
           crystal = listofrefdict[i]
           for reflection in range(crystal['refList']):
               crystal_all['indices'].append(map(lambda x:flex.int(x), reflection[:3]))
               crystal_all['iobs'].append(map(lambda x:flex.double(x), reflection[3]))
               crystal_all['sigI'].append(map(lambda x:flex.double(x), reflection[4]))

           crystal_all['indices'] = flex.miller_index(crystal_all['indices'])
           m = miller.array(miller_set=miller.set(CS, crystal_all['indices']), data=crystal_all['iobs'], sigmas=crystal_all['sigI'])
           m.set_observation_type_xray_intensity()
           crystal_all['mtz_dataset'].append(m.as_mtz_dataset(column_root_label='IMEAN', wavelength=1.07))
    else:
       pass
    return crystal_all

    
def LoadReflections(streamfile):
    try:
       crystal_symmetry = get_unit_cell(streamfile)
	     s = Stream(streamfile)
       s.get_chunk_pointers()
       s.read_chunks(s.codgas_lookup['begin_chunk'][:5000], s.codgas_lookup['end_chunk'])
       s.get_peaklist(s.codgas_lookup['begin_peaklist'][:5000], s.codgas_lookup['end_peaklist'][:5000])
       s.get_reflections_list(s.codgas_lookup['begin_reflist'][:5000], s.codgas_lookup['end_reflist'][:5000])
       s.close()
       n_crystals = handle_reflist(s.image_refls, crystal_symmetry)
       n_crystals['mtz_dataset'].mtz_object().write(file_name=hklout)

    except (OSError, IndexError, ValueError) as err:
        print(err)
	      pass
    


