from __future__ import print_function
import os
import sys
import copy
import numpy as np
import glob
import h5py

try:
    import pycbf
except ImportError:
    sys.path.insert(0,'/opt/pxsoft/ccp4/v7.0/linux-x86_64/ccp4-7.0/lib/py2/cctbx/lib')
    import pycbf
from Image import CBFreader


def prep_spot(imgPrefix,dozorfolder):
    spotfiles = glob.glob(os.path.join(dozorfolder, '*.spot'))
    dozor_results = []
    # dozorDict = copy.deepcopy({})
    if len(spotfiles) > 0:
        for fname in spotfiles:
            dozorDict = {}
            try:
                basename = os.path.basename(fname)
                index = int(basename.strip('.spot'))
                imgName = glob.glob(os.path.join(imgPrefix,('*_%04d.cbf' %index)))[0]
                if os.path.isfile(imgName):
                    dozorDict['image_name'] = imgName
                else:
                    print('image name not found %s' %imgName)
                data_array, spotList, npeaks = read_spotfile(fname)
                dozorDict['DozorSpotList'] = spotList
                dozorDict['nPeaks'] = npeaks
                dozorDict['PeakXPosRaw'] = data_array[:,1]
                dozorDict['PeakYPosRaw'] = data_array[:,2]
                dozorDict['PeakTotalIntensity'] = data_array[:,3]
                dozor_results.append(dozorDict)
            except Exception as e:
                raise e
    else:
        print("No spot files found")
    return dozor_results

def read_spotfile(fname):
    data = np.loadtxt(fname, skiprows=3)
    spotList = []
    for i in range(data.shape[0]):
        spotList.append(data[i,:].tolist())
    npeaks = len(spotList)
    return data, spotList, npeaks

def prep_from_json(jsonfile): # Olof's json file
    fh = open(jsonfile, 'r')
    js = json.load(fh)
    fh.close()
    dozor_results = []
    try:
        for image in js['meshPositions']:
            if image['dozorSpotListShape'] > 0:
                dozorDict = {}
                imgPrefix = js['directory']
                dozorDict['image_name'] = os.path.join(imgPrefix, image['imageName'])
                dozorDict['nPeaks'] = image['dozorSpotListShape'][0]
                spot_arr = np.fromstring(base64.b64decode(image['dozorSpotList']))
                spot_arr = spot_arr.reshape((spot_arr.size/5, 5))
                dozorDict['PeakXPosRaw'] = spot_arr[:,1]
                dozorDict['PeakYPosRaw'] = spot_arr[:,2]
                dozorDict['PeakTotalIntensity'] = spot_arr[:,3]
            else:
                pass
            dozor_results.append(dozorDict)
    except KeyError as e:
        raise e
    return dozor_results

def get_dozor_peaks(dozor_results, start, stop):
    chunk_size = int(stop - start)
    c = CBFreader(dozor_results[0]['image_name'])
    c.read_cbfheaders()
    c.read_cbfdata()
    size_3d = (chunk_size,) + c.data.shape
    data_stack_3d = np.empty(size_3d, dtype=np.int32)
    del c
    lst_cbfs = []; npeaks = []; xpos = []; ypos = []; peak_int = []
    for i in range(start, stop):
        # lst_cbfs.append(dozor_results[i]['image_name'])
        c = CBFreader(dozor_results[i]['image_name'])
        c.read_cbfdata()
        data_stack_3d[i,:,:] = c.data
        del c
        npeaks.append(dozor_results[i]['nPeaks'])
        xpos.append(dozor_results[i]['PeakXPosRaw'])
        ypos.append(dozor_results[i]['PeakYPosRaw'])
        peak_int.append(dozor_results[i]['PeakTotalIntensity'])

    npeaks = np.array(npeaks)
    xpos_arr = np.zeros((chunk_size, npeaks.max()))
    ypos_arr = np.zeros((chunk_size, npeaks.max()))
    peak_int_arr = np.zeros((chunk_size, npeaks.max()))
    for jj in range(chunk_size):
        xpos_arr[jj,0:len(xpos[jj])] = xpos[jj]
        ypos_arr[jj,0:len(ypos[jj])] = ypos[jj]
        peak_int_arr[jj,0:len(peak_int[jj])] = peak_int[jj]
    return data_stack_3d, npeaks, xpos_arr, ypos_arr, peak_int_arr

def create_stack(dozor_results):
    if len(dozor_results) == 0:
        print("dozor did not work or no hits")
        return
    stacklength = len(dozor_results)
    if stacklength < 1000:
        data_stack_3d, npeaks, xpos, ypos, peak_int = get_dozor_peaks(dozor_results, 0, stacklength)
    else:
        nchunk = int(stacklength/1000) + 1
        for i in range(nchunks):
            start = 1000*i
            stop = 1000*(i+1)
            try:
                data_stack_3d, npeaks, xpos, ypos, peak_int = get_dozor_peaks(dozor_results, start, stop)
            except (IndexError, ValueError) as err:
                raise err
    return data_stack_3d, npeaks, xpos, ypos, peak_int

def saveH5(dozor_results):
    data_stack_3d, npeaks, xpos, ypos, peak_int = create_stack(dozor_results)
    hf = h5py.File('dozor_hits.cxi', 'w')
    rawdata = hf.create_dataset('/data/data', data=data_stack_3d)
    npeaks = hf.create_dataset('/data/peakinfo/nPeaks', data=npeaks)
    posXraw = hf.create_dataset('/data/peakinfo/peakXPosRaw', data=xpos)
    posYraw = hf.create_dataset('/data/peakinfo/peakYPosRaw', data=ypos)
    peak_intensity = hf.create_dataset('/data/peakinfo/peakTotalIntensity', data=peak_int)

    hf.close()

if __name__ == '__main__':
    dozor_results = prep_spot(sys.argv[1],sys.argv[2])
    saveH5(dozor_results)
