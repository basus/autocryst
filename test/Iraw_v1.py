import os
import numpy as np
import math
from autocryst.stream import Stream
import tkinter
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

intensity = {}
intensity_raw = {}
adu_per_photon = 1.0
nshells = 1000

A = 68.72
B = 68.72 
C = 108.97


def res(recip):
    return 1 / np.sqrt((recip[0]/A)**2+(recip[1]/B)**2+(recip[2]/C)**2)


def read_crystfel_hkl(hklfilename):
    fh = open(hklfilename, 'r')
    _all = fh.readlines()
    fh.close()
    string1 = "   h    k    l          I    phase   sigma(I)   nmeas\n"
    string2 = "End of reflections\n"

    start_chunk = _all.index(string1)
    stop_chunk = _all.index(string2)
    useful = _all[start_chunk+1:stop_chunk]
    reflist = []
    for lines in useful:
        line = lines.split()
        try:
            line.pop(line.index('-'))
        except (ValueError, IndexError) as err:
            pass
        reflist.append(line)
    return reflist

def refs_from_stream(streamfile):

    s = Stream(streamfile)
    s.get_chunk_pointers()

    s.read_chunks(s.codgas_lookup['begin_chunk'], s.codgas_lookup['end_chunk'])
    s.get_reflections_list(s.codgas_lookup['begin_reflist'][:7000], s.codgas_lookup['end_reflist'][:7000])

    s.close()

    total_reflist = dict()
    for ii in range(len(s.image_refls)):
        for jj in range(len(s.image_refls[ii]['refList'])):
            h = int(s.image_refls[ii]['refList'][jj][0])
            k = int(s.image_refls[ii]['refList'][jj][1])
            l = int(s.image_refls[ii]['refList'][jj][2])
            Iraw = float(s.image_refls[ii]['refList'][jj][3]) / adu_per_photon
            sigI = float(s.image_refls[ii]['refList'][jj][4]) / adu_per_photon
            if Iraw > 0:
               total_reflist[(h,k,l)] = (Iraw, sigI)
    return total_reflist


def get_data(data_list):
    data_arr = dict()
    refl_all = 0
    refl_acc = 0 

    for ii in range(len(data_list)):
        h = int(data_list[ii][0])
        k = int(data_list[ii][1])
        l = int(data_list[ii][2])
        Iraw = float(data_list[ii][3])
        refl_all += 1
        if Iraw > 0:
           data_arr[(h,k,l)] = Iraw/adu_per_photon
           refl_acc += 1
    print("%d (accentric: %d) reflections read\n" %(refl_all,refl_acc)) 
    return data_arr

def binning(hklfile):

    #reflist = read_crystfel_hkl(hklfile)
    #Iraw_table = get_data(reflist)
    Iraw_table = refs_from_stream(hklfile)

    reso = []
    for hkl in Iraw_table:
        reso.append(res(hkl))
    dmin = 1.8

    shells = []
    shell_len = np.zeros(nshells)
    shell_len_sum = np.zeros(nshells)
    shell_fp_exp = np.zeros(nshells)
    shell_sigI_exp = np.zeros(nshells)

    for i in range(1,nshells+1):
        shells.append(np.sqrt((dmin**2*nshells)/i))

    for hkl in Iraw_table:
        d = res(hkl)
        sh = 0
        while (sh < nshells-1) and (d < shells[sh]):
            sh += 1
        shell_len[sh] += 1.0
        shell_fp_exp[sh] += Iraw_table[hkl][0]
        shell_sigI_exp[sh] += Iraw_table[hkl][1]
    '''
    shell_I_exp = 0

    for i in range(len(shells)):
        if shell_len[i] > 0:
           shell_I_exp += shell_fp_exp[i]        
    '''
    return shells, shell_len, shell_fp_exp, shell_sigI_exp


def main():
    #hklfile1 = "/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_10um/Proteinase-K_10um_geoptimiser.stream"
    #hklfile2 = "/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_20um/Proteinase-K_20um_geoptimiser.stream"
    #hklfile3 = "/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_50um/Proteinase-K_50um_geoptimiser.stream"
    #hklfile4 = "/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_75um/Proteinase-K_75um_geoptimiser.stream"
    hklfile1 = "/data/visitor/mx2434/id29/julien/Prot-K/10-10/Prot-K_10-10-final.stream"
    hklfile2 = "/data/visitor/mx2434/id29/julien/Prot-K/20-20/Prot-K_20-20-final.stream"
    hklfile3 = "/data/visitor/mx2434/id29/julien/Prot-K/50-50/Prot-K_50-50-final.stream"	
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(1,1,1)
    ax2 = ax1.twiny()
    def q2r(dstar):
        r = []
        for x in dstar:
            if x == 0:
               r.append("%2.1f" %0)
            else:
               r.append("%2.1f" %(math.sqrt(1.0 / float(x))))
        return r
    
    for fh in [hklfile1, hklfile2, hklfile3]:
        res, shell_len, I_exp, sig_I = binning(fh)
        d2 = []; Imean = [];
        for i in range(len(res)):
            if shell_len[i] > 0:
               d2.append(1/res[i]**2)
               
               Imean.append(I_exp[i]/shell_len[i])
        
        #mu1 = Imean.mean()
        #sig = Imean.std()
        ax1.plot(d2, Imean,'-', linewidth=2)
        #ax1.fill_between(d2, mu1+sig, mu1-sig, alpha=0.5)
    ax1.set_yscale('log')
    locmaj = matplotlib.ticker.LogLocator(numticks=10)
    ax1.yaxis.set_major_locator(locmaj)
    ax1.set_xlabel("1/d$^2$ ($\AA^{-2}$)", fontsize=20, fontweight='bold')
    ax1.set_ylabel("Diffracted Intensity (ADU)", fontsize=20, fontweight='bold')
    ax1.set_xlim([0.1, 0.25])

    new_tick_locations = ax1.get_xticks()
    ax2.set_xlim(ax1.get_xlim())
    ax2.set_xticks(new_tick_locations)
    # print(new_tick_locations)
    ax2.set_xticklabels(q2r(new_tick_locations))
    
    ax2.set_xlabel('Resolution ($\AA$)', fontsize=20, fontweight='bold')
    ax1.legend(['10um', '20um', '50um'], loc='upper right', prop={'size':20, 'weight':'bold'})
    ax1.tick_params(axis='both', which='major', labelsize=16)
    ax2.tick_params(axis='both', which='major', labelsize=16)
    plt.show()

if __name__ == '__main__':
   main()
   
   '''
   s = Stream("/data/visitor/mx2434/id29/proteinase_K/julien/Proteinase-K_10um/Proteinase-K_10um_geoptimiser.stream")
   s.get_chunk_pointers()

   s.read_chunks(s.codgas_lookup['begin_chunk'][:5000], s.codgas_lookup['end_chunk'])
   s.get_reflections_list(s.codgas_lookup['begin_reflist'][:5000], s.codgas_lookup['end_reflist'][:5000])

   s.close()
   
   total_reflist = []
   for ii in range(len(s.image_refls)):
       total_reflist.append(s.image_refls[ii]['refList'])

   print(total_reflist)
   '''

 






