"""
Created on 15-May-2019
Author: S. Basu
"""
from __future__ import print_function
import os
import sys
import subprocess as sub
import logging
import numpy as np
import glob
import h5py
import json
import base64
import src.dozor_input as di
from scitbx.array_family import flex

try:
    import pycbf
except ImportError:
    sys.path.insert(0,'/opt/pxsoft/ccp4/v7.0/linux-x86_64/ccp4-7.0/lib/py2/cctbx/lib')
    import pycbf
from src.Image import CBFreader

logger = logging.getLogger('autoCryst')

class Dozor(object):
    def __init__(self, jsonfile):
        if os.path.exists(jsonfile):
            fh = open(jsonfile, 'r')
            self.jshandle = json.load(fh)
            fh.close()
        else:
            err = "input json file does not exist, Quit!"
            logger.info('Error:{}'.format(err))
            sys.exit()
            self.success = False
        self.input_dict = {}
        self.lst_of_files = []
        self.dozor_results = []
        self.stacklength = 0
        Dozor.stacks = {}
        self.success = True
        return

    def prep_dozorinput(self):
        try:
            self.lst_of_files = sorted(glob.glob(os.path.join(self.jshandle['image_folder'], '*.cbf')))
        except KeyError as err:
            logger.info('Dozor-Error:{}'.format(err))
            self.success = False
        try:
            cbf = CBFreader(self.lst_of_files[0])
            cbf.read_cbfheaders()
            name = os.path.basename(cbf.cbf_file)
            name = name.strip('.cbf')[:-4]
            name = name + '????.cbf'
            name_template = os.path.join(self.jshandle['image_folder'], name)
        except (IndexError, ValueError, IOError) as err:
            logger.info('DozorHit_Error:{}'.format(err))
            self.success = False
            return

        if cbf.headers['detector_name'][0] == 'PILATUS3':
            det = 'pilatus'
        elif cbf.headers['detector_name'][0] == 'PILATUS':
            det = 'pilatus'
        else:
            pass
        det += cbf.headers['detector_name'][1].lower()
        self.input_dict['det'] = det
        self.input_dict['exposure'] = cbf.headers['exposure']
        self.input_dict['detector_distance'] = '%6.3f' %(cbf.headers['detector_distance']*1000)
        self.input_dict['wavelength'] = '%1.3f' %(12398 / cbf.headers['photon_energy'])
        self.input_dict['beamx'] = cbf.headers['beam_center_x']
        self.input_dict['beamy'] = cbf.headers['beam_center_y']
        self.input_dict['oscillation_range'] = cbf.headers['oscillation_range']
        self.input_dict['starting_angle'] = cbf.headers['starting_angle']
        self.input_dict['nframes'] = len(self.lst_of_files)
        self.input_dict['name_template'] = name_template

        return

    def run_dozor(self):
        self.prep_dozorinput()
        if self.success:
            if self.input_dict['det'] == 'pilatus2m':
                dozor_str = di.dozor_input['2m']
            elif self.input_dict['det'] == 'pilatus6m':
                dozor_str = di.dozor_input['6m']
            with open('dozor.dat', 'w') as dh:
                dh.write(dozor_str.format(**self.input_dict))
            cmd = 'dozor__v1.3.8 -p dozor.dat > /dev/null'
            sub.call(cmd, shell=True)
            self.success = True
        else:
            err = 'dozor could not be run'
            logger.info('DozorHit_Error:{}'.format(err))
            self.success = False
        return

    def prep_spot(self):
        if self.jshandle['dozorfolder'] is None:
            cwd = os.getcwd()
        else:
            cwd = self.jshandle['dozorfolder']
        spotfiles = glob.glob(os.path.join(cwd, '*.spot'))

        if len(spotfiles) > 0 and self.success is True:
            for fname in spotfiles:
                dozorDict = {}
                basename = os.path.basename(fname)
                index = int(basename.strip('.spot'))
                imgName = glob.glob(os.path.join(self.jshandle['image_folder'],('*_%04d.cbf' %index)))[0]
                if os.path.isfile(imgName):
                    dozorDict['image_name'] = imgName
                else:
                    logger.info('image name not found %s' %imgName)
                data_array, spotList, npeaks = Dozor.read_spotfile(fname)
                if npeaks > 5:
                    dozorDict['DozorSpotList'] = spotList
                    dozorDict['nPeaks'] = npeaks
                    dozorDict['PeakXPosRaw'] = data_array[:,1]
                    dozorDict['PeakYPosRaw'] = data_array[:,2]
                    dozorDict['PeakTotalIntensity'] = data_array[:,3]
                    self.dozor_results.append(dozorDict)
                    self.success = True
                else:
                    pass
        else:
            logger.info("No spot files found")
            self.success = False
        return

    @staticmethod
    def read_spotfile(fname):
        data = np.array([])
        spotList = []
        npeaks = 0
        if os.path.isfile(fname):
            data = np.loadtxt(fname, skiprows=3)
        else:
            err = "No spot file found"
            logger.info('DozorHit_Error:{}'.format(err))
            return data, spotList, npeaks
        for i in range(data.shape[0]):
            try:
                spotList.append(data[i,:].tolist())
            except IndexError: # dozor can provide hit with only 1 peak (false positive), which needs to be discarded
                pass
        npeaks = len(spotList)
        return data, spotList, npeaks

    def write_json(self):
        if len(self.dozor_results) == 0:
            err = 'Nothing to dump, either dozor did not run or no spots found'
            logger.info('Error: {}'.format(err))
            return

        with open('dozorspots.json', 'w') as jh:
            for image in self.dozor_results:
                for k in image.keys():
                    if isinstance(image[k], np.ndarray):
                        image[k] = image[k].tolist()
                json.dump(image, jh, sort_keys=True, indent=2)

        return

    def extract_olof_json(self, olof_json): # Olof's json file
        if not os.path.isfile(olof_json):
            logger.info('DozorHit_Error:{}'.format("Olof json file does not exist"))
            self.success = False
            return
        fh = open(olof_json, 'r')
        js = json.load(fh)
        fh.close()
        try:
            for image in js['meshPositions']:
                if len(image['dozorSpotListShape']) > 0 and image['dozorSpotListShape'][0] > 5:
                    dozorDict = {}
                    imgPrefix = js['directory']
                    dozorDict['image_name'] = os.path.join(imgPrefix, image['imageName'])
                    dozorDict['nPeaks'] = image['dozorSpotListShape'][0]
                    spot_arr = np.fromstring(base64.b64decode(image['dozorSpotList']))
                    spot_arr = spot_arr.reshape((spot_arr.size/5, 5))
                    dozorDict['PeakXPosRaw'] = spot_arr[:,1]
                    dozorDict['PeakYPosRaw'] = spot_arr[:,2]
                    dozorDict['PeakTotalIntensity'] = spot_arr[:,3]
                    dozorDict['DozorSpotList'] = spot_arr.tolist()
                    self.dozor_results.append(dozorDict)
                else:
                    pass
                self.success = True
        except KeyError as e:
            logger.info('DozorHit_Error:{}'.format(e))
            self.success = False
        return

    @staticmethod
    def get_dozor_peaks(dozor_lst_dict):
        chunk_size = len(dozor_lst_dict)
        '''
        cbf_unicode = dozor_lst_dict[0]['image_name']
        c = CBFreader(cbf_unicode.encode('ascii', 'ignore'))
        c.read_cbfheaders()
        c.read_cbfdata()
        with open('headers.json', 'w') as jh:
            json.dump(c.headers, jh, sort_keys=True, indent=2)
        size_2d = (chunk_size,) + (c.data.size(),)
        size_3d = (chunk_size,) + c.dimension
        '''
        Dozor.stacks['data_stack_3d'] = []
        # del c
        npeaks = []; xpos = []; ypos = []; peak_int = []

        for i in range(chunk_size):
           cbf_ascii = dozor_lst_dict[i]['image_name'].encode('ascii', 'ignore')
           c = CBFreader(cbf_ascii)
           c.read_cbfdata()
           Dozor.stacks['data_stack_3d'].append(c.data)
           del c
           npeaks.append(dozor_lst_dict[i]['nPeaks'])
           xpos.append(dozor_lst_dict[i]['PeakXPosRaw'])
           ypos.append(dozor_lst_dict[i]['PeakYPosRaw'])
           peak_int.append(dozor_lst_dict[i]['PeakTotalIntensity'])
        Dozor.stacks['npeaks'] = flex.int(npeaks)
        Dozor.stacks['xpos_arr'] = flex.float(xpos)
        Dozor.stacks['ypos_arr'] = flex.float(ypos)
        Dozor.stacks['peak_int_arr'] = flex.float(peak_int)
        '''
        for jj in range(chunk_size):
            Dozor.stacks['xpos_arr'][jj,0:len(xpos[jj])] = xpos[jj]
            Dozor.stacks['ypos_arr'][jj,0:len(ypos[jj])] = ypos[jj]
            Dozor.stacks['peak_int_arr'][jj,0:len(peak_int[jj])] = peak_int[jj]
        '''
        Dozor.stacks['data_stack_3d'] = flex.int(np.array(Dozor.stacks['data_stack_3d']))
        Dozor.stacks['data_stack_3d'].reshape(flex.grid(chunk_size,1679,1475))
        return

    def create_stack(self):
        if len(self.dozor_results) == 0:
            msg = "dozor did not work or no hits"
            logger.info('DozorHits_MSG:{}'.format(msg))
            self.success = False
            return
        self.stacklength = len(self.dozor_results)
        if self.stacklength < 1000:
            Dozor.get_dozor_peaks(self.dozor_results)
            Dozor.saveH5(Dozor.stacks, 'dozor_0.cxi')
            self.success = True
        else:
            nchunk = int(self.stacklength/1000) + 1
            njobs = 4
            for i in range(nchunk):
                start = 1000*i
                stop = 1000*(i+1)
                h5name = 'dozor_%d.cxi' %i
                try:
                    Dozor.get_dozor_peaks(self.dozor_results[start:stop])
                except IndexError:
                    stop = start + (self.stacklength - start)
                    Dozor.get_dozor_peaks(self.dozor_results[start:stop])
                # Dozor.saveH5(Dozor.stacks, h5name)
            self.success = True
            print(Dozor.stacks)
        return

    @staticmethod
    def saveH5(dict_with_peakinfo, h5name):
        cxis_all = glob.glob('dozor*cxi')
        if h5name in cxis_all:
            cnt = len(cxis_all)
            h5name = 'dozor_%d.cxi' %cnt
        else:
            pass
        try:
            hf = h5py.File(h5name, 'w')
            rawdata = hf.create_dataset('/data/data', data=dict_with_peakinfo['data_stack_3d'])
            npeaks = hf.create_dataset('/data/peakinfo/nPeaks', data=dict_with_peakinfo['npeaks'])
            posXraw = hf.create_dataset('/data/peakinfo/peakXPosRaw', data=dict_with_peakinfo['xpos_arr'])
            posYraw = hf.create_dataset('/data/peakinfo/peakYPosRaw', data=dict_with_peakinfo['ypos_arr'])
            peak_intensity = hf.create_dataset('/data/peakinfo/peakTotalIntensity', data=dict_with_peakinfo['peak_int_arr'])
        except (KeyError, ValueError) as err:
            logger.info('DozorHit_Error:{}'.format(err))
        hf.close()
        return

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%y-%m-%d %H:%M',
                        filename='test.log',
                        filemode='a+')
    dd = Dozor(sys.argv[1])
    #dd.run_dozor()
    #dd.prep_spot()
    try:
        dd.extract_olof_json(dd.jshandle['olof_json'])
        dd.create_stack()
    except (KeyError, IOError, ValueError, TypeError) as err:
        print("Olof json file was not provided")
        if dd.jshandle['dozorfolder'] is None:
            dd.run_dozor()
        else:
            pass
        dd.prep_spot()
        dd.create_stack()
    #print(dd.dozor_results)
