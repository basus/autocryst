import os,sys
import re, numpy as np

#fname = sys.argv[1]

def read_stream(fname):
    f = open(fname, 'r')
    d = {'image': re.compile(r'^Image\sfilename:\s(?P<image>.*)\n'),
    'hit': re.compile(r'^hit\s=\s(?P<hit>[0-1])\n'),
    'indexed_by': re.compile(r'^indexed_by\s=\s(?P<indexed_by>.*)\n')}
    while True:
        fline = f.readline()
        if not fline:
            break
        for k, pat in d.iteritems():
            match = pat.search(fline)
            if match:
                print k, match.group(1)
    f.close()

def get_chunk_pointers(fname):
    chunk1 = re.compile('----- Begin chunk')
    chunk2 = re.compile('----- End chunk')

    fh = open(fname, 'r')
    length = sum(1 for line in fh)

    line_offset = np.zeros((length)); offset = 0;
    begin_chunk_id = []; end_chunk_id = [];

    with open(fname, 'r') as f:

        for index, line in enumerate(f):
            line_offset[index] = offset
            offset += len(line)
            if chunk1.search( line ):
                begin_chunk_id.append(index)
            if chunk2.search( line ):
                end_chunk_id.append(index)

        fh.seek(0)
        '''
        for ii in range(begin_chunk_id[1],end_chunk_id[1]):
            fh.seek(line_offset[ii])
            print fh.readline()
        '''
    return line_offset, begin_chunk_id, end_chunk_id

def read_chunk(fname, array, chunk1, chunk2):
    fh = open(fname, 'r')
    fh.seek(0)
    d = {'image': re.compile(r'^Image\sfilename:\s(?P<image>.*)\n'),
    'cell': re.compile(r'^Cell\sparameters\s(?P<cell>([0-9\.]+)\s([0-9\.]+)\s([0-9\.]+)\snm,\s([0-9\.]+)\s([0-9\.]+)\s([0-9\.]+)\sdeg$)\n'),
    'crystal' : re.compile('--- Begin crystal')}
    for start, end in zip(chunk1, chunk2):
        for ii in range(start, end+1):
            fh.seek(array[ii])
            line = fh.readline()
            for k, pat in d.iteritems():
                match = pat.search(line)
                if match:
                    if k == 'image':
                        print '%s: %s' %(k,match.group('image'))

                    if k == 'cell':
                        print '%s: %s' %(k,match.group('cell'))
    return

line_offset, begin_chunk_id, end_chunk_id = get_chunk_pointers(sys.argv[1])
read_chunk(sys.argv[1], line_offset, begin_chunk_id, end_chunk_id)
