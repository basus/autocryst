#!/bin/bash
#SBATCH --job-name="ExeCrystFEL"
#SBATCH --partition=mx
#SBATCH --mem=2000
#SBATCH --nodes=1
#SBATCH --cpus-per-task=20
#SBATCH --time=0:10:00
#SBATCH --chdir=/home/esrf/basus/scripts/edna2/testdata/rundir/20200817_185947/ExeCrystFEL_mvco0hex
#SBATCH --output=stdout.txt
#SBATCH --error=stderr.txt
indexamajig -i /home/esrf/basus/scripts/edna2/testdata/rundir/20200817_185947/ExeCrystFEL_mvco0hex/Sample-1-3-12/XrayCentering_01/autoCryst_2020-08-17_18-59-47/input.lst -o /home/esrf/basus/scripts/edna2/testdata/rundir/20200817_185947/ExeCrystFEL_mvco0hex/Sample-1-3-12/XrayCentering_01/autoCryst_2020-08-17_18-59-47/18-59-47.stream -g /home/esrf/basus/scripts/edna2/testdata/rundir/20200817_185947/ExeCrystFEL_mvco0hex/Sample-1-3-12/XrayCentering_01/autoCryst_2020-08-17_18-59-47/pilatus2m.geom --indexing=mosflm --no-cell-combinations --peaks=peakfinder8 --integration=rings-grad-rescut --int-radius=3,4,6 -j 20 --no-check-peaks --highres=0.0 --peak-radius=3,4,5 --min-peaks=30 --wait-for-file=0 --min-snr=4 --threshold=10 --local-bg-radius=10 --min-res=50 --no-non-hits-in-stream 1>ExeCrystFEL.log.txt 2>ExeCrystFEL.err.txt
