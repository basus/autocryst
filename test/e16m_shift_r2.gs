; Optimized panel offsets can be found at the end of the file
; Optimized panel offsets can be found at the end of the file
; Example geometry file for Eiger 16M detector, using its native file format
; and binning 2.

; Camera length (in m) and photon energy (eV)
clen = 0.299981
photon_energy = 12400
max_adu=6000
; adu_per_photon needs a relatively recent CrystFEL version.  If your version is
; older, change it to adu_per_eV and set it to one over the photon energy in eV
;adu_per_photon = 1 
adu_per_eV = 8.0645e-5
res = 13333.3   ; 75 micron pixel size

; These lines describe the data layout for the Eiger native multi-event files
dim0 = %
dim1 = ss
dim2 = fs
data = /entry/data/data

; Mask out strips between panels
bad_v0/min_fs = 1030
bad_v0/min_ss = 0
bad_v0/max_fs = 1039
bad_v0/max_ss = 2166

bad_h0/min_fs = 0
bad_h0/min_ss = 514
bad_h0/max_fs = 2069
bad_h0/max_ss = 550

bad_h1/min_fs = 0
bad_h1/min_ss = 1065
bad_h1/max_fs = 2069
bad_h1/max_ss = 1101

bad_h2/min_fs = 0
bad_h2/min_ss = 1616
bad_h2/max_fs = 2069
bad_h2/max_ss = 1652

; Uncomment these lines if you have a separate bad pixel map (recommended!)
mask_file =  /sls/X06DA/Data10/e15602/20170828_TOMCAT_PepT/SiN_PepT2/stills/SiN_PepT2_stills_0deg_-141_1_master.h5 
mask = /entry/instrument/detector/detectorSpecific/pixel_mask
mask_good = 0x00
mask_bad  = 0xFF

; corner_{x,y} set the position of the corner of the detector (in pixels)
; relative to the beam
panel0/min_fs = 0
panel0/min_ss = 0
panel0/max_fs = 4149
panel0/max_ss = 4370
panel0/corner_x = -2039.93
panel0/corner_y = -2180.97


panel0/fs = +1.000000x -0.000017y
panel0/ss = +0.000017x +1.000000y

rigid_group_panel0 = panel0
rigid_group_collection_panel0 = panel0



panel0/coffset = -0.000022
