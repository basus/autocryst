
mask_file = /home/esrf/opid29/test_edna2/autocryst/test/mesh-Myo_cryo_10T-Myo_cryo_10T_0_1_master.h5
mask = /entry/instrument/detector/detectorSpecific/pixel_mask
mask_good = 0x0 ;0x00
mask_bad = 0x1  ;0xFF
dim0 = %
dim1 = ss
dim2 = fs
data = /entry/data/data
photon_energy = 14199.519150406284
clen = 0.17067500000000002
coffset = 0.0
adu_per_eV = 7.04249199855044e-05
res = 13333.333333333334
0/min_fs = 0
0/max_fs = 3107
0/min_ss = 0
0/max_ss = 3261
0/fs = x
0/ss = y
0/corner_x = -1582.27808
0/corner_y = -1617.5725


bad_v_0/min_ss = 512.0
bad_v_0/max_ss = 549.0
bad_v_0/min_fs = 0
bad_v_0/max_fs = 3107

bad_v_1/min_ss = 1061.0
bad_v_1/max_ss = 1098.0
bad_v_1/min_fs = 0
bad_v_1/max_fs = 3107

bad_v_2/min_ss = 1610.0
bad_v_2/max_ss = 1647.0
bad_v_2/min_fs = 0
bad_v_2/max_fs = 3107

bad_v_3/min_ss = 2159.0
bad_v_3/max_ss = 2196.0
bad_v_3/min_fs = 0
bad_v_3/max_fs = 3107

bad_v_4/min_ss = 2708.0
bad_v_4/max_ss = 2745.0
bad_v_4/min_fs = 0
bad_v_4/max_fs = 3107

bad_h_0/min_fs = 1028.0
bad_h_0/max_fs = 1039.0
bad_h_0/min_ss = 0
bad_h_0/max_ss = 3261

bad_h_1/min_fs = 2067.0
bad_h_1/max_fs = 2078.0
bad_h_1/min_ss = 0
bad_h_1/max_ss = 3261

