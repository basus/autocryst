import h5py
import pathlib
import matplotlib.pyplot as plt
import sys

ncols = 600
nrows = 400

data_dir = pathlib.Path("/data/visitor/mx2545/id29/20231206/RAW_DATA/TLN_apo/TLN_apo/run_0/aggregated")
master = (datadir.glob("master*dense.h5"))
data_path = data_dir / master[0]

print(data_path)

f = h5py.File(data_path, "r")
isHit = f["/entry_0000/processing/peakfinder/isHit"][()]
print(f"No data points : {len(isHit)}")
print(f"Type of data   : {type(isHit)}")
print(f"Max value      : {max(isHit)}")
print(f"Min value      : {min(isHit)}")

isHit.shape = (nrows, ncols)
print(isHit.shape)
plt.imshow(isHit, interpolation="none")
plt.show()
