
mask_file = ../examples/mesh-x_2_1_master.h5
mask = /entry/instrument/detector/detectorSpecific/pixel_mask
mask_good = 0x00
mask_bad = 0xFF
dim0 = %
dim1 = ss
dim2 = fs
data = /entry/data/data
photon_energy = 12811.821785
clen = 0.211762502789
coffset = 0.0
adu_per_eV = 7.80529121292e-05
res = 13333.3327
0/min_fs = 0
0/max_fs = 2069
0/min_ss = 0
0/max_ss = 2166
0/fs = x
0/ss = y
0/corner_x = -1022.18511963
0/corner_y = -1086.01086426


bad_v_0/max_fs = 2069
bad_v_0/min_fs = 0
bad_v_0/max_ss = 550
bad_v_0/min_ss = 514

bad_v_1/max_ss = 1100
bad_v_1/min_ss = 1064
bad_v_1/max_fs = 2069
bad_v_1/min_fs = 0

bad_v_2/max_ss = 1650
bad_v_2/min_fs = 0
bad_v_2/max_fs = 2069
bad_v_2/min_ss = 1614

bad_h_0/max_ss = 2166
bad_h_0/min_fs = 1030
bad_h_0/max_fs = 1039
bad_h_0/min_ss = 0

