# Com bination Of Datasets by Genetic Algorithm Selection
#import matplotlib
#matplotlib.use('Agg') 
import math
import time
import shutil
import uuid
from oarpy.oarjob import submit
import stat
import sys
sys.path.insert(0,"/data/visitor/mx1787/autocryst/")
#from src.run_crystfel import AutoCrystFEL
#from src.stream import Stream
#from src.parser import ResultParser
import random
import os
import re
import time
#import pylab
import numpy
import glob
#import matplotlib as mpl
#mpl.use('Agg')
#import matplotlib.pyplot as plt
#import networkx
import datetime, operator 
from deap import base
from scipy.spatial import distance
from deap import creator
from deap import tools
from deap import algorithms
from optparse import OptionParser
from scoop import futures, shared    
from functools import partial
#from flufl.lock import Lock
#from easyprocess import Proc
user_res_override = 0.0

refmac5 = "/opt/pxsoft/bin/refmac5"
refmac_cycles = 1;
findblobs = "/opt/pxsoft/bin/find-blobs"

equilibration_mode = "initial" # "initial" : look at all metrics at the beginning, then set them to 1, THEN apply user weights
                            # "optimal" : scale to hoped for values

parser = OptionParser()

#
# For SIR mode, make sure all individual datasets are Processed with a common reference in cases where indexing ambiguity exists.  Example command line
#
# python /data/id23eh2/inhouse/max2/NS1/CG_SIR.py -n40 -g200 -t ../lyso_microcrystals_UV_5min_REAL/ -u ../lyso_microcrystals_noUV/ -l96 --sad_reference /data/id23eh2/inhouse/opid232/20150401/PROCESSED_DATA/lyso_rip/REF.pdb -fNOANO.HKL
#
#
#
#
# 

sgdict = {"1":"P1","10":"P12/m1","100":"P4bm","101":"P42cm","102":"P42nm","103":"P4cc","104":"P4nc","105":"P42mc","106":"P42bc","107":"I4mm","108":"I4cm","109":"I41md","11":"P121/m1","110":"I41cd","111":"P-42m","112":"P-42c","113":"P-421m","114":"P-421c","115":"P-4m2","116":"P-4c2","117":"P-4b2","118":"P-4n2","119":"I-4m2","12":"C12/m1","120":"I-4c2","121":"I-42m","122":"I-42d","123":"P4/mmm","124":"P4/mcc","125":"P4/nbm","126":"P4/nnc","127":"P4/mbm1","128":"P4/mnc1","129":"P4/nmm1","13":"P12/c1","130":"P4/ncc1","131":"P42/mmc","132":"P42/mcm","133":"P42/nbc","134":"P42/nnm","135":"P42/mbc","136":"P42/mnm","137":"P42/nmc","138":"P42/ncm","139":"I4/mmm","14":"P121/c1","140":"I4/mcm","141":"I41/amd","142":"I41/acd","143":"P3","144":"P31","145":"P32","146":"H3","147":"P-3","148":"H-3","149":"P312","15":"C12/c1","150":"P321","151":"P3112","152":"P3121","153":"P3212","154":"P3221","155":"H32","156":"P3m1","157":"P31m","158":"P3c1","159":"P31c","16":"P222","160":"H3m","161":"H3c","162":"P-31m","163":"P-31c","164":"P-3m1","165":"P-3c1","166":"H-3m","167":"H-3c","168":"P6","169":"P61","17":"P2221","170":"P65","171":"P62","172":"P64","173":"P63","174":"P-6","175":"P6/m","176":"P63/m","177":"P622","178":"P6122","179":"P6522","18":"P21212","180":"P6222","181":"P6422","182":"P6322","183":"P6mm","184":"P6cc","185":"P63cm","186":"P63mc","187":"P-6m2","188":"P-6c2","189":"P-62m","19":"P212121","190":"P-62c","191":"P6/mmm","192":"P6/mcc","193":"P63/mcm","194":"P63/mmc","195":"P23","196":"F23","197":"I23","198":"P213","199":"I213","2":"P-1","20":"C2221","200":"Pm-3","201":"Pn-3","202":"Fm-3","203":"Fd-3","204":"Im-3","205":"Pa-31","206":"Ia-31","207":"P432","208":"P4232","209":"F432","21":"C222","210":"F4132","211":"I432","212":"P4332","213":"P4132","214":"I4132","215":"P-43m","216":"F-43m","217":"I-43m","218":"P-43n","219":"F-43c","22":"F222","220":"I-43d","221":"Pm-3m","222":"Pn-3n","223":"Pm-3n1","224":"Pn-3m1","225":"Fm-3m","226":"Fm-3c","227":"Fd-3m1","228":"Fd-3c1","229":"Im-3m","23":"I222","230":"Ia-3d1","24":"I212121","25":"Pmm2","26":"Pmc21","27":"Pcc2","28":"Pma2","29":"Pca21","3":"P121","30":"Pnc2","31":"Pmn21","32":"Pba2","33":"Pna21","34":"Pnn2","35":"Cmm2","36":"Cmc21","37":"Ccc2","38":"Amm2","39":"Abm2","4":"P1211","40":"Ama2","41":"Aba2","42":"Fmm2","43":"Fdd2","44":"Imm2","45":"Iba2","46":"Ima2","47":"Pmmm","48":"Pnnn","49":"Pccm","5":"C121","50":"Pban","51":"Pmma1","52":"Pnna1","53":"Pmna1","54":"Pcca1","55":"Pbam1","56":"Pccn1","57":"Pbcm1","58":"Pnnm1","59":"Pmmn1","6":"P1m1","60":"Pbcn1","61":"Pbca1","62":"Pnma1","63":"Cmcm1","64":"Cmca1","65":"Cmmm","66":"Cccm","67":"Cmma","68":"Ccca","69":"Fmmm","7":"P1c1","70":"Fddd","71":"Immm","72":"Ibam","73":"Ibca1","74":"Imma1","75":"P4","76":"P41","77":"P42","78":"P43","79":"I4","8":"C1m1","80":"I41","81":"P-4","82":"I-4","83":"P4/m","84":"P42/m","85":"P4/n","86":"P42/n","87":"I4/m","88":"I41/a","89":"P422","9":"C1c1","90":"P4212","91":"P4122","92":"P41212","93":"P4222","94":"P42212","95":"P4322","96":"P43212","97":"I422","98":"I4122","99":"P4mm"}


parser.add_option("-a", "--anom_weight", dest="anomweight", help="Weighting for anomalous part of the fitness",  type="float",default = 0.0)  # 0-1
parser.add_option("-b", "--anomalous_on", dest="anom_on", help="Separate anomalous pairs") 
parser.add_option("-c", "--correlation_cutoff", dest="correlation_cutoff")  # Minimum average correlation.  If a dataset has an average CC below this value, it will be rejected
parser.add_option("-d", "--multiplicity_weight", dest="multiplicityweight",  type="float",default= 0.0)  # 0-1
parser.add_option("-e", "--equilibrate_scales_to_R", dest="equilibrate_scales_to_R")  
parser.add_option("-f", "--find_files", dest="find_files", default=False )  # 0-1
parser.add_option("-g", "--generations", dest="generations")  # 0-1
# no -h
parser.add_option("-i", "--i_sigma_weight", dest="isigweight",  type="float",default = 2.0)    # 0-1
parser.add_option("-j", "--resolution_override", dest="user_res_override", help="High Resolution cutoff used during optimisation"  )    # 0-1
parser.add_option("","--output_resolution",dest="output_resolution",help="Resolution cut used for final output as opposed to during the optimisation ")

parser.add_option("-k", "--CChalf_weight",  type="float", dest="cchalf_weight",default=1) 
parser.add_option("-l", "--override_space_group", dest="user_sgn") 
parser.add_option("-m", "--mutation_probability", dest="mprob_user") 
parser.add_option("-n", "--npop", dest="population_size") 
parser.add_option("","--completeness_weight", dest="completeness_weight",  type="float",default = 0.2)  # usually 1/5
parser.add_option("-p", "--groups", dest="groups")  # 0-1
parser.add_option("", "--no_file_delete", action="store_true", dest="nofiledelete", default=False , help="Normally, directories are deleted after XSCALEING. This turns off the deletion of these directories")
parser.add_option("", "--inner_isig_max", dest="inner_isig_max", default=999 , help="Maximum allowable value for the inner I over sig. This option exists because in some cases spurious outlier merges occur")
parser.add_option("", "--CChalf_mode",dest="CChalf_mode",default="overall", help="Which CCHalf bin to use -- 'overall' or 'outer'")
parser.add_option("", "--I_sigma_mode",dest="I_sigma_mode",default="overall", help="Which I over Sigma bin to use -- 'overall' or 'outer'")
parser.add_option("", "--anom_mode",dest="ano_mode",default="AnoCC_overall", help="Anomalous mode  -- 'SigAno_inner' or 'SigAno_overall' or 'AnoCC_inner' or 'AnoCC_overall' or 'AnoCC_resolution'")
parser.add_option("-r", "--r_weight", dest="rweight",  type="float",default=1.0)  # 0-1
parser.add_option("-s", "--weighting_scheme", dest="weighting_scheme")  
parser.add_option("-v", "--sad_reference", dest="sad_reference") 
parser.add_option("-x", "--crossover_probability", dest="cxprob_user")
parser.add_option("-y", "--best_or_total", dest="best_or_total") 
parser.add_option("-z", "--sad_phase", dest="sad_phase") 
############ SIR
parser.add_option("", "--sir_phenix_stats", dest="sir_phenix_stats", default=0)  # 0-1
parser.add_option("", "--sir_xprep_stats", dest="sir_xprep_stats", default=0)  # 0-1
parser.add_option("-w", "--swap_SIR_datasets", dest="swap_SIR_datasets") 
parser.add_option("-t", "--SIR_directory1", dest="sir1")  
parser.add_option("-u", "--SIR_directory2", dest="sir2")  
parser.add_option("-q", "--SIR_rvalue_cutoff", default=9999999,dest="SIR_rvalue", help="Inner Rvalue cutoff for SIR comparison to occur")
parser.add_option("", "--SIR_inner_isig_cutoff", dest="SIR_inner_isig_cutoff", default=-2, help="Minimum I over sigma value for the inner shell that will be considered ") 
parser.add_option("-1", "--sir_no_overlap", dest="sir_no_overlap") 
parser.add_option("-2", "--sir_res_limit", dest="sir_res_limit", default="3") 
parser.add_option("-3", "--sir_individual_stat_weight", dest="sir_individual_stat_weight", default=0.1) 
parser.add_option("-4", "--sir_iso_weight", dest="sir_iso_weight", default=1) 
parser.add_option("-5", "--sir_use_shelxd", dest="sir_use_shelxd", default=0) 
parser.add_option("-6", "--sir_use_shelxd_signal_cutoff", dest="sir_use_shelxd_signal_cutoff", default=1.0, help="The signal criteria which must be surpassed for SHLEX to actually be run. This can be inner shell difference or slope, for example")
parser.add_option("","--sir_shelxd_cutoff_mode",dest="sir_shelxd_cutoff_mode",default="slope",help="Criteria to be used to determine if SHELXD should be run. In combination with sir_use_shelxd_signal_cutoff. Possible values are inner_differences or slope")


parser.add_option("-7", "--sir_nsites", dest="sir_nsites", default=2)
parser.add_option("-8", "--sir_shelxc_reindexing_workaround", dest="sir_shelxc_reind", default=0, help="SHELXC automatically reindexes incorrectly sometimes. This workaround applies a max or min inner I sig or Rint cutoff")
parser.add_option("", "--sir_shelxc_reindexing_workaround_cutoff_value", dest="sir_shelxc_reind_cutoff_value", default=0)
parser.add_option("", "--sir_shelxc_reindexing_workaround_mode", dest="sir_shelxc_reind_mode", default="Signal_inner", help= "Signal_inner or Rint_inner or Rint_overall")
parser.add_option("-9", "--sir_shelxc_NaN", dest="check_shelxc_NaN", default=0)
parser.add_option("-0", "--sir_XSCALE_diffs", dest="sir_XSCALE_diffs", default=0)
parser.add_option("", "--sir_signal_inner_weight", dest="sir_signal_inner_weight", default=100.0, help = "Weight of the inner d'/sigD' in SIR scoring ")
parser.add_option("", "--sir_inverse_R_int_weight", dest="sir_inverse_R_int_weight", default=1, help = "Weight of the Rint in SIR scoring) ")  # 0-1
parser.add_option("", "--sir_max_resolution_difference_weight", dest="sir_max_resolution_difference_weight", default=0, help = "Weight of the maximum resolution at which d/sigD > 1 in SIR scoring")  # 0-1
parser.add_option("","--sir_scoring_method",dest="sir_scoring_method",default="difference_plot_slope",help="sum: sum all terms. ratio:inner diffs/Riso_inner. total_ratio total_diffs/totalR rsquared_ratio Rsquared/Riso_inner rsquared_total_ratio Rsquared/totalRDEFAULT sum difference_plot_slope slope of differences by linear regression") 
parser.add_option("", "--sir_shelxd_weight", dest="sir_shelxd_weight", default=1)
parser.add_option("", "--sir_signal_plot_low_resolution_cutoff", dest="sir_signal_plot_low_resolution_cutoff", default=3.5,help="This resolution cutoff is appllied before linear regression is used to fit the differences over sig differences to a line. It is important because of the hump in the wilson plot around 2.5A")
parser.add_option("","--sir_signal_plot_weight",dest="sir_signal_plot_weight",default=1000,help="Signal plot weight in fitness function")

parser.add_option("","--sir_riso_inner_weight",dest="sir_riso_inner_weight",default=1,help="weight of Riso inner in SIR scoring")

#parser.add_option("","--sir_signal_slope_weight",dest="sir_signal_slope_weight",default=1,help="Signal plot weight of slope of differences/resolution")
#parser.add_option("","--sir_signal_rsquared_weight",dest="sir_signal_rsquared_weight",default=1,help="Signal plot weight of the rsquared of the linear regression fit to the plot of signal vs res")
parser.add_option("","--sir_signal_rsquared_exponent",dest="sir_signal_rsquared_exponent",default=10,help="Rsquared raised to this exponent. 10 by default to throw away poor fits")
parser.add_option("","--sir_slope_threshold_overallriso",dest="sir_slope_threshold_overallriso",default=0.5,help="Slope of difference plot at which to start including Riso overall")
parser.add_option("","--sir_riso_threshold_overallriso",dest="sir_riso_threshold_overallriso",default=15,help="Risos to include in the fitness function in conditional Riso mode")
parser.add_option("","--sir_riso_overall_base",dest="sir_riso_overall_base",default=1.0,help="Base for Riso function. This part of the fitness function is calculated as base^(R_iso_overall - R_iso_offset). Defaults to 1. The smaller the number, the steeper the slope i.e. large Riso values will contribute less to the fitness. Do not use bases >=1! ")
parser.add_option("","--sir_slope_offset",dest="sir_slope_offset",default=1.0,help="Offset to difference slope ")
parser.add_option("","--sir_score_combination_mode",dest="sir_score_combination_mode",default="sum",help="How individual nad isomorphous scores are combined: sum or product")
parser.add_option("","--sir_individual_fitness_mode",dest="sir_individual_fitness_mode",default="minimum",help="How fitness is calculated for the two datasets that are used for SIR. Minimum takes the lowest score between the native and derivative. multiply takes the product of the fitness of the native and derivative. Sum is the sum of the two fitnesses. Average is the average value ")


parser.add_option("","--xscale_corrections", dest="xscale_corrections", default="ALL")
parser.add_option("", "--MR_pdb", dest="MR_pdb", help = "Molecular replacement PDB file")
parser.add_option("", "--MR_cif", dest="MR_cif", help = "Molecular replacement CIF file for ligands in MR pdb file") 

parser.add_option("", "--blob_weight",dest="blob_weight",default=0, help= "Blob score weight")
parser.add_option("", "--blob_position", dest="blob_position",help= "x y z coordinates of the region of interest comma separated")
parser.add_option("", "--blob_distance", dest="blob_distance",default=5, help= " Distance cutoff from blob center to user blob position")


parser.add_option("","--hall_of_fame_number", dest="hall_of_fame_number", default=3)
        
parser.add_option("", "--crystfel",dest="crystfel_mode",default=0, help= "Crystfel mode, for single images")
parser.add_option("", "--streamfile",dest="crystfel_streamfile", help= "Crystfel stream file")

        
#ano_mode = "SigAno_Inner"  # Resolution or SigAno_Inner
#ano_mode = "SigAno_Inner"  # Resolution or SigAno_Inner
rmtree= True
#rmtree= False

statfile = "statfile.txt"
overall_statfile = "overall_statfile.txt"
fitness_values = "fitness.txt"

SF2 = open(overall_statfile,"w")
SF2.write("Rmeas_inner,R_score,I_score,Anom_score,Completeness_overall,CC_score,Multiplicity_overall \n")
SF2.close()





FI = open(fitness_values,"w")
FI.write("Fitness1,Fitness2,Indiv_score,IsoScore,TotalScore\n")
FI.close()
          
#parser.add_option("-p", "--parameter_for_optimisation", dest="objective")  # "A=anom", R=r_inner, I=Isig_inner





(optionsG, args) = parser.parse_args()

crystfel_mode = optionsG.crystfel_mode
crystfel_streamfile = optionsG.crystfel_streamfile

if (crystfel_streamfile):
    crystfel_mode = 1

blob_position = optionsG.blob_position
blob_weight = optionsG.blob_weight
blob_distance = optionsG.blob_distance
MR_pdb = optionsG.MR_pdb
MR_cif = optionsG.MR_cif

blob_x=0.0
blob_y=0.0
blob_z=0.0


starting_dir =  os.getcwd()

if (blob_position):
    items = re.split(',',blob_position)
    try:
        blob_x  = float(items[0])
    except ValueError:
        print("Error extracting blob X position")
    try:
        blob_y  = float(items[1])
    except ValueError:
        print("Error extracting blob Y position")
    try:
        blob_z  = float(items[2])
    except ValueError:
        print("Error extracting blob Z position")



if (MR_pdb):
    if (not os.path.exists(MR_pdb)):
        print("PDB file for MR does not exist "+MR_pdb)
        os._exit(1)



nofiledelete = optionsG.nofiledelete
if (nofiledelete is True):
    rmtree = False
    print("Will NOT delete temp directories -- be careful because this will make a lot of files")
elif (nofiledelete is False):
    rmtree=True
    print("Will delete tmp directories")




output_resolution =optionsG.output_resolution



CChalf_mode =optionsG.CChalf_mode
ano_mode =optionsG.ano_mode
I_sigma_mode =optionsG.I_sigma_mode
inner_isig_max = optionsG.inner_isig_max
xscale_corrections = optionsG.xscale_corrections
sir_use_shelxd = optionsG.sir_use_shelxd
sir_nsites = optionsG.sir_nsites
sir_use_shelxd_signal_cutoff = optionsG.sir_use_shelxd_signal_cutoff
sir_shelxd_cutoff_mode = optionsG.sir_shelxd_cutoff_mode
sir_no_overlap = optionsG.sir_no_overlap
sir_res_limit = optionsG.sir_res_limit

sir_scoring_method=optionsG.sir_scoring_method
sir_signal_inner_weight=optionsG.sir_signal_inner_weight
sir_inverse_R_int_weight=optionsG.sir_inverse_R_int_weight
sir_max_resolution_difference_weight=optionsG.sir_max_resolution_difference_weight
sir_shelxd_weight=optionsG.sir_shelxd_weight
sir_signal_plot_low_resolution_cutoff = optionsG.sir_signal_plot_low_resolution_cutoff
sir_signal_plot_weight = optionsG.sir_signal_plot_weight
sir_riso_inner_weight = optionsG.sir_riso_inner_weight
#sir_signal_slope_weight =optionsG.sir_signal_slope_weight
#sir_signal_rsquared_weight =optionsG.sir_signal_slope_weight
sir_signal_rsquared_exponent =int(optionsG.sir_signal_rsquared_exponent)
sir_slope_threshold_overallriso = optionsG.sir_slope_threshold_overallriso
sir_riso_threshold_overallriso = optionsG.sir_riso_threshold_overallriso
sir_individual_fitness_mode = optionsG.sir_individual_fitness_mode
sir_slope_offset = optionsG.sir_slope_offset
sir_score_combination_mode = optionsG.sir_score_combination_mode
sir_riso_overall_base = float(optionsG.sir_riso_overall_base)
if (sir_riso_overall_base >1):
    print("A base of >=1  means that higher R values will get larger scores -- this is almost certainly not what you want.")
    exit(1)

sir_individual_stat_weight = optionsG.sir_individual_stat_weight
sir_iso_weight = optionsG.sir_iso_weight
sir_shelxc_reind = optionsG.sir_shelxc_reind
sir_shelxc_reind_cutoff_value = optionsG.sir_shelxc_reind_cutoff_value
sir_shelxc_reind_mode = optionsG.sir_shelxc_reind_mode
sir_phenix_stats = optionsG.sir_phenix_stats
sir_xprep_stats = optionsG.sir_xprep_stats
#sir_difference_scoring = optionsG.sir_difference_scoring

check_shelxc_NaN = optionsG.check_shelxc_NaN

sir_XSCALE_diffs = optionsG.sir_XSCALE_diffs

user_res_override = optionsG.user_res_override
if user_res_override:
    print("User resolution "+user_res_override)
groups = optionsG.groups
if (not groups):
    groups = 3
    print("Setting groups to be "+str(groups))


if ((not output_resolution) and (user_res_override )):
    output_resolution = user_res_override


hall_of_fame_number =optionsG.hall_of_fame_number
    
    
swap_SIR_datasets = optionsG.swap_SIR_datasets
correlation_cutoff = optionsG.correlation_cutoff

weighting_scheme = optionsG.weighting_scheme
find_files = optionsG.find_files

#####################################################################
r_weight = optionsG.rweight
cchalf_weight = optionsG.cchalf_weight
print("CCHALFW "+str(cchalf_weight))
completeness_weight = optionsG.completeness_weight



isig_weight = optionsG.isigweight
anom_weight = optionsG.anomweight
multiplicity_weight = optionsG.multiplicityweight
#####################################################################




    

user_sgn = optionsG.user_sgn
anom_on = optionsG.anom_on
sad_phase = optionsG.sad_phase
sad_reference = optionsG.sad_reference

r_value_inner_max = optionsG.SIR_rvalue
I_sig_inner_min = optionsG.SIR_inner_isig_cutoff
best_or_total = optionsG.best_or_total
sir1 = optionsG.sir1
sir2 = optionsG.sir2

if (not best_or_total):
    best_or_total = "best"
if (not swap_SIR_datasets):
    swap_SIR_datasets = 0
#if (not anom_weight):
#    if (anom_on):
#        anom_weight=1
#    else:    
#        anom_weight = 0.0


#if (not anom_on):
#    anom_on = 0
#if (not sad_phase):
#    sad_phase = 0


#if (sir1 and not user_sgn):
#    print "You must specify the space group number in SIR mode"
#    os._exit(0)

if (not user_sgn):
    user_sgn = 0
else:
    print("User specified Space Group number is "+str(user_sgn))



if weighting_scheme is None:
#    weighting_scheme="one_over_x"
    weighting_scheme="linear"

config_file = "weeded_files.txt"


sgname=""
if user_sgn in sgdict:
    sgname = sgdict[user_sgn]
else:
    print("Uh-oh. no number associated with the space group "+str(user_sgn))


#if (not objective):
#    objective = "I"
groups = int(groups)


#if (sir1):
#    groups = 3
#    print "Setting groups to be "+str(groups)
#if (len(sys.argv) == 1  ):
#    print "you didn't specify any filenames or options.  use -h for help"
#    os._exit(0)

if (sad_phase == 1):
    print("SAD phasing mode on")
else:
    print("SAD phasing mode off")






#xdsfileString = str(options.xdsfiles)
#xdsfiles = xdsfileString.split(" ")


def phenix_iso_stats(nat_der):
    os.system("/opt/pxsoft/bin/phenix.reflection_statistics --bins=10 "+nat_der[0]+" "+nat_der[1]+" >phenix_ISO_log.txt")
    PL = open("phenix_ISO_log.txt")
    lines = PL.readlines()

    # CC Obs 1 2  0.969  h,k,l
    # Correlation of:
    # 1: native.hkl:iobs,sigma_iobs,merged
    # 2: derivative.hkl:iobs,sigma_iobs,merged
    # Overall correlation:  0.969
    # unused:         - 29.6008 [    0/70    ]
    # bin  1: 29.6008 -  2.5848 [47982/113216]  0.963
    # bin  2:  2.5848 -  2.0518 [48362/113262]  0.962
    # bin  3:  2.0518 -  1.7925 [44438/113352]  0.938
    # bin  4:  1.7925 -  1.6286 [46708/113154]  0.889
    # bin  5:  1.6286 -  1.5119 [41272/113268]  0.817
    # bin  6:  1.5119 -  1.4227 [41378/113420]  0.730
    # bin  7:  1.4227 -  1.3515 [39202/113080]  0.607
    # bin  8:  1.3515 -  1.2927 [39048/113526]  0.547
    # bin  9:  1.2927 -  1.2429 [31210/113174]  0.486
    # bin 10:  1.2429 -  1.2000 [16272/113064]  0.393
    # unused:  1.2000 -         [    0/0     ]


    startCC  = re.compile('CC Obs 1 2\s+(\d+\.\d+)\s+h,k,l')
    lastbin  = re.compile('bin 10:\s+(\d+\.\d+) -\s+(\d+\.\d+) \[(\d+)\/(\d+)\]\s+(\d+\.\d+)')
    Resolutions = []
    highres_CC = 0.0
    for i in range(len(lines)):
        item = lines[i]
        if startCC.search(item):
            for j in range(i+14,len(lines)):  # we could jump to the correct line directly, but just in case ...
                item2 = lines[j]
                if lastbin.search(item2):
                    matches = lastbin.search(item2)
                    CC = matches.group(5)
                    try:
                        highres_CC  = float(CC.strip())
                    except ValueError:
                        print("Error extracting high res CC")

    return highres_CC
        
        
        


def reflection_files_to_cell_and_sg (xdsfiles,mode):
    xdsfiles_by_sgn = [None]*230  # list of lists
    sgdict = {}
    aveCellParams = {}  # dict keyed on sg number
    aveCellParamsFinal = {}  # dict keyed on sg number
    mcell = " "
    weeded_xdsfiles = []

    if len(xdsfiles) == 0 :
        print("No XDS files have been specified. Is there a config file? If not, you need to start the program in -f mode to find reflection files")
        exit(1)
    cell_values= numpy.zeros((6,len(xdsfiles)-1))


    passes = [0,1]
    
#    if (crystfel_mode):
#        (cells,foundcell) = cell_and_sg_from_crystfel_stream (crystfel_streamfile)
#        passes = [1]

#    for passnumber in (0,1):
    for passnumber in passes:
        # first pass is to calculate average
        # then we run again to remove any outliers


        # if this is pass 1 (second pass) calculate  the medians
        medians = numpy.zeros(6)
        if (passnumber ==1):
            medians=numpy.median(cell_values,axis=1)
            print("MEDIAN CELL VALUES After first pass (all data files included) ")
            print(medians)


        filecount = 0
        for xf in xdsfiles:
            filecount += 1
            SGN = 1
            cell = numpy.zeros((6,len(xdsfiles)-1))

            if (crystfel_mode):
                pass
            else:
                (SGN, cell) = cell_and_sg_from_XDS_ASCII (xf)

                

            cellpars = cell.split()
            if (not SGN):
                if (user_sgn):
                    SGN= user_sgn
                else:    
                    SGN  = 1
            if (sgdict.get(SGN)):
                sgdict[SGN] = sgdict[SGN] +1

                if (xdsfiles_by_sgn[int(SGN)]):
                    xdsfiles_by_sgn[int(SGN)].append(xf)
                else:
                    xdsfiles_by_sgn[int(SGN)]=[xf]
            else:
                sgdict[SGN] = 1
                xdsfiles_by_sgn[int(SGN)]=[xf]
            if aveCellParams.get(SGN):    
                cp = aveCellParams[SGN]
                newcp = []
                cell_params_ok = True
                # check that these are not too different
                # note that cellpars is the current value
                # and cp is the average value
                # this is only done on the second pass in case early datasets have outlier cell params (which would cause all the
                # datasets with OK cell params to be rejected 
                if (passnumber == 1):
                    for it in range(0,len(cp)):
                        if (  (abs (float(medians[it]) -float(cellpars[it]))/float(medians[it])) >0.2):
                            cell_params_ok = False

                if cell_params_ok:
                    print("Including this dataset: "+str(cell))

                    for it in range(0,len(cp)):
                        nval = "{0:.2f}".format((float(cp[it])+float(cellpars[it]))/2)
                        newcp.append(nval)
                        cell_values[it][filecount-2]=cellpars[it]
                    aveCellParams[SGN]=newcp

                    if (passnumber == 1):
                        weeded_xdsfiles.append(xf)
                else:
                    print("Excluding this dataset: "+str(cell)+ "   *******")
            else:
                aveCellParams[SGN]=cellpars


#        print cell_values        
        for sgn in list(sgdict.keys()):
            cstr = " "
            print("Average cell params "+cstr.join(map(str,aveCellParams[sgn])))
                
    realSG="1"
    maxcount =0


    for sgn in list(sgdict.keys()):
        print("Space group "+str(sgn)+" was seen "+str(sgdict[sgn]/2)+" times")
        if sgdict[sgn] >maxcount:
            realSG= sgn
            mcell = mcell.join(map(str,aveCellParams[sgn]))
            maxcount = sgdict[sgn]
            
    SGN=realSG
    if (user_sgn):
        print("You have specified a space group number of "+str(sgn))
        SGN=user_sgn
    else:
        print("Chose SGN :"+str(SGN)+" cell "+str(mcell))
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("IT IS IMPORTANT TO CHECK THE ABOVE VALUES!!!")
    print("Number of XDS files before weeding out different space groups: "+str(len(xdsfiles)))
    xdsfiles = xdsfiles_by_sgn[int(SGN)]
    print("Number of XDS files after weeding out different space groups: "+str(len(weeded_xdsfiles)))
    return (SGN,mcell,weeded_xdsfiles)
    




def write_config_file(list1, fname,sgn,cell):
    XDSFILES = open(fname,"w")
    XDSFILES.write(str(SGN)+"\n")
    XDSFILES.write(cell+"\n")
    for xf in list1:
        XDSFILES.write(str(xf)+"\n")
    XDSFILES.close()


def setup_diff_files(native,derivative):

    unique_dir = "tmpSHELX"+str(uuid.uuid4())
    if (not os.path.exists(unique_dir)):
        os.mkdir(unique_dir)
    os.chdir(unique_dir)    

    INFO = open("files.txt","w")
    INFO.write("NATIVE:"+native+"\n")
    INFO.write("DERIVATIVE:"+derivative+"\n")
    INFO.write("../"+native+"\n")
    INFO.write("../"+derivative+"\n")
    INFO.write("../"+native+"/XSCALE.LP\n")
    INFO.write("../"+derivative+"/XSCALE.LP\n")

    
    INFO.close()

    os.symlink("../"+native+"/merged.hkl", "native.hkl")
    os.symlink("../"+derivative+"/merged.hkl", "derivative.hkl")
    return(unique_dir)

def xprep_parse(native,derivative,cell,sg):
    startdir =  os.getcwd()
    unique_dir = setup_diff_files(native,derivative)
    runstring = "/opt/pxsoft/bin/get_xprep_rip_stats.pl native.hkl derivative.hkl \'"+cell+"\' "+sg+" >xprep.log"
    print("XP"+runstring)
    os.system(runstring)
    signal = 0.0
    inner_signal = 0.0
    R_local_scaled = 99999.0
    XPL = open("xprep.log")
    nlines = XPL.readlines()
    for i in range(len(nlines)):
        item = nlines[i]
        items = re.split("\s+",item)
#        print "IT 0 "+items[0]
        if (items[0] == "ISIGI"):
            signal = float(items[1])
        if (items[0] == "R_local_scaled"):
            R_local_scaled = float(items[1])
        if (items[0] == "ISIGI_INNER"):
            inner_signal = float(items[1])
    os.chdir(startdir)
    print("SIGNAL "+str(signal)+" C "+str(R_local_scaled))
    XPL.close()
    return (signal,inner_signal, R_local_scaled,unique_dir)

    
def shelxc_parse(native, derivative, cell, sg) :
    startdir =  os.getcwd()
    unique_dir= setup_diff_files(native,derivative)
    
    SX = open("shelxc.inp","w")
    SX.write("CELL "+cell+"\n")
    SX.write("SPAG "+sg+"\n")
#    SX.write("NORI\n")


    SX.write("SHEL 500 "+str(sir_res_limit)+"\n")
#    SX.write("SHEL 500 2\n")

    if (int(swap_SIR_datasets) == 1):
        SX.write("NAT derivative.hkl\n")
        SX.write("RIPA native.hkl\n")
    else:
        SX.write("NAT native.hkl\n")
        SX.write("RIPA derivative.hkl\n")
    SX.write("SFAC S\n")
#    SX.write("FIND 4\n")
    SX.write("FIND "+str(sir_nsites)+"\n")
#    SX.write("DSUL 3\n")

    SX.write("NTRY 1500\n")
    SX.close()
       # now iterate until I/sig or CC reach some target 
#    os.system("/data/id23eh2/inhouse/opid232/shelxc.bin RIP <shelxc.inp >shelxc.log")
    os.system("/opt/pxsoft/bin/shelxc RIP <shelxc.inp >shelxc.log") 
#    stdout=Proc('/opt/pxsoft/bin/shelxc RIP <shelxc.inp').call(timeout=60).stdout
#    XSI = open("shelxc.log","w")
#    XSI.write(stdout)

    if (sad_reference is not None ):
        os.system("cp "+str(sad_reference)+" RIP.ent")

#        stdout=Proc('/opt/pxsoft/bin/anode_mp RIP').call(timeout=60).stdout

        os.system("/opt/pxsoft/bin/anode RIP")


    # <d'/sig>   3.85  3.50  3.70  4.02  3.75  3.61  3.06  2.22  1.65  1.27  1.01
    # R(isom)   0.141 0.140 0.146 0.155 0.149 0.156 0.159 0.169 0.182 0.228 0.300

    diffLine  = re.compile('\<d\'\/sig\>\s+(\d+.\d+.*)')
    resLine = re.compile('Resl\.');
    # Fixed width 
    # R(isom)   0.081 0.094 0.111 0.124 0.135 0.168 0.226 0.334 4.71338.53487.747
#    RLine  = re.compile('R\(isom\)\s+(\d+.\d+.*)')
#    RLine  = re.compile('R\(isom\)\s+(\d{2}\.\d{3})\s+(\d{2}\.\d{3})')

    SHELXC_NaN = re.compile('SIR data re-indexed to improve CC against NAT from   NaN% to   NaN%')
    SHELXC_NaN_flag = False
    OverallR = re.compile('R\(int\) \=\s*(\d+\.\d+)')
#    RLine  = re.compile('R\(isom\)  (.{2}\.\d{3})(.{2}\.\d{3})(.{2}\.\d{3})(.{2}\.\d{3})(.{2}\.\d{3})(.{2}\.\d{3}|\*{6})(.{2}\.\d{3}|\*{6})(.{2}\.\d{3}|\*{6})(.{2}\.\d{3}|\*{6})(.{2}\.\d{3}|\*{6})(.{2}\.\d{3}|\*{6})')

    RLine  = re.compile('R\(isom\)\s+(\d+\.\d+.*)')

    XSO = open("shelxc.log")
    lines = XSO.readlines()
#    lines = stdout
    total_Rvalue = 0.0
    total_signal = 0.0
    lowRes_Rvalue = 999.0
    inner_signal = 0.0
    Overall_Rvalue = 999.0
    max_res_by_diffs = 10.0
    Resolutions = []
    diffs = []
    
    for i in range(len(lines)):
        item = lines[i]
        if resLine.search(item):
            Resolutions = re.split("\s+",item)
        if diffLine.search(item):
            matches = diffLine.search(item)
            diff = matches.group(1)
            diffs = re.split("\s+",diff)
            scount=0
            for x in diffs:
                stripped = 0.0
                try:
                    stripped = float(x.strip())
                except ValueError:
                    stripped = 0.0
                if (stripped != ""):
                    total_signal = total_signal + stripped
                    scount = scount +1
                    if (stripped > 1.0):
                        max_res_by_diffs=Resolutions[scount+1]
                        if (re.search('Inf\.',max_res_by_diffs)):
                            max_res_by_diffs = 100.0
                    if (scount == 1):
                        inner_signal = stripped
        if OverallR.search(item):
            matches = OverallR.search(item)
            Overall_Rvalue = matches.group(1)
        if RLine.search(item):
            matches = RLine.search(item)
            rl = matches.group(1)
            rls = re.split("\s+",rl)
            rcount=0
            for x in rls:
                stripped = 1.0
                try:
                    stripped = float(x.strip())
                    if  (stripped == "******") :
                        stripped = 1.0
                except ValueError:
                    stripped = 1.0
                if (stripped != ""):
                    total_Rvalue = total_Rvalue +stripped
                    rcount = rcount +1
                    if (rcount == 1):
                        lowRes_Rvalue = stripped
#                print "Riso "+str(x)+" stripped_Riso "+str(stripped)
#                print "Riso "+str(stripped)
        if SHELXC_NaN.search(item):
           SHELXC_NaN_flag = True
           print("NaN detected in shelxc auto reindexing ")
    if (total_Rvalue == 0):
        total_Rvalue = 999

    #    
    # get an R value and possible BIC to linear regression fit
    #

    print("      Getting Rsquared")
    firstbin = 0
    lastbin = len(Resolutions)
    print("RR "+str(Resolutions))
    print("RR "+str(diffs))
    for i in range(3,len(Resolutions)):
        if (re.search('\d+',Resolutions[i])):
            if ( float(Resolutions[i]) >= float(sir_signal_plot_low_resolution_cutoff)):
                firstbin  = i
            lastbin = i
    trimmed_res = Resolutions[firstbin:lastbin]
    trimmed_diffs = diffs[firstbin-3:lastbin-3]

    res_string = ",".join(trimmed_res)
    diff_string = ",".join(trimmed_diffs)

    print("res string "+res_string)
    print("diff string "+diff_string)
    
    LFIT = open("differences_fit_R.inp","w")
    LFIT.write("options(scipen=999)\n")
    LFIT.write("par(bg = 'white')\n")
    LFIT.write("signal <- c("+diff_string+")\n")
    LFIT.write("res <- c("+res_string+")\n")
    LFIT.write("linearMod <- lm(signal ~ res)\n")
    LFIT.write("linearMod\n")
    LFIT.write("plot(res,signal,xlim=c(5,1))\n")
    LFIT.write("abline(linearMod)\n")
    LFIT.write("summary(linearMod)\n")
    LFIT.write("BIC(linearMod)\n")
    LFIT.close()
    os.system("/usr/bin/R --no-save <differences_fit_R.inp >differences_fit_R.log")

    LF = open("differences_fit_R.log")
    lines = LF.readlines()
    Rsquared_regex  = re.compile('Multiple R-squared:\s+(\d+\.\d+)\,')
    Res_std_error_regex  = re.compile('Residual standard error:\s+(\d+\.\d+) on')
    BIC_regex  = re.compile('\[1\]\s+(\S+)')
    slope_regex = re.compile('^res\s+(\d+\.\d+)\s+') 
    Rsquared = 0.0
    slope = 0.0
    res_std_error = 0
    rescount = 0
    BIC = 0.0
    for i in range(len(lines)):
        item = lines[i]
        if Res_std_error_regex.search(item):
            matches = Res_std_error_regex.search(item)
            res_std_error =float(matches.group(1))
        if Rsquared_regex.search(item):
            matches = Rsquared_regex.search(item)
            Rsquared = float(matches.group(1))
        if BIC_regex.search(item):
            matches = BIC_regex.search(item)
            BIC = float(matches.group(1))
        if slope_regex.search(item):
            matches = slope_regex.search(item)
            if (rescount ==0):
                slope = float(matches.group(1))
                rescount= rescount+1
                
    #
    #
    #

    
    shelxd_score = 0.0
    if (sir_use_shelxd):
        print("Use SHELXD mode")
#    Criteria unmet for SHELXD... Mode slope inner score 2.21 slope 0.800616cutoff 1.0
#    print " Criteria unmet for SHELXD... Mode "+sir_shelxd_cutoff_mode+" inner score "+str(inner_signal)+ " slope "+str(slope)+"cutoff "+str(sir_use_shelxd_signal_cutoff)
            
        if ((sir_shelxd_cutoff_mode == "slope" and float(slope) > float(sir_use_shelxd_signal_cutoff)) or (sir_shelxd_cutoff_mode == "inner_signal" and float(inner_signal) >float(sir_use_shelxd_signal_cutoff))): 
#        if ( float(inner_signal) > float(sir_use_shelxd_signal_cutoff)):
            shelxd_scoreD = 0.0
            shelxd_scoreN = 0.0
            print("SHELXD mode and signal is OK")
            os.system("/opt/pxsoft/bin/codgas_reverse_anode.pl noanode")


#            os.system("/opt/pxsoft/bin/shelxd RIP_fa");
#            os.system("/opt/pxsoft/bin/shelxd_to_mclust.pl RIP_fa.lst |grep CC_ >CCs.txt");

            os.system("/opt/pxsoft/bin/shelxd RIPN_fa >/dev/null")
            os.system("/opt/pxsoft/bin/shelxd_to_mclust.pl RIPN_fa.lst |grep CC_ >CCs_ripn.txt");
            os.system("cp SHELXD_max_values.txt RIPN_shelxd_max_values.txt")
            ##########
            CC_dist_patt = re.compile('CC_DIST: (\d+\.\d+)')
            CC_percent_patt = re.compile('CC_PERCENT_DIFF: (\d+)')
            ##########
            CC1 = open("CCs_ripn.txt")
            lines1 = CC1.readlines()
            #
            SHELXD_dist_N=0.0
            SHELXD_percent_N=0.0
            for i in range(len(lines1)):
                item = lines1[i]
                if CC_dist_patt.search(item):
                    matches = CC_dist_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                    SHELXD_dist_N = stripped
                if CC_percent_patt.search(item):
                    matches = CC_percent_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                    SHELXD_percent_N = stripped
            print("SHELXD_dist_N "+str(SHELXD_dist_N)+" SHELXD_percent_N "+str(SHELXD_percent_N))       
 #            shelxd_scoreN = SHELXD_dist_N*SHELXD_percent_N
            shelxd_scoreN = SHELXD_dist_N
#            shelxd_scoreN = 
            os.system("/opt/pxsoft/bin/shelxd RIPD_fa >/dev/null")
            os.system("/opt/pxsoft/bin/shelxd_to_mclust.pl RIPD_fa.lst |grep CC_ >CCs_ripd.txt");
            os.system("cp SHELXD_max_values.txt RIPD_shelxd_max_values.txt")
            CC2 = open("CCs_ripd.txt")
            lines2 = CC2.readlines()

            SHELXD_dist_D = 0.0
            SHELXD_percent_D = 0.0
            for i in range(len(lines2)):
                item = lines2[i]
                if CC_dist_patt.search(item):
                    matches = CC_dist_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                    SHELXD_dist_D = stripped
                if CC_percent_patt.search(item):
                    matches = CC_percent_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                    SHELXD_percent_D = stripped
#            shelxd_scoreD = SHELXD_dist_D*SHELXD_percent_D
            shelxd_scoreD = SHELXD_dist_D
            print("SHELXD_dist_D "+str(SHELXD_dist_D)+" SHELXD_percent_D "+str(SHELXD_percent_D))

            
            # grab SHELXD Best_CC_All: 14.44
            # Best_CC_Weak: 9.82
            # Best_FOM: 24.26
            # SHELXD_max_values.txt
            CC_all_patt = re.compile('Best_CC_All: (\d+\.\d+)')
            CC_weak_patt = re.compile('Best_CC_Weak: (\d+)')
            FOM_patt = re.compile('Best_FOM: (\d+)')
            CCbest= open("SHELXD_max_values.txt")
            CClines = CCbest.readlines()
            CC_all=0.0
            CC_weak=0.0
            FOM=0.0
            for i in range(len(CClines)):
                item = CClines[i]
                if CC_all_patt.search(item):
                    matches = CC_all_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                        CC_all = stripped
                if CC_weak_patt.search(item):
                    matches = CC_weak_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                        CC_weak = stripped
                if FOM_patt.search(item):
                    matches = FOM_patt.search(item)
                    stripped = matches.group(1)
                    try:
                        stripped = float(stripped.strip())
                    except ValueError:
                        stripped = 0.0
                        FOM= stripped
            #
            #
            #

            shelxd_score = float(CC_all)*float(CC_weak)* float(sir_shelxd_weight)
#            if (shelxd_scoreN > shelxd_scoreD):
#                shelxd_score =shelxd_scoreN
#            else:
#                shelxd_score =shelxd_scoreD
        else:
            print(" Criteria unmet for SHELXD... Mode "+sir_shelxd_cutoff_mode+" inner score "+str(inner_signal)+ " slope "+str(slope)+"cutoff "+str(sir_use_shelxd_signal_cutoff))

    os.chdir(startdir    )
    print("Rsquared is "+str(Rsquared))
    print("Slope is "+str(slope))
    return (float(total_Rvalue), float(lowRes_Rvalue), float(total_signal), float(inner_signal),float(Overall_Rvalue), float(shelxd_score), max_res_by_diffs,unique_dir,SHELXC_NaN_flag,Rsquared,slope, res_std_error, BIC )   


                
##############
def build_xdsfile_list(pattern,start_directory,startnumber):
    Fcount = 0
    if os.path.isfile(config_file):
        print("WARNING -- REMOVING CONFIG FILE")
        os.remove(config_file)
    xdsfiles = []
    if find_files:
        Fcount=startnumber;
        print("Find files mode.  Looking for files ending with "+pattern+" in "+start_directory)
        for root, dirs, files in os.walk(str(start_directory),followlinks=True):
            for file in files:
#                if file.endswith("ASCII.HKL"):
#                ext = ["s1.HKL","s2.HKL"]
#                if file.endswith(tuple(ext)):
#                if file.endswith("deisa"):
#                 if file.endswith(pattern):
                 source =  os.path.join(root, file)
                 if re.search(pattern,str(source)):

                    print((os.path.join(root, file)))
                    target = str(Fcount)+".HKL"
                    print("SOURCE file "+str(source)+" TARGET link "+str(target))
                    if os.path.exists(target):
                        print("Hmm... "+target+" already exists")
                        if os.path.islink(target):
                            print("Clobbering link "+target)
                            os.remove(target)
                        else:
                            print("Could not set up links:  the target file "+target+" already exists.  Please delete it!")
                            exit
                    os.symlink(str(source),target)
                    xdsfiles.append(str(Fcount)+".HKL")
                    Fcount = Fcount +1
#    else:
#        xdsfiles = glob.glob("*.HKL")
#        xdsfiles.append(glob.glob("*.hkl"))
    return xdsfiles,Fcount    


######################
def cell_and_sg_from_crystfel_stream (streamfile):
    sgn=""
    cell=""
    #
    #    Cell parameters 7.85618 7.83382 3.95238 nm, 89.54867 90.50763 89.93054 deg
    #
    print("streamfile "+streamfile)
    STRM = open(streamfile)
    UCELL = re.compile('Cell parameters (.*) nm, (.*) deg')
    
    dims=""
    angles=""

    cells = list()
#    cellstrings = list()

    
    found_cell =0
    cell_values= list()
    
    for item in STRM.readlines():
        if UCELL.match(item):
            matches = UCELL.search(item)
            dims=matches.group(1)
            angles=matches.group(2)

            cparams = re.split("\s+",dims)
            cangles = re.split("\s+",angles)
            angstrom_params = [0,0,0,0,0,0]
            angstrom_params_str = ""
            sidecount = 0
            for side in cparams:
#                print("side"+str(side))
                np = float(side )* 10;
#                print( "NP"+str(np))
                angstrom_params[sidecount]=np
                angstrom_params_str = angstrom_params_str+" "+str(np)
                sidecount= sidecount+1

            angstrom_params_str = angstrom_params_str + " "+angles
            angstrom_params[3]=float(cangles[0])
            angstrom_params[4]=float(cangles[1])
            angstrom_params[5]=float(cangles[2])


            npa = numpy.asarray(angstrom_params)
            cells.append(angstrom_params_str)
            cells.append(npa)
            cell_values.append(angstrom_params)
            found_cell=1


    medians_np = numpy.asarray(cell_values)
    medians=numpy.median(medians_np,axis=0)
#    print ("Np median "+str(medians_np))
    print ("Numpy median "+str(medians))

    final_cellstring = ""
    for it in range(0,len(medians)):
        final_cellstring = final_cellstring+" " +"{0:.2f}".format(float(medians[it]))
        
    print ("Final cellstring "+final_cellstring)    
    return (final_cellstring,found_cell)
######################
 
def cell_and_sg_from_XDS_ASCII (xdsfile):
    sgn=""
    cell=""
#    print "XDS FILE "+str(xdsfile)
    XDS = open(xdsfile)
    SG = re.compile('\!SPACE_GROUP_NUMBER=\s+(\d+)')
    UCELL = re.compile('\!UNIT_CELL_CONSTANTS=\s+(.*)')
    for item in XDS.readlines():
        if SG.match(item):
            matches = SG.search(item)
            sgn=matches.group(1)
        if UCELL.match(item):
            matches = UCELL.search(item)
            cell=matches.group(1)
    return (sgn,cell)         
##############


def score_Rvalue(best_Rvalue_inner, weighting_scheme) :
    R_term = 0.0
    if re.search("one",weighting_scheme):
        R_term = ((1.0/best_Rvalue_inner))*100
    elif re.search("sigmoid",weighting_scheme):
        R_sigmoid = 1/(1+math.e**(best_Rvalue_inner-20))
        R_term = R_sigmoid
    elif re.search("linear",weighting_scheme):
        R_term = 100-best_Rvalue_inner
    else:
        R_term = 100-best_Rvalue_inner
    return R_term

###############



######################




def evalOneMax(individual):
    isig_weight=0.0
    anom_weight=0.0
    cchalf_weight=0.0
    completeness_weight=0.0
    multiplicity_weight=0.0
    
    if (shared.getConst('isig_weight')):
        isig_weight=shared.getConst('isig_weight')        
    else:
        isig_weight=shared.getConst('Uisig_weight')
    if (shared.getConst('anom_weight')):    
        anom_weight=shared.getConst('anom_weight')
    else:
        anom_weight=shared.getConst('Uanom_weight')
    if (shared.getConst('cchalf_weight')):    
        cchalf_weight=shared.getConst('cchalf_weight')
    else:
        cchalf_weight=shared.getConst('Ucchalf_weight')
    if (shared.getConst('completeness_weight')):    
        completeness_weight=shared.getConst('completeness_weight')
    else:            
        completeness_weight=shared.getConst('Ucompleteness_weight')
    if (shared.getConst('multiplicity_weight')):    
        multiplicity_weight=shared.getConst('multiplicity_weight')
    else:            
        multiplicity_weight=shared.getConst('Umultiplicity_weight')
    if (shared.getConst('blob_weight')):    
        blob_weight=shared.getConst('blob_weight')
    else:            
        blob_weight=shared.getConst('Ublob_weight')
        
    count = 0
    Isigma_average=0
    Rvalue_average=0
    Anom_average=0
    Res_average=0

#    print "INDIVIDUAL"  
    total_res=0
    total_isig=0
    n_points =0
#    print "  Group summary\n---------------" 


    total_fitness =0
    best_fitness = -9999999
    bests = {}
    average_fitness = -9999999
    for group in range(1,groups+1):
        maxres = 9999
        Isig_inner = -9999999
        Rvalue_inner = 9999999
        SigAno_inner = -9999999
        CCAno_inner = -9999999
        multiplicity_overall  =-9999999
        Rvalue_overall = 9999999
        Isigma_overall = -9999999
        CChalf_overall= -9999999
        CChalf_inner= -9999999
        CChalf_outer= -9999999
        Rmeas_overall= 9999999
        Rmeas_inner= 9999999
        SigAno_overall= -9999999
        CCAno_overall= -9999999
        Completeness_overall = -9999999
        MaxAnomRes= 9999999
        Rvalues= []
        Isigmas = []
        Rmeas= []
        CChalf= []
        resultdir= ""
        newfiles= []
        corr_lookup = []
        
        HKLfilenames=[]
        groupstr=""
        HKLindices = []
        for dataset in range(0,len(xdsfiles)-1):
            
            if ( individual[dataset] == group):
                HKLfilenames.append(xdsfiles[dataset])
                HKLindices.append(dataset)
                groupstr = groupstr +str(group)
            else:
                groupstr = groupstr +"_"    
        if (len(HKLfilenames) ==0): 
            return (-9999999,)
        
        if (crystfel_mode):
#                return (CCAno_inner,overall_ccano,Isig_inner,overall_snr,Rsplit_inner,overall_snr,resolution_bin_above_snr_threshold,unique_dir)
            (CCAno_inner,CCAno_overall,Isig_inner,Isigma_overall,Rvalue_inner,Rvalue_overall,maxres,resultdir) = filenames_to_partialator(HKLindices,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
            print ("CRYSTFEL snr "+str(Isig_inner)+" CCAno "+str(CCAno_inner))
        else:    
            (maxres, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall,CCAno_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,corr_lookup)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)


        
        print("CCANO "+str(CCAno_inner))
        


#        SF = open(str(resultdir)+"/rmeas_CC_inners","a")
#        SF.write("Rmeas_inner "+str(Rmeas[0])+" CC internal "+str(CChalf[0])+"  CChalf_outer "+str(CChalf[len(CChalf)-1]))
#        SF.close

        if (not crystfel_mode):
            if (len(Rmeas) <1):
                return (-9999999,)

            Rmeas_inner = Rmeas[0]
            CChalf_inner = CChalf[0]
            CChalf_outer = CChalf[len(CChalf)-1]
        
        # scoring
#        print "Iweight "+str(isig_weight)
#        print "Aweight "+str(anom_weight)

#        print "MO "+str(multiplicity_overall)
        print(("CChalf mode "+str(CChalf_mode)+" I sigma mode "+str(I_sigma_mode)))

        CC_value=0.0
        if (CChalf_mode == "overall") :
            CC_value = CChalf_overall
        else:
            CC_value = CChalf_outer

        CC_term=CC_value *float(cchalf_weight)
            
        I_value = 0.0    
        if (I_sigma_mode == "overall"):
            I_value = Isigma_overall
        else:
            I_value = Isig_inner

#        CC_term = CChalf_overall*float(cchalf_weight)
#        R_term = score_Rvalue(Rvalue_inner,weighting_scheme)
        R_term_unweighted = score_Rvalue(Rmeas_inner,weighting_scheme)
        R_term = R_term_unweighted *float(r_weight)
        I_term = I_value*float(isig_weight)

        #parser.add_option("", "--Anom_mode",dest="ano_mode",default="SigAno_inner", help="Anomalous mode  -- 'SoigAno_inner' or 'SigAno_overall' or 'AnoCC_inner' or 'AnoCC_overall' or 'AnoCC_resolution'")

        Avalue = 0
        if re.search("Resolution",ano_mode):
            Avalue = MaxAnomRes
        elif re.search("SigAno_inner",ano_mode):
            Avalue= SigAno_inner
        elif re.search("SigAno_overall",ano_mode):
            Avalue = SigAno_overall
        elif re.search("AnoCC_inner",ano_mode):
            Avalue = CCAno_inner
        elif re.search("AnoCC_overall",ano_mode):
            Avalue =CCAno_overall
        else:
            Avalue =SigAno_overall
            
        A_term = Avalue *float(anom_weight)            

        print ("COver "+str(Completeness_overall)+" Cweight "+str(completeness_weight))
        C_term = Completeness_overall*float(completeness_weight)
        M_term = multiplicity_overall*float(multiplicity_weight)
#        print "RT "+str(R_term)+" IT "+str(I_term)+" A_term "+str(A_term)+" C_term "+str(C_term)+" M_term "+str(M_term)
#        print  " R term "+str(R_term)+" I/s(I) term "+str(I_term)+" CChalf term "+str(CC_term)+" Anom term "+str(A_term)+" Comp term "+str(C_term)+" Multiplicity term "+str(M_term)+" R_inner "+str(Rmeas_inner)+" CC_term "+str(CC_term)+" CC_outer "+str(CChalf_outer)+" CC overall "+str(CChalf_outer)

        blob_score = 0.0

        print("currently in "+str(os.getcwd()))
        if ((blob_weight != 0.0 ) and (MR_pdb)):
            print("going into "+resultdir)
            os.chdir(resultdir)
            # convert hkl to mtz 
            os.system("/opt/pxsoft/bin/phenix.reflection_file_converter merged.hkl --mtz=merged.mtz  --write_mtz_amplitudes --generate_r_free_flags --mtz_root_label nat >reflection_convert.log")

            if (os.path.exists(MR_pdb)):
                remove_het(MR_pdb,"nohet.pdb")
            elif (os.path.exists("../"+MR_pdb)):
                remove_het("../"+MR_pdb,"nohet.pdb")
            else:
                print("Major problem: cannot find PDB file!!!\n");
                
            print("   Getting phases...")
#            getphases("nohet.pdb","out.pdb","merged.mtz","phases.mtz",MR_cif)

            rundimple("nohet.pdb","merged.mtz",MR_cif)
            # def getblobscore(pdbfile,mtzfile,blob_position):

#            os.system(findblobs+" "+pdbfile+" "+mtzfile+" >"+blobout)  
            print("  Getting blob score")
#            blob_score = parseblobscore("out.pdb","phases.mtz",blob_position)
            print("__currently in "+str(os.getcwd()))
            logfile = ""
            if (os.path.exists("dmpl/08-find-blobs.log")):
                logfile = "dmpl/08-find-blobs.log"
            elif (os.path.exists("dmpl/09-find-blobs.log")):
                logfile = "dmpl/09-find-blobs.log"
            elif (os.path.exists("dmpl/10-find-blobs.log")):
                logfile = "dmpl/10-find-blobs.log"
            
            blob_score = parsebloblog(logfile)
            print("  changing dir\n")
            os.chdir("../")
            print("going back out to "+str(os.getcwd()))
        B_term = blob_score*blob_weight

        fitn = R_term+I_term+A_term+C_term+M_term+CC_term+B_term

        print ("FIT "+str(R_term)+" I "+str(I_term)+" A "+str(A_term)+" C "+str(C_term)+" M "+str(M_term)+" CC "+str(CC_term))

#        if (crystfel_mode):
#            fitn = 


        
        SF2 = open(overall_statfile,"a")       
        SF2.write(str(Rmeas_inner)+","+str(R_term_unweighted)+","+str(I_value)+","+str(Avalue)+","+str(Completeness_overall)+","+str(CC_value)+","+str(multiplicity_overall)+"\n")
        SF2.close()      

        

        if (float(Isig_inner) > float(inner_isig_max)):   # In data that have had their gimas fiddled with via for example DEISA, we might want to torpedo unrealistically high inner I sigs
            fitn=-999
        
#        print "FIT "+str(fitn)+" BEST fit "+str(best_fitness)

        print( "Iinner"+str(Isig_inner))

        if (fitn >= best_fitness):
            best_fitness = fitn
            bests['res']=str(maxres)
            bests['I_inner']=str(Isig_inner)
            bests['I_term']=str(I_term)
            bests['Rmeas_inner']=str(Rmeas_inner)
            bests['R_term']=str(R_term)
            bests['Completeness_overall']=str(Completeness_overall)
            bests['C_term']=str(C_term)
            bests['Multiplicity_inner']=str(multiplicity_overall)
            bests['M_term']=str(M_term)
            bests['CChalf_outer']=str(CChalf_outer)
            bests['CC_term']= str(CC_term)
            bests['SigAno_overall']=str(SigAno_overall)
            bests['SigAno_inner']=str(SigAno_inner)
            bests['CChalf_overall']=str(CChalf_overall)
        total_fitness = total_fitness + fitn

        print("  "+groupstr +" --> Mres "+str(maxres)+" Fitness "+str(fitn)+", I value: "+str(I_value)+"->Isig score: "+str(I_term)+" Rmrg_inner: "+str(Rmeas_inner)+"->Rscore "+str(R_term)+" Completeness: "+str(Completeness_overall)+"->Completeness_score: "+str(C_term)+" Multiplicity: "+str(multiplicity_overall)+"-> Mult.Score "+str(M_term)+" CC0.5: "+str(CC_term)+"-> CC0.5 score "+str(CC_term)+" Anom value "+str(Avalue)+"->"+str(A_term)+" Blob value "+str(blob_score)+"->"+str(B_term))
        #
#        SF = open(statfile,"a")
#        SF.write(str(Isig_inner)+" "+str(Rvalue_inner )+" "+str(Anom_overall)+" "+str(SigAno_inner)+" "+str(best_fitness)+" "+str(CChalf_outer)+"\n")
#        SF.write(str(best_fitness)+" "+bests['I_inner']+" "+bests['Rmeas_inner']+" "+bests['Anom_overall']+" "+bests['SigAno_inner']+" "+bests['CChalf_overall']+" "+bests['CChalf_outer']+"\n")
#        SF.close()
        npar = group/(group+1)

        total_res = total_res+maxres
        total_isig = total_isig+Isig_inner
        n_points = n_points+1

        if (rmtree):
            shutil.rmtree(resultdir, ignore_errors=True)
    if (not crystfel_mode):        
        SF = open(statfile,"a")
        SF.write(str(best_fitness)+" "+bests['I_inner']+" "+bests['Rmeas_inner']+" "+bests['SigAno_overall']+" "+bests['SigAno_inner']+" "+bests['CChalf_overall']+" "+bests['CChalf_outer']+"\n")
        SF.close()
    Res_average= total_res/n_points
    Isig_inner_average= total_isig/n_points
    retval = ""



    print("Best fitness"+str(best_fitness)+" total fitness "+str(total_fitness))

#1    print "FITN "+str(fitn)
#    return (fitn,)
    if (best_or_total == "total"):
        return (total_fitness,)
    else:
        return (best_fitness,)

################




#def evalSIR(individual):
def evalSIR(randfiles, individual):

    print("evalSIR DIFFSCORE Csir_signal_inner_weight "+str(sir_signal_inner_weight))
    print("evalSIR DIFFSCORE sir_max_resolution_difference_weight "+str(sir_max_resolution_difference_weight))
    print("evalSIR DIFFSCORE sir_inverse_R_int_weight "+str(sir_inverse_R_int_weight))
    
    isig_weight=0.0
    anom_weight=0.0
    cchalf_weight=0.0
    completeness_weight=0.0
    multiplicity_weight=0.0
    
    if (shared.getConst('isig_weight')):
        isig_weight=shared.getConst('isig_weight')        
    else:
        isig_weight=shared.getConst('Uisig_weight')
    if (shared.getConst('anom_weight')):    
        anom_weight=shared.getConst('anom_weight')
    else:
        anom_weight=shared.getConst('Uanom_weight')
    if (shared.getConst('cchalf_weight')):    
        cchalf_weight=shared.getConst('cchalf_weight')
    else:
        cchalf_weight=shared.getConst('Ucchalf_weight')
    if (shared.getConst('completeness_weight')):    
        completeness_weight=shared.getConst('completeness_weight')
    else:            
        completeness_weight=shared.getConst('Ucompleteness_weight')
    if (shared.getConst('multiplicity_weight')):    
        multiplicity_weight=shared.getConst('multiplicity_weight')
    else:            
        multiplicity_weight=shared.getConst('Umultiplicity_weight')



    startdir =  os.getcwd()
#    r_value_acceptable = 0
    print("INDIV "+str(individual))

    best_score = -999999999
    best_shelx_dir = ""
    best_dataset1_dir =""
    best_dataset2_dir =""
    best_dir = ""
    
    print("New Individual ")
    print("---------------------------------")

    groupstring_to_dir={}


    for nat_group in range(1,groups+1):
        nat_startindex =0
        nat_endindex = len(sirfiles1)
        nat_groupstring = ""
        nat_HKLfilenames=[]

        #
        # build the list of files in each group for NATIVE
        #
        for datafile in range(nat_startindex, nat_endindex):
            if ( individual[datafile] == nat_group):
                nat_HKLfilenames.append(sirfiles1[datafile])
                nat_groupstring = nat_groupstring+   str(nat_group)
            else:
                nat_groupstring = nat_groupstring+  "_"

        if (len(nat_HKLfilenames) ==0):  #################################
            continue

        # 0 (float(maxres),   # checked may 22 against filenames_to_xscale return
        # 1 float(Isig_inner),
        # 2 float(Rvalue_inner),
        # 3 float(SigAno_inner),
        # 4 float(CCAno_inner),
        # 5 multiplicity_overall,
        # 6 float(Rvalue_overall),
        # 7 float(Isigma_overall),
        # 8 float(CChalf_overall),
        # 9 float(Rmeas_overall),
        # 10 float(SigAno_overall),
        # 11 float(CCAno_overall),
        # 12 float(Completeness_overall),
        # 13 float(MaxAnomRes),
        # 14 Rvalues, (array)
        # 15 Isigmas, (array)
        # 16 Rmeas, (array)
        # 17 CChalf, (array)
        # 18 unique_dir,
        # 19 newHKLfilenames,
        # 20 correlation_lookup

        local_rescut = 0
        if (randfiles != 1):
            local_rescut = output_resolution 
        
#        nat_stats = filenames_to_xscale(nat_HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
        nat_stats = filenames_to_xscale(nat_HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,local_rescut,0)
        
        print("1")

        nat_Isig_inner = nat_stats[1]
        nat_Rvalue_inner = nat_stats[2]
        SigAno_inner=nat_stats[3]
        AnoCC_inner = nat_stats[4]
        nat_multiplicity_overall = nat_stats[5]


        nat_Isig_overall = nat_stats[7]
        nat_CChalf_overall = nat_stats[8]

        SigAno_overall = nat_stats[10]
        AnoCC_overall = nat_stats[11]
        nat_Completeness_overall = nat_stats[12]
        MaxAnomRes=nat_stats[13]

        nat_CChalf = nat_stats[17]
        nat_resultdir = nat_stats[18]


        nat_CChalf_outer = nat_CChalf[len(nat_CChalf)-1]
        
        R_term_unweighted = score_Rvalue(nat_Rvalue_inner,weighting_scheme)
        R_term = R_term_unweighted *float(r_weight)

        print("RV "+str(nat_Rvalue_inner)+"RTU "+str(R_term_unweighted)+" DIR "+str(nat_resultdir))
        print("2")
        
#        I_term = nat_Isig_inner*float(isig_weight)
        C_term = nat_Completeness_overall*float(completeness_weight)
        M_term = nat_multiplicity_overall*float(multiplicity_weight)
        CC_term=0.0
        CC_value = 0.0
        if (CChalf_mode == "overall") :
            CC_value = nat_CChalf_overall
        else:
            CC_value = nat_CChalf_outer
        CC_term = CC_value*float(cchalf_weight)
            
        I_term = 0.0
        I_value = 0.0    
        if (I_sigma_mode == "overall"):
            I_value = nat_Isig_overall
        else:
            I_value = nat_Isig_inner
        I_term = I_value *float(isig_weight)

        Avalue = 0
        if re.search("Resolution",ano_mode):
            Avalue = MaxAnomRes
        elif re.search("SigAno_inner",ano_mode):
            Avalue= SigAno_inner
        elif re.search("SigAno_overall",ano_mode):
            Avalue = SigAno_overall
        elif re.search("AnoCC_inner",ano_mode):
            Avalue = AnoCC_inner
        elif re.search("AnoCC_overall",ano_mode):
            Avalue =AnoCC_overall
        else:
            Avalue =SigAno_overall

        A_term = Avalue *float(anom_weight)
            
        nat_fitn = R_term+I_term+C_term+M_term+CC_term  # not currently using A term but maybe eventually


        SF2 = open(overall_statfile,"a")       
        SF2.write(str(nat_Rvalue_inner)+","+str(R_term_unweighted)+","+str(I_value)+","+str(Avalue)+","+str(nat_Completeness_overall)+","+str(CC_value)+","+str(nat_multiplicity_overall)+"\n")
        SF2.close()      
        
        
        if (float(nat_Isig_inner) > float(inner_isig_max)): # In data that have had their sigmas fiddled with via for example DEISA, we might want to torpedo unrealistically high inner I sigs
            nat_fitn=-999


        print("NAT: "+nat_groupstring)
        print("         Nat_fitn R:"+str(R_term)+" I:"+str(I_term)+" C:"+str(C_term)+" M:"+str(M_term)+" CC:"+str(CC_term)+" DIR "+nat_resultdir)
#        (maxres, Isig_inner, Rvalue_inner, SigAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,Anom_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles)    =
        #
        # build the list of files in each group for DERIVATIVE
        #
        for der_group in range(groups+1,groups*2+1):
            der_HKLfilenames=[]
            der_groupstring = ""
            der_startindex = 0
            der_endindex = len(sirfiles1)
            for datafile in range(der_startindex, der_endindex):
                if ( individual[datafile] == der_group):
                    der_HKLfilenames.append(sirfiles2[datafile])
                    der_groupstring = der_groupstring+  str(der_group)
                else:
                    der_groupstring = der_groupstring+  "_"
        
            if (len(der_HKLfilenames) ==0):  #################################
                continue

            der_stats = []



            # caching results so we dont do the same XSCALE multiple times -- at some point make this thread aware -- maybe cache results on disk?

            if (der_groupstring in groupstring_to_dir):
                der_stats = groupstring_to_dir[der_groupstring]
            else:
#                der_stats = filenames_to_xscale(der_HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
                der_stats = filenames_to_xscale(der_HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,local_rescut,0) 
                groupstring_to_dir[der_groupstring] = der_stats


                # 0 (float(maxres),   # checked may 22 against filenames_to_xscale return
                # 1 float(Isig_inner),
                # 2 float(Rvalue_inner),
                # 3 float(SigAno_inner),
                # 4 float(CCAno_inner),
                # 5 multiplicity_overall,
                # 6 float(Rvalue_overall),
                # 7 float(Isigma_overall),
                # 8 float(CChalf_overall),
                # 9 float(Rmeas_overall),
                # 10 float(SigAno_overall),
                # 11 float(CCAno_overall),
                # 12 float(Completeness_overall),
                # 13 float(MaxAnomRes),
                # 14 Rvalues, (array)
                # 15 Isigmas, (array)
                # 16 Rmeas, (array)
                # 17 CChalf, (array)
                # 18 unique_dir,
                # 19 newHKLfilenames,
                # 20 correlation_lookup        

            der_Rvalue_inner = der_stats[2]
            der_Isig_inner = der_stats[1]
            der_Isig_overall = nat_stats[7]
            der_Completeness_overall = der_stats[12]
            der_multiplicity_overall = der_stats[5]
            der_resultdir = der_stats[18]

            der_CChalf_overall = der_stats[8]
            der_CChalf = der_stats[17]
            der_CChalf_outer = der_CChalf[len(der_CChalf)-1]

            MaxAnomRes=der_stats[13]
            SigAno_inner=der_stats[3]
            SigAno_overall = der_stats[10]
            AnoCC_inner = der_stats[4]
            AnoCC_overall = der_stats[11]



            der_Avalue = 0
            if re.search("Resolution",ano_mode):
                der_Avalue = MaxAnomRes
            elif re.search("SigAno_inner",ano_mode):
                der_Avalue= SigAno_inner
            elif re.search("SigAno_overall",ano_mode):
                der_Avalue = SigAno_overall
            elif re.search("AnoCC_inner",ano_mode):
                der_Avalue = AnoCC_inner
            elif re.search("AnoCC_overall",ano_mode):
                der_Avalue =AnoCC_overall
            else:
                der_Avalue =SigAno_overall

            der_A_term = der_Avalue *float(anom_weight)
                                
            
            R_term_unweighted  = score_Rvalue(der_Rvalue_inner,weighting_scheme)
            R_term = R_term_unweighted *float(r_weight)
#            I_term = der_Isig_inner*float(isig_weight)
            C_term = der_Completeness_overall*float(completeness_weight)
            M_term = der_multiplicity_overall*float(multiplicity_weight)


            CC_term=0.0
            CC_value=0.0
            if (CChalf_mode == "overall") :
                CC_value = der_CChalf_overall
            else:
                CC_value = der_CChalf_outer
            CC_term - CC_value * float(cchalf_weight)
            ##########
            I_term = 0.0
            I_value = 0.0
            if (I_sigma_mode == "overall"):
                I_value = der_Isig_overall
            else:
                I_value = der_Isig_inner
            I_term = I_value*float(isig_weight)    


            
            der_fitn = R_term+I_term+C_term+M_term+CC_term

            if (float(der_Isig_inner) > float(inner_isig_max)):
                der_fitn=-999

            print("DER: "+der_groupstring)
            print("         Der_fitn R:"+str(R_term)+" I:"+str(I_term)+" C:"+str(C_term)+" M:"+str(M_term)+" CC:"+str(CC_term))

            stats_acceptable = 0
            #
            # This needs more testing, but this step pre-screens individuals from native and derivative to make sure they are OK
            # datasets, by checking the R-value. This might however cause problems for the GA, since the target function is changing at 
            # different stages of the optimisation
            #
            print("Cutoff is "+str(r_value_inner_max)+" Rin1 "+str(nat_Rvalue_inner)+" Rin2 "+str(der_Rvalue_inner))
            if ((float(nat_Rvalue_inner)<  float(r_value_inner_max)  ) and  (float(der_Rvalue_inner) < float(r_value_inner_max) )):
                stats_acceptable = 1
                print("R values OK.") 
            else:
                print("R values unacceptable.")
                stats_acceptable = 0

            if (float(I_sig_inner_min) != -2): 
                if ((float(nat_Isig_inner)>  float(I_sig_inner_min)  ) and  (float(der_Isig_inner) > float(I_sig_inner_min) )):
                    stats_acceptable = 1
                    print("ISIG inner values OK.") 
                else:
                    print("ISIG inner values unacceptable.")
                    stats_acceptable = 0

            SF2 = open(overall_statfile,"a")       
            SF2.write(str(der_Rvalue_inner)+","+str(R_term_unweighted)+","+str(I_value)+","+str(der_Avalue)+","+str(der_Completeness_overall)+","+str(CC_value)+","+str(der_multiplicity_overall)+"\n")
            SF2.close()      

            print("RV2 "+str(der_Rvalue_inner)+"RTU "+str(R_term_unweighted)+" RT "+str(R_term)+"DIR "+str(nat_resultdir)+" Weighting "+str(weighting_scheme))


            individual_stats = 0.0
            if (sir_individual_fitness_mode.upper() == "MINIMUM"):
                if (nat_fitn <= der_fitn):
                    individual_stats= nat_fitn
                else:
                    individual_stats= der_fitn
            elif (sir_individual_fitness_mode.upper() == "AVERAGE" ):
                individual_stats= (nat_fitn+der_fitn)/2 
            elif (sir_individual_fitness_mode.upper() == "SUM" ):
                individual_stats= nat_fitn+der_fitn
            elif (sir_individual_fitness_mode.upper() == "SYMMETRIC" ):
                individual_stats= (1/((nat_fitn-der_fitn)+0.1))*nat_fitn*der_fitn
            else:
                individual_stats= nat_fitn*der_fitn


            
            if (stats_acceptable and sir_iso_weight != 0 ):        
                # now we shelxc together the best dirs
#                totalR = -999.0
                totalR = 10000.0
                totalSignal = 0.0
                lowRes_Rvalue= 1.0
                Signal_inner = 0.0
                shelxd_score = 0.0
                Riso_inner = 10000.0
                Overall_Rvalue = 10000.0
                max_res_by_diffs= 10000.0
                shelx_dir=""
                NaNFlag = False
                XSCALE_diff_score = 0.0
                outer_ISO_CC = 0.0
                signal_XP = 0.0
                inner_signal_XP = 0.0
                R_local_scaled_xp = 9999.0
                Rsquared = 0.0
                slope = 0.0
                residual_standard_error =0.0
                BIC=0.0
                # For the time being, we need to run the shelxc subroutine to set up files etc, even if we only use XSCALE to get stats.


                if (sir_xprep_stats):
                    (signal_XP,inner_signal_XP, R_local_scaled_XP, shelx_dir)=xprep_parse(nat_resultdir,der_resultdir, cell, sgname)
                else:
                    # (float(total_Rvalue), float(lowRes_Rvalue), float(total_signal), float(inner_signal),float(Overall_Rvalue), float(shelxd_score), max_res_by_diffs,unique_dir,SHELXC_NaN_flag)   
                    (totalR,Riso_inner,totalSignal,Signal_inner,Overall_Rvalue, shelxd_score, max_res_by_diffs, shelx_dir, NaNFlag,Rsquared,slope,residual_standard_error, BIC) =  shelxc_parse(nat_resultdir,der_resultdir, cell, sgname)                 

                nat_der = ["native.hkl","derivative.hkl"]
                if sir_XSCALE_diffs:

                    os.chdir(shelx_dir)
                        
                    (diffxscale)    =  filenames_to_xscale(nat_der,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,1)
                            # 0 (float(maxres),
                            # 1 float(Isig_inner),
                            # 2 float(Rvalue_inner),
                            # 3 float(SigAno_inner),
                            # 4 float(CCAno_inner),
                            # 5 multiplicity_overall,
                            # 6 float(Rvalue_overall),
                            # 7 float(Isigma_overall),
                            # 8 float(CChalf_overall),
                            # 9 float(Rmeas_overall),
                            # 10 float(SigAno_overall),
                            # 11 float(CCAno_overall),
                            # 12 float(Completeness_overall),
                            # 13 float(MaxAnomRes),
                            # 14 Rvalues,
                            # 15 Isigmas,
                            # 16 Rmeas,
                            # 17 CChalf,
                            # 18 unique_dir,
                            # 19 newHKLfilenames,
                            # 10 correlation_lookup
                    cc_lookup = diffxscale[20]

                    import pprint
                    pp = pprint.PrettyPrinter(indent=4)
                    pp.pprint(diffxscale)
                    cc = float(cc_lookup[0][1])
                    if (cc != 1):
                        XSCALE_diff_score = 1.0/(1.0-float(cc_lookup[0][1]))
                    else:
                        XSCALE_diff_score = 1000.0
                    print("NNN cc_lookup "+str(diffxscale[20])+" XSCALE_diff_score "+str(XSCALE_diff_score))
                    os.chdir("../")
                elif sir_phenix_stats:
                    os.chdir(shelx_dir)
                    (outer_ISO_CC) = phenix_iso_stats(nat_der)
                    print("PHENIX outer CC is "+str(outer_ISO_CC)+ " in dir "+shelx_dir)
                    os.chdir("../")
                else:
                    pass


                ###############################                ###############################                ###############################                ###############################
                #
                #  Scoring
                #
                ###############################                ###############################                ###############################                ###############################                
                
                difference_score =  0.0

#                individual_stats= (nat_fitn+der_fitn)
                difference_plot_score = 0.0
                
                #   difference_score = Signal_inner/Riso_inner
                #   difference_score = totalSignal*Riso_inner
                #   difference_score = 1.0/Overall_Rvalue
                
                if (sir_XSCALE_diffs):
                    difference_score = XSCALE_diff_score
                    print("XSCALE diffs")
                elif (sir_phenix_stats):
                    difference_score = outer_ISO_CC*100.0
                    print("PHENIX DIFFS")
                elif sir_xprep_stats:
                    print("XPREP DIFFS")
#                        difference_score = signal_XP
#                        difference_score = inner_signal_XP
                    if (R_local_scaled_xp != 0):
                        difference_score  = inner_signal_XP/R_local_scaled_xp
                # "Regular" scoring      
                else:
#                    print "DEFAULT DIFFS"
#                    print "DIFFSCORE Csir_signal_inner_weight "+str(sir_signal_inner_weight)
#                    print "DIFFSCORE sir_max_resolution_difference_weight "+str(sir_max_resolution_difference_weight)
#                    print "DIFFSCORE sir_inverse_R_int_weight "+str(sir_inverse_R_int_weight)
#                    print "DIFFSCORE signal_inner "+str(Signal_inner)
#                    difference_score = (float(sir_signal_inner_weight)*float(Signal_inner))+(float(sir_max_resolution_difference_weight)*(1.0/float(max_res_by_diffs)))+(float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue))


#                    difference_plot_score = float(sir_signal_plot_weight)*
#                    difference_plot_score = slope*residual_standard_error
                    if (Rsquared != 0):
#                        difference_plot_score = slope*(math.log(Rsquared)+1)
#                        difference_plot_score = (Rsquared**sir_signal_rsquared_exponent) *slope
                        print("RSQUARED "+str(Rsquared))
                        print("sir_signal_rsquared_exponent "+str(sir_signal_rsquared_exponent))
                        print("Slope "+str(slope))
                        difference_plot_score = math.pow(Rsquared,sir_signal_rsquared_exponent) *slope
                    else:
                        difference_plot_score = 0

                    print("SIR Scoring method "+    str(sir_scoring_method))
                        
                    if (sir_scoring_method == "total_ratio"):
                        if (totalR != 0): 
                            difference_score = totalSignal/totalR
                        else:
                            difference_score = totalSignal
                    if (sir_scoring_method == "difference_plot_offset"):
                        difference_score = difference_plot_score+float(sir_slope_offset)
                    if (sir_scoring_method == "difference_plot_Riso_sum"):
                        # y=0.85^(abs(x)-20)

                        diff_exponent = abs(Overall_Rvalue)-0.20
                        print("EXPO "+str(diff_exponent))
                        print("DPLOTSC "+str(difference_plot_score))
                        print("BASE "+str(sir_riso_overall_base))
                        difference_score = difference_plot_score+math.pow(sir_riso_overall_base,diff_exponent)
#                        difference_score = math.pow(sir_riso_overall_base**diff_exponent
                        print("Difference score "+str(difference_score))
                    if (sir_scoring_method == "difference_plot_log_ratio"):
                        # y=0.85^(abs(x)-20)

                        diff_exponent = abs(Overall_Rvalue)-20
                        print("EXPO "+str(diff_exponent))
                        print("DPLOTSC "+str(difference_plot_score))
                        print("BASE "+str(sir_riso_overall_base))
                        difference_score = difference_plot_score*math.pow(sir_riso_overall_base,diff_exponent)
#                        difference_score = math.pow(sir_riso_overall_base**diff_exponent
                        print("Difference score "+str(difference_score))
                    elif (sir_scoring_method == "ratio"):
                        if ((float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue)) != 0):
                            difference_score = (float(sir_signal_inner_weight)*float(Signal_inner))/(float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue))
                        else:
                            difference_score = (float(sir_signal_inner_weight)*float(Signal_inner))
                    elif (sir_scoring_method == "difference_plot_total_ratio"):
                        if (totalR != 0): 
                            difference_score = difference_plot_score/totalR
                        else:
                            difference_score = difference_plot_score
                    elif (sir_scoring_method == "difference_plot_ratio"):
                        if ((float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue)) != 0):
                            difference_score = difference_plot_score/(float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue))
                        else:
                            difference_score = difference_plot_score
                    elif (sir_scoring_method == "difference_plot_slope"):
                        difference_score = slope
                    elif (sir_scoring_method == "difference_plot_slope_riso"):
                        print("Riso "+str(Overall_Rvalue))
                        difference_score = slope/float(Overall_Rvalue)

                    elif (sir_scoring_method == "difference_plot_slope_lowresriso"):
                        print("LowresRiso "+str(Riso_inner))
                        if (float(Riso_inner) !=0):
                            difference_score = slope/float(Riso_inner)
                    elif (sir_scoring_method == "difference_plot_slope_totalriso"):
                        print("TotalR "+str(totalR))
                        if (float(totalR) !=0):
                            difference_score = slope/(float(sir_riso_inner_weight)*float(totalR))
                    elif (sir_scoring_method == "difference_plot_slope_innersignal"):
                        print("Innersignal "+str(Signal_inner))
                        difference_score = slope*float(Signal_inner)
                        



                    elif (sir_scoring_method == "difference_plot_slope_offset"):
                        difference_score = slope+float(sir_slope_offset)
                        
                    elif (sir_scoring_method == "difference_plot"):
                        difference_score = difference_plot_score
                    elif (sir_scoring_method == "Inner_signal"):
                        difference_score = Signal_inner
                    elif (sir_scoring_method == "Total_signal"):
                        difference_score = totalSignal                        
                    elif (sir_scoring_method == "Total_signal_Rsquared"):
                        difference_score = totalSignal*Rsquared
                    elif (sir_scoring_method == "difference_plot_conditional_riso"):
#                        if (slope > float(sir_slope_threshold_overallriso)):
                        if (Overall_Rvalue > float(sir_riso_threshold_overallriso)):
                            print(" Overal Rvalue "+str(Overall_Rvalue)+"too high and in difference_plot_conditional_riso mode")
                            difference_score = difference_plot_score
                        else:
                            print("Riso "+str(Overall_Rvalue)+" so Riso kicking in") 
                            difference_score = difference_plot_score+(float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue))
                    else:
                        difference_score = (float(sir_signal_inner_weight)*float(Signal_inner))+(float(sir_max_resolution_difference_weight)*(1.0/float(max_res_by_diffs)))+(float(sir_inverse_R_int_weight)*(100.0-Overall_Rvalue))+float(sir_signal_plot_weight)*difference_plot_score

                        

                # add in shelxd values, if appropriate    
                if (sir_use_shelxd and float(difference_score) != 0.0  ):     # Shelxd score is only calculated if the difference signal is above a certain value. If this is not met
                                                                              # then the score could be 0, so we use the difference score instead.
                    difference_score = difference_score + float(shelxd_score)

                print("Indiv score "+str(individual_stats)+" Weighted "+str(float(sir_individual_stat_weight)*individual_stats))
                print("Diff score "+str(difference_score)+" Weighted "+ str(float(sir_iso_weight)*difference_score))



                if (sir_score_combination_mode == "product"):
                    print("Product mode")
                    total_score = (float(sir_individual_stat_weight)*individual_stats)* ((float(sir_iso_weight)*difference_score))
                else:
                    total_score = (float(sir_individual_stat_weight)*individual_stats)+ ((float(sir_iso_weight)*difference_score))




                if (Riso_inner == 999):
                    # this means the inner Riso was not parsed, which usually means there was a problem, possible related to reindexing
                    # -- these individuals should probably be given a low score.
                    total_score = -999999999 


                FI = open(fitness_values,"a")
                FI.write(str(nat_fitn)+","+str(der_fitn)+","+str(individual_stats)+","+str(difference_score)+","+str(total_score)+"\n")
                FI.close()


                ###############################                ###############################                ###############################                ###############################
                #
                # End scoring
                #
                ###############################                ###############################                ###############################                ###############################

                
                # look at the ratio between the average inner R values and Riso.  If they are bigger than a certain value, set score to -999
                # we do this because sometimes (rarely) the shelxc automatic origin finding fails 
                delta_cut = 100.0
                if (sir_shelxc_reind and not sir_XSCALE_diffs):
                    print("SIR workaround on")
                    #
                    if ((sir_shelxc_reind_mode =="Rint_overall") and  (float(Overall_Rvalue) > float(sir_shelxc_reind_cutoff_value))):
                        total_score = -999999999
                        print("SIR workaround... Rint_overall mode, R too high "+str(Overall_Rvalue)+" vs cutoff "+str(sir_shelxc_reind_cutoff_value))
                    if ((sir_shelxc_reind_mode =="Rint_inner") and  (float(Riso_inner) > float(sir_shelxc_reind_cutoff_value))):
                        total_score = -999999999
                        print("SIR workaround... Rint_inner mode, R too high "+str(Riso_inner)+" vs cutoff "+str(sir_shelxc_reind_cutoff_value))
                    if ((sir_shelxc_reind_mode =="Signal_inner") and  (float(Signal_inner) > float(sir_shelxc_reind_cutoff_value))):
                        total_score = -999999999 
                    if ((sir_shelxc_reind_mode =="Rint_inner_individual_Rs") and  (float(Riso_inner) > float(sir_shelxc_reind_cutoff_value))):
                        avg_inner = (float(der_Rvalue_inner)+float(nat_Rvalue_inner))/2
                        inner_iso_diff = abs((float(Riso_inner)*100)-float(avg_inner))
                        if ((inner_iso_diff >delta_cut) or (NaNFlag  and check_shelxc_NaN)):
                            print("Delta unacceptable avg inner "+str(avg_inner)+" inner Riso "+str(float(Riso_inner)*100)+" ratio "+str(inner_iso_diff)+" cut "+str(delta_cut)+" dir"+str(shelx_dir))
                            total_score = -999999999
                        else:
                            print("Delta OK inner "+str(avg_inner)+" inner Riso "+str(Riso_inner)+" ratio "+str(inner_iso_diff)+" cut "+str(delta_cut)+ " dir "+str(shelx_dir))


                SFD = open("sirscores_debug.txt","a")
                SFD.write(str(nat_fitn)+" "+str(der_fitn)+" "+str(Signal_inner)+" "+str(difference_score)+" "+str(shelxd_score)+" "+str(total_score)+"\n")
                SFD.close()

                print("Total score "+str(total_score))
                print("ABC total_score "+str(total_score)+" best score "+str(best_score))
                if (total_score >best_score) :
                    best_score = total_score
                    best_dir = shelx_dir
                    best_dataset1_dir = nat_resultdir 
                    best_dataset2_dir =der_resultdir

                    print("ABC Best score now "+str(best_score)+" best dir now "+best_dir)
                SF = open(statfile,"a")
                SF.write(str(nat_Rvalue_inner)+" "+str(der_Rvalue_inner)+" "+str(totalR)+" "+str(totalSignal)+" "+str(Overall_Rvalue)+" "+str(total_score)+"\n")
                SF.close()
                ScoreFile = open(str(shelx_dir)+"/ScoreFile.txt","w")

#                total_score = float(sir_individual_stat_weight)*individual_stats+ (float(sir_iso_weight)*difference_score)
                weighted_diff_score = float(sir_iso_weight)*difference_score
                weighted_indiv_score = float(sir_individual_stat_weight)*individual_stats
                weighted_Rsquared_score = float(sir_signal_plot_weight)*Rsquared

                
                # my ($total_score, $R_inner_1,$R_inner_2,$totalR, $overall_R, $lowres_R, $totalSignal,$signal_inner,  $difference_score,$riso,$fitness1,$fitness2 ) = split / /, $_;
                ScoreFile.write(str(total_score)+" "+str(nat_Rvalue_inner)+" "+str(der_Rvalue_inner)+" "+str(totalR)+" "+str(Overall_Rvalue)+" "+str(Riso_inner)+" "+str(totalSignal)+" "+str(Signal_inner)+" "+str(individual_stats)+" "+str(difference_score)+" "+str(nat_fitn)+" "+str(der_fitn)+" "+str(weighted_diff_score)+" "+str(weighted_indiv_score)+" "+str(Rsquared)+" "+str(weighted_Rsquared_score)+" "+str(difference_plot_score)+" "+str(slope)+"\n")
                ScoreFile.close()
            else:    
                total_score = float(sir_individual_stat_weight)*individual_stats
                if (sir_iso_weight == 0 ):
                     print("SIR ISO weight set to 0, so we are only using individual stats")
                else:
                     print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> R values not good enough ...  SCORE "+str(total_score))
                SF = open(statfile,"a")
                SF.write(str(nat_Rvalue_inner)+" "+str(der_Rvalue_inner)+" "+str(total_score)+"\n")
                SF.close()



    if (randfiles != 1):
#        outputted_directory = False
        for solution in range (0,10):
            print("ABC copying to solution "+str(solution))
            sir_dirname = "best_shelx_solution_"+str(solution)
            sir_dataset1 = "best_native_solution_"+str(solution)
            sir_dataset2 = "best_derivative_solution_"+str(solution)


    
            print("ABC SIRDIR "+sir_dirname)
            if ((not os.path.exists(sir_dirname)) and (not os.path.exists(sir_dataset1)) and (not os.path.exists(sir_dataset2))):
                if (os.path.exists(best_dataset1_dir)):
                    shutil.copytree (best_dataset1_dir,sir_dataset1)
                else:
                    print(("ABC no best_dataset1_dir"+best_dataset1_dir))
                if (os.path.exists(best_dataset2_dir)):
                    shutil.copytree (best_dataset2_dir,sir_dataset2)
                else:
                    print(("ABC no best_dataset2_dir"+best_dataset2_dir))
                if (os.path.exists(best_dir)):
                    shutil.copytree (best_dir,sir_dirname)
                else:
                    print(("ABC no best_dir"+best_dir))
                outputted_directory = True
                print("ABC found dir")
                break
            else:
                print("ABC Output directory exists .. skipping to next")
 #       if (not outputted_directory):
 #           print "Best shelx dataset does not exist -- this usually is only possible if you have set the minimum R value too optimistically. Try again with a higher -q level " +best_dir
 #           os.system("touch WARNING_NO_BEST_SHELX_DIR_Q_PARAM_TOO_HIGH")
                        



    

 


    return (best_score,)
################


def filenames_to_partialator(HKLindices,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl, res_override,getCCs):

    unique_dir = "tmpPTLR"+str(uuid.uuid4())
    if (not os.path.exists(unique_dir)):
        os.mkdir(unique_dir)
    os.chdir(unique_dir)    
    

    
    os.symlink("../"+str(crystfel_streamfile),str(crystfel_streamfile))
    
    # make stream by catting files


    if (len(HKLindices) <2): ##########################
        return (9999999, -9999999,9999999,-9999999,-9999999,-9999999, 9999999,-9999999,-9999999,9999999,-9999999,-9999999,-9999999, -9999999, Rvalues,Isigmas,Rmeas,CChalf,unique_dir,newHKLfilenames,correlation_lookup)  


    chunk_begins= []
    chunk_ends = []
    refl_begins= []
    refl_ends = []
    peak_begins = []
    peak_ends = []
    
    filestring = ""

#    print (stream_object)
#    stream_object.get_chunk_pointers()
#    exit
    for hkl in HKLindices:

        chunk_begins.append(stream_object.codgas_lookup['begin_chunk'][hkl])
        chunk_ends.append(stream_object.codgas_lookup['end_chunk'][hkl])

        peak_begins.append(stream_object.codgas_lookup['begin_reflist'][hkl])
        peak_ends.append(stream_object.codgas_lookup['end_reflist'][hkl])

        refl_begins.append(stream_object.codgas_lookup['begin_reflist'][hkl])
        refl_ends.append(stream_object.codgas_lookup['end_reflist'][hkl])

#        chunkIDs.append(int(hkl))
#        os.symlink("../"+str(hkl),str(hkl))
#        filestring = filestring + " "+str(hkl)

#        print("partial "+str(hkl))

#    print("FILSTRING"+filestring)

#    with open('streamfile','wb') as wfd:
#        for f in HKLfilenames:
#            with open(f,'rb') as fd:
#                shutil.copyfileobj(fd, wfd)


    input_stream = "streamfile"
#    stream_object.write_stream(stream_object.image_refls, stream_object.image_peaks, stream_object.header, input_stream)
#    print ("HKL begins"+str(begins))
#    print ("HKL ends"+str(ends))



    t_start = time.time()
#    stream_object.read_chunks(stream_object.codgas_lookup['begin_chunk'][chunkIDs], stream_object.codgas_lookup['end_chunk'][chunkIDs])
    stream_object.read_chunks( chunk_begins,chunk_ends)
    t_readchunks = time.time()-t_start
    t_start = time.time()
#    stream_object.get_peaklist(stream_object.codgas_lookup['begin_peaklist'][chunkIDs], stream_object.codgas_lookup['end_peaklist'][chunkIDs])
    stream_object.get_peaklist( peak_begins,peak_ends)
    t_get_peaklist = time.time()-t_start
    t_start = time.time()
#    stream_object.get_reflections_list(stream_object.codgas_lookup['begin_reflist'][chunkIDs], stream_object.codgas_lookup['end_reflist'][chunkIDs])
    stream_object.get_reflections_list( refl_begins,refl_ends)
    t_get_reflections = time.time()-t_start
    t_start = time.time()
    
    stream_object.write_stream(stream_object.image_refls, stream_object.image_peaks,  stream_object.header, input_stream)
    t_write_stream = time.time()-t_start

    print ("TIMES2 read_chunks "+str(t_readchunks)+" get_peaklist "+str(t_get_peaklist)+" get_reflections "+str(t_get_reflections)+" write_stream "+str(t_write_stream))
    
    
    partialator_output= input_stream+".hkl"
    partialator_first_half= input_stream+".hkl1"
    partialator_second_half= input_stream+".hkl2"

    # to be fixed!
    cell_file = "/data/visitor/mx1787/tests/RAW_DATA/lysozyme_4_Gd/autoCryst_2019-08-22_13-03-22/auto.cell"
    point_group = 422
    nproc = 8
    if (user_res_override):
        rescut = float(user_res_override)
    
    # run partialtor

    t_start = time.time()
    
#    command_string =  AutoCrystFEL.partialator_cmd(input_stream, point_group, nproc)
    t_pltr_prepare = time.time()-t_start
        

    cwd_local = os.getcwd()

    command_string = AutoCrystFEL.process_cmd(input_stream, point_group)
    command_string = command_string + "> /dev/null 2>&1"
    

    #    command_string = command_string + "> /dev/null 2>&1"
    os.system(command_string)  
#    job = submit(command = command_string,name = "partialator",core=10,gpu=False,walltime={'minutes':5}, working_directory=cwd_local) 
#    job.wait()

#    os.system("ssh mxnice 'cd "+str(cwd_local)+";"+str(command_string)+"'")    # parse stats
#    os.system("oarsub -d "+str(cwd_local)+" '"+str(command_string)+";touch done'")    # parse stats
#    while (not os.path.exists("done") ):
#        time.sleep(1)
#        print("no done file")
        
    t_pltr = time.time()-t_start
    t_start = time.time()
    
    check_hkl_command = AutoCrystFEL.check_hkl_cmd(partialator_output,point_group,cell_file,rescut)
    check_hkl_command = check_hkl_command + "> /dev/null 2>&1"
    os.system(check_hkl_command)

    t_check = time.time()-t_start

    t_start = time.time()
    compare_hkl_command = AutoCrystFEL.compare_hkl_cmd(partialator_first_half,partialator_second_half,cell_file,rescut,'CCano')
    compare_hkl_command = compare_hkl_command+"> /dev/null 2>&1"
    os.system(compare_hkl_command)
    t_compare_ccano = time.time()-t_start
    t_start = time.time()
    
    compare_hkl_command = AutoCrystFEL.compare_hkl_cmd(partialator_first_half,partialator_second_half,cell_file,rescut,'Rsplit')
    compare_hkl_command = compare_hkl_command+"> /dev/null 2>&1"
    os.system(compare_hkl_command)
    t_compare_rsplit = time.time()-t_start

    print("PTL command "+str(command_string)) 
    print("TIMES prepare_partialator "+str(t_pltr_prepare)+" os.run partialtor "+str(t_pltr)+" check_hkl "+str(t_check) + " compare_ccano "+str(t_compare_ccano)+" compare_rsplit "+str(t_compare_rsplit))
    
    snrfile = input_stream+'_snr.dat'
    ccfile = input_stream+'_CCano.dat'
    Rsplitfile = input_stream+'_Rsplit.dat'

    print ("RSPLITFILE "+Rsplitfile)
    statgen = ResultParser()
    stat = {}
    #
    # stat["ccano"][0]["ccano"] is inner shell ccano value
    # stat["ccano"][0]["ccano"] is inner shell ccano value
    # stat["ccano"][0]["q"] is the center of the shell
    # stat["ccano"][0]["resolution"] is resolution shell
    # stat["snr"][0]["snr"] is I/sigI

#    overall_snr
#    overall_ccano
#    overall_rsplit
#    for fname, fom in zip([snrfile, ccfile,Rsplitfile],['snr', 'ccano','rsplit']):
    for fname, fom in zip([snrfile, ccfile,Rsplitfile],['snr', 'ccano','rsplit']):
        os.system("cat "+fname)
        if os.path.exists(fname):
            statgen.getstats(fname, fom=fom)
            stat[fom] = statgen.results

    retval = -999999    
    CCAno_inner = -999999
    Isig_inner = -99999
    Rsplit_inner = 99999

    last_bin_above_snr_threshold =0
    resolution_bin_above_snr_threshold = 0

    print("KEYS "+str(stat.keys()))
    
    print("UDIR "+unique_dir)

    overall_snr =0.0
    overall_rsplit =100.0
    overall_ccano =0

    snr_DQ=[]
    ccano_DQ=[]
    rsplit_DQ=[]
    
    if ("snr" in stat):
        snr_DQ = stat["snr"]['DataQuality']
    if ("ccano" in stat):
        ccano_DQ = stat["ccano"]['DataQuality']
    if ("rsplit" in stat):
        rsplit_DQ = stat["rsplit"]['DataQuality']



    if (len(snr_DQ)>0):
        if "overall_snr" in stat["snr"]:
            overall_snr = stat["snr"]["overall_snr"]
            if str(overall_snr) == "nan":
                overall_snr = 0
        if "snr" in snr_DQ[0]:
            Isig_inner = snr_DQ[0]["snr"]
            if str(Isig_inner) == "nan":
                Isig_inner = 0
             # get last res bin where SNR >1

            for resbin in range(0,len(snr_DQ)-1):
                if (snr_DQ[resbin]["snr"] > 1):
                    last_bin_above_snr_threshold = resbin
                    resolution_bin_above_snr_threshold = snr_DQ[resbin]["resolution"]
                else:
                    break

                
    if (len(rsplit_DQ)>0):
            overall_rsplit = stat["rsplit"]["overall_rsplit"]
            if str(overall_rsplit) == "nan":
                overall_rsplit = 100
            if "rsplit" in rsplit_DQ[0]:
                Rsplit_inner = rsplit_DQ[0]["rsplit"]
                if str(Rsplit_inner) == "nan":
                    Rsplit_inner = 100
    if (len(ccano_DQ)>0):
            overall_ccano = stat["ccano"]["overall_ccano"]
            if str(overall_ccano) == "nan":
                overall_ccano = 100
            if "ccano" in ccano_DQ[0]:
                ccano_inner = ccano_DQ[0]["ccano"]
                if str(ccano_inner) == "nan":
                    ccano_inner = 100
                
    print ("Max Resolution"+str(resolution_bin_above_snr_threshold))            
    os.chdir("../")

 #   print ("isig inner "+str(Isig_inner)+" ioverall "+str(overall_snr)+" rsplit inner "+str(Rsplit_inner)+" rsplit overall "+str(overall_rsplit))


    
    
    return (CCAno_inner,overall_ccano,Isig_inner,overall_snr,Rsplit_inner,overall_snr,resolution_bin_above_snr_threshold,unique_dir)    

    
def filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl, res_override,getCCs):
#    print "FNAMES"+str(HKLfilenames)


    newHKLfilenames =[]

    unique_dir = "tmpXSCALE"+str(uuid.uuid4())
    if (not os.path.exists(unique_dir)):
        os.mkdir(unique_dir)
    os.chdir(unique_dir)    

    avg_correlation_by_fileindex=[None]*(len(HKLfilenames)+1)

    correlation_lookup = [[i * j for j in range(len(HKLfilenames)+1)] for i in range(len(HKLfilenames)+1)]

#    print os.getcwd()
    XS = open("XSCALE.INP","w")

    XS.write("OUTPUT_FILE=merged.hkl\n")
    XS.write("MERGE=TRUE\n")
    XS.write("SAVE_CORRECTION_IMAGES=FALSE\n")
    XS.write("UNIT_CELL_CONSTANTS="+cell+"\n")


#    print "ANOM "+str(anom_on)
    if (int(anom_on) != 0):
        XS.write("FRIEDEL'S_LAW=FALSE\n")
        XS.write("STRICT_ABSORPTION_CORRECTION=TRUE\n")
    else:
        XS.write("FRIEDEL'S_LAW=TRUE\n")
    if (int(user_sgn) != 0 ):
        XS.write("SPACE_GROUP_NUMBER="+str(user_sgn)+"\n")
    else :
        XS.write("SPACE_GROUP_NUMBER="+str(SGN)+"\n")
#    XS.write("MAXIMUM_NUMBER_OF_PROCESSORS=8\n")

    maxres_byCC=999
    maxres_byAnom=999
    maxres_byIsig=999
    maxres_byR=999
    maxres_byCompl=999
    maxres = 999


    Rvalues=[]
    Isigmas=[]
    Rmeas=[]
    CChalf=[]

    # 
    if (len(HKLfilenames) ==0): ##########################
        return (9999999, -9999999,9999999,-9999999,-9999999,-9999999, 9999999,-9999999,-9999999,9999999,-9999999,-9999999,-9999999, -9999999, Rvalues,Isigmas,Rmeas,CChalf,unique_dir,newHKLfilenames,correlation_lookup)  
#        return (9999999, -9999999,9999999,-9999999,-9999999, 9999999,-9999999,-9999999,9999999,-9999999,-9999999, -9999999, Rvalues,Isigmas,Rmeas,CChalf,unique_dir,newHKLfilenames,correlation_lookup)  
    
    CChalf_overall=0
    Rmeas_overall=0
    Rvalue_overall=0
    Isigma_overall=0
#    Anom_overall = 0
    Completeness_overall = 0 
    MaxAnomRes = 999

    Rvalue_inner = 999
    Isig_inner= 0
    lastres=999
    SigAno_inner=0
    SigAno_overall = 0
    CCAno_inner = 0
    CCAno_overall = 0
    anom_mrg = [ 'False']
    multiplicity_overall = 0.0
    for x in anom_mrg:
       for hkl in HKLfilenames:
#           print "Adding file: "+hkl
           os.symlink("../"+str(hkl),str(hkl))
#           os.symlink(os.path.join("../",str(hkl)),".")
           XS.write("INPUT_FILE="+hkl+" XDS_ASCII\n")
           if (res_override != 0 ):
                   XS.write("INCLUDE_RESOLUTION_RANGE=100 "+str(res_override)+"\n")               
           elif (user_res_override is not None):
                XS.write("INCLUDE_RESOLUTION_RANGE=100 "+str(user_res_override)+"\n")
           else:
               XS.write("INCLUDE_RESOLUTION_RANGE=100 0\n")
           XS.write("MINIMUM_I/SIGMA= 0\n")
           XS.write("CORRECTIONS="+xscale_corrections+"\n")
       XS.close()
       # now iterate until I/sig or CC reach some target 
#       stdout=Proc('/opt/pxsoft/bin/xscale_par').call(timeout=300).stdout
       os.system("/usr/bin/nice /opt/pxsoft/bin/xscale_par >/dev/null") 
##       os.system("/opt/pxsoft/bin/xscale_par>/dev/null")
##       os.system("xscale_par>/dev/null")
       # now parse the results
       XSO = open("XSCALE.LP")
       #
       # Example
       #
       # SUBSET OF INTENSITY DATA WITH SIGNAL/NOISE >= -3.0 AS FUNCTION OF RESOLUTION
       # RESOLUTION     NUMBER OF REFLECTIONS    COMPLETENESS R-FACTOR  R-FACTOR COMPARED I/SIGMA   R-meas  CC(1/2)  Anomal  SigAno   Nano
       # LIMIT     OBSERVED  UNIQUE  POSSIBLE     OF DATA   observed  expected                                      Corr
       # 
       # 11.48       14347    2385      2425       98.4%       4.0%      4.4%    14333   36.16      4.4%    99.8*    -7    0.778    1736
       #  9.13       13952    2269      2273       99.8%       4.4%      4.8%    13935   31.93      4.9%    99.8*   -15    0.722    1806
       # total      139351   31054     31292       99.2%       7.6%      7.8%   138571   12.23      8.6%    99.8*    -4    0.773   19923
       #
       # total     1432938  138865    165843       83.7%      14.1%     15.7%  1420489   10.65     14.7%    99.1*    39*   1.215   52987

       # total       32172   22103     49824       44.4%      70.8%    190.7%    17680    1.36     94.8%    66.7*    28*   0.506     244
       # total     1668798  101743    101751      100.0%      39.6%     40.0%  1668749    8.80     40.8%    99.6*    -3    0.630   90515

       dataLine  = re.compile('\s+(\d+\.\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+\.\d)\%\s+(\d+\.\d)\%\s+(\d+\.\d)\%\s+(\d+)\s+(\-?\d+\.\d+)\s+(\-?\d+\.\d+)\%\s+(\-?\d+\.\d)(.)\s+(\-?[0-9]+)( |\*)\s+(\S+)\s+(\d+)')            
       totalLine = re.compile('\s+total\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+\.\d)\%\s+(\d+\.\d)\%\s+(\d+\.\d)\%\s+(\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\%\s+(\d+\.\d)(.)\s+(\-?[0-9]+)(\*?)\s+(\S+)\s+(\d+)') 
       errorLine = re.compile('\!\!\! ERROR \!\!\!')
       lines = XSO.readlines()

        

       if getCCs:
           error = 0
           startline=0
           endline = len(lines)
           for i in range(0,len(lines)):
               if re.search(".i   .j     REFLECTIONS     BETWEEN i,j  INTENSITIES \(i/j\)  BETWEEN i,j",lines[i]):
                   startline = i+2
               if re.search("K\*EXP\(B\*SS\) \= Factor applied to intensities",lines[i]):
                   endline = i-2
                   break
               if re.search(" !!! WARNING !!! INACCURATE SCALING FACTORS.",lines[i]):
                   endline = i-2
                   break
               if re.search("SOME CORRELATION FACTORS ARE DANGEROUSLY SMALL",lines[i]):
                   endline = i-2
                   break
               if re.search("\!\!\! ERROR \!\!\!",lines[i]):
                   print("Error scaling datasets:  no CC based weeding will be done")
                   newHKLfilenames = HKLfilenames
                   error =1
                   break
           # now look in the correct range
           #
           #
           #
       #DATA SETS  NUMBER OF COMMON  CORRELATION   RATIO OF COMMON   B-FACTOR
       #i   #j     REFLECTIONS     BETWEEN i,j  INTENSITIES (i/j)  BETWEEN i,j
       #1    2        2675           0.959            1.1713        -5.7007
       #1    3        2640           0.993            0.8360         1.3312
       #2    3        1764           0.531            0.6618         8.1364
       #1    4        2721           0.984            1.0339        -0.6862
       #2    4        4443           0.933            0.8307         4.8258
       #3    4        1997           0.973            1.1990        -2.9705
           if not error:
               for i in range(startline,endline):
                   items = re.split("\s+",lines[i])
#                   print "Num items "+str(len(items))
                   if len(items) >2 :
#                       print "dataset 1: "+items[1]+" dataset 2: "+items[2]+" CC "+items[4]
                       SF = open("li.txt","w")
                       SF.write("0 "+items[0]+"\n1 "+items[1]+"\n2 "+items[2]+" "+unique_dir+"\n")
                       SF.close()
                       
                       this_index1 = int(items[1])-1
                       this_index2 = int(items[2])-1
                       correlation_lookup[this_index1][this_index2]=items[4]

                       if (this_index1==0 and this_index2==1):
                           SF = open("../ratio.txt","w")
                           SF.write("RATIO "+str(items[5])+"\n")
                           SF.write("BFACT "+str(items[6])+"\n")
                           SF.write("CC "+str(items[4])+"\n")
                           SF.close()
                                   
                       
#                       try:
                       if (avg_correlation_by_fileindex[this_index1]) is not None:
                           avg_correlation_by_fileindex[this_index1] = (float(avg_correlation_by_fileindex[this_index1])+float(items[4]))/2
                       else:
                           avg_correlation_by_fileindex[this_index1]=float(items[4])
#                       try:
                       if (avg_correlation_by_fileindex[this_index2]) is not None:
                           avg_correlation_by_fileindex[this_index2] = (float(avg_correlation_by_fileindex[this_index2])+float(items[4]))/2
                       else:
                           avg_correlation_by_fileindex[this_index2]=float(items[4])

#                       except IndexError:
#                           avg_correlation_by_fileindex[this_index]=float(items[4])

               #
               # Now we can optionally use the average correlations to weed out datasets.
               #
               if (correlation_cutoff):
                   for i in range(0,len(avg_correlation_by_fileindex)-1):
                       print("Dataset "+str(i)+" Avg correlation "+str(avg_correlation_by_fileindex[i]))
                       if  avg_correlation_by_fileindex[i] > float(correlation_cutoff ):
                           newHKLfilenames.append(HKLfilenames[i])
                       else:
                           print("Dataset number "+str(i)+" was rejected because its average CC is "+str(avg_correlation_by_fileindex[i]))
       #######################################
       dataLineCount = 0
       for i in range(len(lines)):
           item = lines[i]
#           print "INLINE "+item

           if errorLine.search(item):
               print("XSCALE error "+item)
               os.chdir("../")
               Rvalues = [9999999]* 20
               Isigmas = [-9999999]*20
               Rmeas = [9999999]* 20
               CChalf = [-9999999]*20 
               
               return (9999999, -9999999,9999999,-9999999,-9999999,-9999999, 9999999,-9999999,-9999999,9999999,-9999999,-9999999,-9999999, -9999999, Rvalues,Isigmas,Rmeas,CChalf,unique_dir,newHKLfilenames,correlation_lookup)  
           if dataLine.search(item):
#               print "DATALINE"+item
               dataLineCount = dataLineCount+1
               matches = dataLine.search(item)
               res = matches.group(1)
               Rvalues.append( float(matches.group(6)))
               Isigmas.append( float(matches.group(9)))
               Rmeas.append( float(matches.group(10)))
               CChalf.append( float(matches.group(11)))

               CCAno =  float(matches.group(13))
               SigAno = float(matches.group(15))
               if (( CCAno >30 )  and (SigAno >1.0)):
                   MaxAnomRes = res

               if (dataLineCount ==1 ):
                   Isig_inner=float(matches.group(9))
                   Rvalue_inner = float(matches.group(6))
                   SigAno_inner= float(matches.group(15))
                   CCAno_inner = float(matches.group(13))

               if ( float(matches.group(6)) > cutoffR):
                   maxres_byR= res
                   maxres = res
#                   print "AAAA"
               if ( float(matches.group(9)) < cutoffI):
                    maxres_byIsig = res
                    maxres = res
#                    print "BBBB I"+matches.group(9)+" res "+str(res)
               if ( float(matches.group(11)) < cutoffCC):
                   maxres_byCC = res
                   maxres = res
#                   print "CCCC"
               if ( float(matches.group(5)) < cutoffCompl):
#                   print "DDDD "+str(cutoffCompl)+" MM "+matches.group(5) 
                   maxres = res
                   maxres_byCompl = res
               lastres = matches.group(1)    
           if maxres != 999:
               break
       for i in range(len(lines)):
           item = lines[i]
           if totalLine.search(item):
               matches = totalLine.search(item)
               res = maxres
               Rvalue_overall=matches.group(5)
               Isigma_overall=matches.group(8)
#               Anom_overall=matches.group(12)
#               Anom_overall=matches.group(14)
               Completeness_overall=matches.group(4)
               Rmeas_overall=matches.group(9)
               CChalf_overall=matches.group(10)
               SigAno_overall = matches.group(14)
               CCAno_overall = matches.group(12) 
               ObservedNref = matches.group(1)
               Uniqueref = matches.group(2)
#               print "UREf "+str(Uniqueref)
               if float(Uniqueref) != 0.0 :
#                   print "URE"+str(ObservedNref)+" "+str(Uniqueref)
                   multiplicity_overall = float(ObservedNref)/float(Uniqueref)
#                   print "URE mult "+str(multiplicity_overall)
               else:
                   multiplicity_overall = 0.0                             
       if maxres != 999:
            break
    os.chdir("../")
    if maxres == 999:
        maxres = lastres
#    return (float(maxres), float(Isig_inner),float(Rvalue_inner), float(SigAno_inner),multiplicity_overall, float(Rvalue_overall),float(Isigma_overall),float(Anom_overall),float(Completeness_overall), float(MaxAnomRes),Rvalues,Isigmas,unique_dir,newHKLfilenames)
#    print "UREZZ mult "+str(multiplicity_overall)
        
    return (float(maxres), float(Isig_inner),float(Rvalue_inner), float(SigAno_inner),float(CCAno_inner),multiplicity_overall, float(Rvalue_overall),float(Isigma_overall),float(CChalf_overall),float(Rmeas_overall),float(SigAno_overall),float(CCAno_overall),float(Completeness_overall), float(MaxAnomRes),Rvalues,Isigmas,Rmeas,CChalf,unique_dir,newHKLfilenames, correlation_lookup)   
###################################################################




#groups = 4   

cutoffI=2.0
cutoffR=9999.
cutoffCC=0.
cutoffCompl=-999.

SGN=1
cell = ""
xdsfiles = []
sirfiles1 = []
sirfiles2 = []

stream_object = type('', (), {}) 


if (crystfel_mode):
    print("Crystfel streamparsing mode")
    # make a list of pseudo files

    stream_object = Stream(crystfel_streamfile)
    print ("Crystfel getting chunk pointers")
    stream_object.get_chunk_pointers()

    # read the chunks, you want by providing list of chunk ids (i.e. a subset from the big list).

#    print("SO"+str(stream_object.codgas_lookup['begin_chunk']))
    number_of_chunks = len(stream_object.codgas_lookup['begin_chunk'])-1
    print ("Crystfel number of chunks "+str(number_of_chunks))
    

    xdsfiles = stream_object.codgas_lookup['begin_chunk']
    

    
elif ((find_files is not False) and (not sir1)): 
    (xdsfiles, lcount) = build_xdsfile_list(find_files,"./",0)
elif (sir1 and sir2):

    if (os.path.exists("sir_files1.txt") and os.path.exists("sir_files1.txt")):
        print("BOTH SIR files exist... reading in XDS files from config file ... \n");
        sirfiles1=[] # reset arrays
        sirfiles2=[]

        count = 0
        clines = open("sir_files1.txt").read().splitlines()
        for item in clines:
            if (count == 0 ):
                SGN=item
            elif (count == 1 ):
                cell=item
            else:    
#                print "FILE "+item
                sirfiles1.append(item)
            count = count + 1
        count = 0
        clines = open("sir_files2.txt").read().splitlines()
        for item in clines:
            if (count == 0 ):
                SGN=item
            elif (count == 1 ):
                cell=item
            else:    
#                print "FILE "+item
                sirfiles2.append(item)
            count = count + 1
    else:
        print("No config files exist -- building list from scratch")

#        print "SIR mode-- getting space group from first HKL file!!!!!"
        (sirfiles1,lcount) = build_xdsfile_list(find_files,sir1,0)
        (sirfiles2, lcount2) = build_xdsfile_list(find_files,sir2,lcount)

#        (SGN, cell) = cell_and_sg_from_XDS_ASCII (sirfiles1[0])

        print("Getting average cell parameters and most frequent SG...")
        (SGN,cell,newxdsfiles) = reflection_files_to_cell_and_sg (sirfiles1+sirfiles2,crystfel_mode)

        sirfiles1= [val for val in sirfiles1 if val in newxdsfiles]
        sirfiles2= [val for val in sirfiles2 if val in newxdsfiles]


        
        if (user_sgn):
            SGN = user_sgn
            
        write_config_file(sirfiles1,"sir_files1.txt",SGN,cell)
        write_config_file(sirfiles2,"sir_files2.txt",SGN,cell)

        print("SIR FILES ONE "+str(sirfiles1))
        print("SIR FILES TWO "+str(sirfiles2))
    
    
else:
    xdsfiles = glob.glob("*.hkl")
#    xdsfiles.append(glob.glob("*.hkl"))

               
if (((not os.path.exists(config_file)) or (correlation_cutoff is not None)) and (not sir1)):
    
    #################################################
    #  Build a list of XDS files  Need to do this globally because we need to know how many files there are

    # xdsfiles = build_xdsfile_list(find_files)

    ##########################################    
    # what SG?


    if (crystfel_mode):
        (cell, foundcell)=cell_and_sg_from_crystfel_stream (crystfel_streamfile)
    else:
        (SGN,cell,xdsfiles) = reflection_files_to_cell_and_sg(xdsfiles,crystfel_mode)


        
    # optionally trim based on CC
    if correlation_cutoff and not crystfel_mode:
        print("Making sure no datasets have poor correlations ...")
        (maxres_new1, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall,CCAno_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,cc_table)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
        xdsfiles = newfiles
        
    # write out the files
    print("Writing config file "+str(config_file))    
#    write_config_file(sirfiles1,"sir_files1.txt",SGN,cell)

    if (user_sgn):
        SGN = user_sgn
    write_config_file(xdsfiles,config_file, SGN, cell)

else:
    print("Reading in XDS files from config file ... \n");
#    sirfiles1=[] # reset arrays
#    sirfiles2=[]
    if (sir1):
        pass

    else:    
        xdsfiles=[]
#    # read from config file
        count = 0
        clines = open(config_file).read().splitlines()
        for item in clines:
            if (count == 0 ):
                SGN=item
            elif (count == 1 ):
                cell=item
            else:    
#                print "FILE "+item
                xdsfiles.append(item)
            count = count + 1
        if (count <= 0 ):
            print("ERROR:  no HKL files to merge!  Potential causes: incorrectly formatted weeded_files.txt file, or -f specified which does not match andy HKL files")

#############  End file setup
        

#creator.create("FitnessMulti", base.Fitness, weights=(-0.2,+0.7,+1.0))
#creator.create("FitnessMulti", base.Fitness, weights=(float(res_weight), float(isig_weight), float(anom_weight)))
creator.create("FitnessMulti", base.Fitness, weights=(1.0,))

creator.create("Individual", list, fitness=creator.FitnessMulti)


######################################## Make population
toolbox = base.Toolbox()
# Attribute generator


# Structure initializers


if (sir1):
    toolbox.register("attr_bool", random.randint, 1, groups*2+1)
    print("SIRFILES "+str(len(sirfiles1))+" SIRFILES2 "+str(len(sirfiles2)))
    print("SIRFILES "+str(sirfiles1)+" SIRFILES2 "+str(sirfiles2))
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, (len(sirfiles1)+len(sirfiles2)))
else:
    toolbox.register("attr_bool", random.randint, 1, groups)
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, len(xdsfiles))


# now make the population

toolbox.register("population", tools.initRepeat, list, toolbox.individual)


########################################  Operator registering
# check this out for explanations of probabilities
# http://www.academia.edu/1810452/DEAP_a_python_framework_for_evolutionary_algorithms
#
if (sir1):
#    toolbox.register("evaluate", evalSIR)
    toolbox.register("evaluate", partial(evalSIR,1))
    toolbox.register("mutate", tools.mutUniformInt,low=1,up=(groups*2),indpb=0.05)  # 2* groups here because groups 1..groups are native and groups+1..2*(groups) is derivative
else:
    SF = open(statfile,"w")
    SF.write("Fitness I_inner R_inner AnomCC_overall Sigano_overall CCHalf_overall CCHalf_outer\n")
    SF.close()         
    toolbox.register("evaluate", evalOneMax)
    toolbox.register("mutate", tools.mutUniformInt,low=1,up=groups,indpb=0.05)
toolbox.register("mate", tools.cxUniform,indpb=0.05)

#toolbox.register("mate", tools.cxUniform,indpb=float(cxprob))
#toolbox.register("mutate", tools.mutUniformInt,low=1,up=groups,indpb=float(mutprob))
#toolbox.register("mate", tools.cxUniform)
#toolbox.register("mate", tools.cxOnePoint)
#toolbox.register("mutate", tools.mutUniformInt,low=1,up=groups)

toolbox.register("select", tools.selTournament, tournsize=3)
#toolbox.register("select", tools.selRandom)

#toolbox.register("select", tools.selBest)
#toolbox.register("select", tools.selNSGA2)
toolbox.register("map", futures.map)

# http://stackoverflow.com/questions/20411847/solve-multi-objectives-optimization-of-a-graph-in-python
# However, since you want to do multi-objective, you would need a multi-objective selection operator, either NSGA2 or SPEA2. Finally, the algorithm would have to be mu + lambda. For both multi-objective selection and mu + lambda algorithm usage, see the GA Knapsack example.



def plotcolumn(datafile, columnName):


    # plot other stats
    #I_inner R_inner AnomCC_overall Sigano_overall Fitness
    cwd = str(os.getcwd())
    lfile = open(str(columnName)+".R", "w")
    pscript = """library(ggplot2)
mtcars <-read.table("%s", header = TRUE, sep = "")
attach(mtcars)
png("%s.png",width=8, height=16, units="in", res=72)
gen<- seq(1, length.out=nrow(mtcars), by=1)
p <- ggplot(mtcars, aes(gen, %s ))
p+geom_point(size=6)
"""  % (datafile, columnName, columnName)
    lfile.write(pscript)
    lfile.close()
    os.system("ssh debian7-test 'cd "+cwd+" ; /usr/bin/R --no-save <"+columnName+".R'")


def remove_het(inpdbfile,outpdbfile):
    if (os.path.exists(outpdbfile)):
        os.remove(outpdbfile)
    os.system("egrep -v 'HETATM|ANISOU' "+inpdbfile+" > "+outpdbfile)


def rundimple(inpdbfile,inmtzfile,ciffile):
    os.system("/opt/pxsoft/bin/dimple "+inpdbfile+" "+inmtzfile+" --libin "+ciffile+" --no-hetatm dmpl --fcol nat --sigf SIGnat --jelly 1 --restr-cycles 5")
    
    
def getphases(inpdbfile,outpdbfile,inmtzfile,outmtzfile,ciffile):
    RM = open("refmac.inp","w")
    if(os.path.exists(outpdbfile)):
        os.remove(outpdbfile)
    cifstring = ""
    if (ciffile):
        cifstring = " LIBIN "+ciffile
    print("making refmac input")    
    RM.write(refmac5+" hklin "+inmtzfile+" hklout "+outmtzfile+" xyzin "+inpdbfile+ " xyzout "+outpdbfile+cifstring+" <<EEE >refmaclog.txt\n")
    RM.write("NCYC "+str(refmac_cycles)+"\n")
    RM.write("END\n")
    RM.write("EEE\n")
    RM.close()
#    os.chmod("refmac.inp",stat.S_IEXEC)
    os.system("chmod 755 refmac.inp")
    print("running refmac input")
    os.system("./refmac.inp")

    
def parsebloblog(logfile):
    #blobout = "blobs.txt"

    BL = open(logfile)
    lines = BL.readlines()
    #0    3751 grid points, score 2083    (  10.76,   6.80, -11.81)
    griditem  = re.compile('(\d+) grid points\, score (\S+)\s+\(\s+(\S+),\s+(\S+),\s+(\S+)\)')
    blob_score = 0.0
    
    for i in range(len(lines)):
        item = lines[i]
        if griditem.search(item):
            BScore=0.0
            x_pos=0.0
            y_pos=0.0
            z_pos=0.0
            
            matches = griditem.search(item)
            BS = matches.group(2)
            x = matches.group(3)
            y = matches.group(4)
            z = matches.group(5)
            try:
                BScore = float(BS.strip())
            except ValueError:
                print("Error extracting blob score")
            try:
                x_pos = float(x.strip())
            except ValueError:
                print("Error extracting X")
            try:
                y_pos = float(y.strip())
            except ValueError:
                print("Error extracting Y")
            try:
                z_pos = float(z.strip())
            except ValueError:
                print("Error extracting Z")
            ########    ZZZ
            if (blob_position):
                a = (x_pos, y_pos,z_pos)
                b = (blob_x, blob_y, blob_z)

                dst = distance.euclidean(a, b)
                print("dist "+str(dst)+" blob XYZ "+str(x_pos)+" "+str(y_pos)+" "+str(z_pos)+" user XYZ "+str(blob_x) +" "+str(blob_y) +" "+str(blob_z))
                if (dst < blob_distance):
                    blob_score = BScore
                    break
            else:
                blob_score = BScore
                break
    return(blob_score)                 

def main():
    if (os.path.exists("old_codgas_results")):
        old_dir = "old_codgas_results_"+str(uuid.uuid4())
        os.system("mv -f old_codgas_results "+old_dir)
    os.mkdir("old_codgas_results")
    os.system("mv -f best_solution_* old_codgas_results")


    (options, args) = parser.parse_args()
    hall_of_fame_number =int(options.hall_of_fame_number)
    r_weight = float(str(options.rweight))
    isig_weight = float(str(options.isigweight))
    anom_weight = float(str(options.anomweight))
    multiplicity_weight = options.multiplicityweight
    completeness_weight = float(str(options.completeness_weight))
    cchalf_weight = float(str(options.cchalf_weight))
    blob_weight = float(str(options.blob_weight))
    print("___________ "+str(cchalf_weight))

    if (anom_on):
        if (anom_weight == 0 ):
            print("WARNING -- setting anomalous weight to 1 since it looks like you want to optimise anomalous signal")
            anom_weight=1
    else:    
        anom_weight = 0.0

    print("Saving constants CChalf "+str(cchalf_weight)+" Isig "+str(isig_weight))


    shared.setConst(Ublob_weight =blob_weight) 
    shared.setConst(Uisig_weight=isig_weight)
    shared.setConst(Uanom_weight=anom_weight)
    shared.setConst(Ucchalf_weight=cchalf_weight)
    shared.setConst(Ucompleteness_weight=completeness_weight)
    shared.setConst(Umultiplicity_weight=multiplicity_weight)

    print(" recall "+str(shared.getConst('Ucchalf_weight')))

    
    population_size = options.population_size
#objective = options.objective
    generations = options.generations
    groups = options.groups

    correlation_cutoff = options.correlation_cutoff
    mutprob = options.mprob_user
    cxprob = options.cxprob_user
    equilibrate_scales_to_R = options.equilibrate_scales_to_R



    
    sir_no_overlap = options.sir_no_overlap
    sir_res_limit = options.sir_res_limit
    sir_individual_stat_weight = options.sir_individual_stat_weight
    sir_iso_weight = options.sir_iso_weight

    if (generations is None):
        generations = 25
        
#    if (not anom_weight):
#        if (anom_on):
#            anom_weight=1
#        else:    
#            anom_weight = 0.0

    if (not population_size):
        population_size = 20
    if (not groups):
        groups = 3
    if mutprob is None:
        mutprob = 0.6
    if cxprob is None:
        cxprob = 0.3


    print("Weighting is "+weighting_scheme+ ". Available schemes are: one_over_x, sigmoid and linear")


    if ((float(mutprob)+float(cxprob)) >1 ) :
        print("Mutation probability + Crossover probability cannot exceed 1")
        os._exit(0)

#    if (correlation_cutoff ):
#        if os.path.isfile(config_file):
#            os.remove(config_file)



#    shared.setConst('isig_weight',isig_weight)
#    shared.setConst('anom_weight',anom_weight)
#    shared.setConst('cchalf_weight',cchalf_weight)
#    shared.setConst('completeness_weight',completeness_weight)

    startTime = time.time()
    random.seed()
    print("Fitness weights:  R-value ()"+str(r_weight)+" ISIG "+str(isig_weight)+" Anom "+str(anom_weight))
    print(str(groups)+" Groups")
    print(str(len(xdsfiles))+" Datasets")
    print("SG number is "+str(SGN)+" and Unit cell is "+cell)
    print("XDS files "+str(xdsfiles))
    print("Population size is "+str(population_size))
    pop = toolbox.population(n=int(population_size))
    hof = tools.HallOfFame(hall_of_fame_number)
    # http://deap.readthedocs.org/en/latest/tutorials/basic/part3.html
    stats = tools.Statistics(lambda ind: ind.fitness.values)






    history = tools.History()
    toolbox.decorate("mate", history.decorator)
    toolbox.decorate("mutate", history.decorator)

    history.update(pop)

    
    # pop = toolbox.population(n=MU)
    # hof = tools.ParetoFront()
    # stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

#    pop, log = algorithms.eaMuPlusLambda(pop, toolbox, MU, LAMBDA, CXPB, MUTPB, NGEN, stats,
#                              halloffame=hof)


    print("Mutation probability: "+str(mutprob))
    print("Crossover probability: "+str(cxprob))


    #########################################################
    
    if equilibrate_scales_to_R is not None:
        print(" Doing a one cycle pre-run to get scales for anomalous and I.  Note that if you have provided any scales for these targets, they will be applied *after* CODGAS scales to R. ")
        try:
            float(equilibrate_scales_to_R)
            print("    You have specified an R of "+equilibrate_scales_to_R+" as the target")
        except:
            equilibrate_scales_to_R=5  # use 5% as a reaosonable default
            print("    No R target specified -- setting to 5 percent")



        # re-adjust scales so that starting value of I/sigI and Anom are roughly on the same scale as R
        stats2 = tools.Statistics(lambda ind: ind.fitness.values)
        stats2.register("avg", numpy.mean)
        stats2.register("std", numpy.std)
        stats2.register("min", numpy.min)
        stats2.register("max", numpy.max)

        pop = toolbox.population(n=int(population_size))

        print("****************** Starting pre-optimisation ******************")
        pop2, log = algorithms.eaSimple(pop, toolbox, cxpb=float(cxprob), mutpb=float(mutprob), ngen=1, stats=stats2, halloffame=hof, verbose=True)
        print("****************** Done with pre-optimisation ******************")

        bests= tools.selBest(pop2, k=1)

        count = 0
        best_I = 0
        best_A = 0
        best_R = 999
        best_CChalf = 0

        for x in bests:

            count = count +1  
            dirs=[]
            for group in range(1,int(groups)+1):

                HKLfilenames=[]
                groupstr=""
                for dataset in range(0,len(xdsfiles)-1):
                    if (x[dataset] == group):
                        HKLfilenames.append(xdsfiles[dataset])
#                print HKLfilenames

                (maxres, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall,CCAno_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,corr_lookup)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)

                #

                CChalf_outer = CChalf[len(CChalf)-1]
#                
#                print "            Ano mode"+ano_mode+" SigAno_inner "+str(SigAno_inner)
                A_term=0



                A_value = 0
                if re.search("Resolution",ano_mode):
                    A_value = MaxAnomRes
                elif re.search("SigAno_inner",ano_mode):
                    A_value= SigAno_inner
                elif re.search("SigAno_overall",ano_mode):
                    A_value = SigAno_overall
                elif re.search("AnoCC_inner",ano_mode):
                    A_value = CCAno_inner
                elif re.search("AnoCC_overall",ano_mode):
                    A_value =CCAno_overall
                else:
                    A_value =SigAno_overall

                A_term = A_value *float(anom_weight)
#                print "   A-term is "+str(A_term)
        
#                print "Anom weight "+str(anom_weight)
#                if re.search("Resolution",ano_mode):
#                    A_term = MaxAnomRes
#                    A_term = MaxAnomRes*float(anom_weight)

#                else:
#                    A_term = SigAno_inner *float(anom_weight)
#                    A_term = SigAno_inner *float(anom_weight)            
#                    print "   A-term is "+str(A_term)


                CC_value=0.0
                if (CChalf_mode == "overall") :
                    CC_value= CChalf_overall
                else:
                    CC_value = CChalf_outer

                I_value = 0.0    
                if (I_sigma_mode == "overall"):
                    I_value = Isigma_overall
                else:
                    I_value = Isig_inner

 #               print "dddddd"
                    
                if (I_value >best_I):
                    best_I = I_value
                if (CC_value >best_CChalf):
                    best_CChalf = CC_value
                if (A_value >best_A):
                    best_A = A_value
                if (Rvalue_inner < best_R):
                    best_R = Rvalue_inner

        print("ggg")
        R_term = score_Rvalue(float(equilibrate_scales_to_R),weighting_scheme)
        Real_R_term = score_Rvalue(float(best_R),weighting_scheme)
        I_term = best_I
        A_term = best_A
        CC_term = best_CChalf

        if R_term != 0.0:
             print(" 1 cycle best I term "+str(I_term))
             print(" 1 cycle best A term "+str(A_term))
             print(" 1 cycle best CC term "+str(CC_term))


             if (equilibration_mode == "initial"):
                 new_I_weight = Real_R_term/I_term
                 new_Ano_weight = 1.0
                 if (A_term != 0) :
                     new_Ano_weight = Real_R_term/A_term
                 new_CChalf_weight = Real_R_term/CC_term
                 new_Completeness_weight = Real_R_term /100
                 print("Weight to scale <I/sigI> to R is "+str(new_I_weight))
                 print("Weight to scale Ano to R is "+str(new_Ano_weight))
                 print("Weight to scale CC half to R is "+str(new_CChalf_weight))
                 print("Weight to scale Completeness to R is "+str(new_Completeness_weight))
                 isig_weight=float(new_I_weight)*float(isig_weight)
                 anom_weight=float(new_Ano_weight)*float(anom_weight)
                 cchalf_weight=float(new_CChalf_weight)*float(cchalf_weight)
                 completeness_weight=float(new_Completeness_weight)*float(completeness_weight)
                 print("New <I/sigI> weight WITH user supplied modifier is "+str(isig_weight))
                 print("New Ano weight WITH user supplied modifier is "+str(anom_weight))
                 print("New CChalf weight WITH user supplied modifier is "+str(cchalf_weight))
                 print("New Completeness weight WITH user supplied modifier is "+str(completeness_weight))
                 
             else:
                 print("Desired R value is "+str(equilibrate_scales_to_R)+" which corresponds to a score of "+str(R_term)+" during the pre-run")
                 print("  Will scale I weight, CChalf and Anom weight to this value")

                 new_I_weight = R_term/I_term
                 new_Ano_weight = R_term/A_term
                 new_CChalf_weight = R_term/CC_term
                 new_Completeness_weight = R_term/100 
                 print("Weight to scale <I/sigI> to R is "+str(new_I_weight))
                 print("Weight to scale Ano to R is "+str(new_Ano_weight))
                 print("Weight to scale CC half to R is "+str(new_CChalf_weight))
                 print("Weight to scale Completeness to R is "+str(new_Completeness_weight))
                 isig_weight=float(new_I_weight)*float(isig_weight)
                 anom_weight=float(new_Ano_weight)*float(anom_weight)
                 cchalf_weight=float(new_CChalf_weight)*float(cchalf_weight)
                 completeness_weight=float(new_Completeness_weight)*float(completeness_weight)
                 print("New <I/sigI> weight WITH user supplied modifier is "+str(isig_weight))
                 print("New Ano weight WITH user supplied modifier is "+str(anom_weight))
                 print("New CChalf weight WITH user supplied modifier is "+str(cchalf_weight))
                 print("New Completeness weight WITH user supplied modifier is "+str(completeness_weight))



        else:
             print("Something strange happened: setting weights to 1")
             isig_weight=1
             anom_weight=1
             cchalf_weight=1
        SF = open(statfile,"w")
        SF.write("Fitness I_inner R_inner AnomCC_overall Sigano_overall CCHalf_overall CCHalf_outer\n")
        SF.close()                      


    #########################################################
    # set weights
    shared.setConst(isig_weight=isig_weight)
    shared.setConst(anom_weight=anom_weight)
    shared.setConst(cchalf_weight=cchalf_weight)
    shared.setConst(completeness_weight=completeness_weight)

    weights = open("weights.txt","w")
    weights.write("ISIG_WEIGHT "+str(isig_weight)+"\nANOM_WEIGHT "+str(anom_weight)+"\nCCHALF_WEIGHT "+str(cchalf_weight)+ "\nCOMPLETENESS_WEIGHT "+str(completeness_weight)+"\nRMEAS_WEIGHT "+str(r_weight)+"\nSIR_INDIV_WEIGHT "+str(sir_individual_stat_weight)+"\nSIR_ISO_WEIGHT "+str(sir_iso_weight)+"\nMULTIPLICITY_WEIGHT "+str(multiplicity_weight)+"\n") 
    weights.close()



    
    #: The sum of the crossover and mutation probabilities must be smaller or equal to 1.0.

    generations = int(generations)         
#    print " <I/sigI> weight WITH user supplied modifier is "+str(isig_weight)
 #   print " Ano weight WITH user supplied modifier is "+str(anom_weight)
 #   print " CChalf weight WITH user supplied modifier is "+str(cchalf_weight)
 #   print " Completeness weight WITH user supplied modifier is "+str(completeness_weight)


    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=float(cxprob), mutpb=float(mutprob), ngen=generations, 
                                   stats=stats, halloffame=hof, verbose=True)
    print("foo");    
    ##$$$$$$$

    # fitnesses = list(map(toolbox.evaluate, pop))
    # for ind, fit in zip(pop, fitnesses):
    #     ind.fitness.values = fit
    
    # print("  Evaluated %i individuals" % len(pop))
    
    # CXPB, MUTPB = float(cxprob), float(mutprob)

    # # Begin the evolution
    # for g in range(generations):
    #     print("-- Generation %i --" % g)
        
    #     # Select the next generation individuals
    #     offspring = toolbox.select(pop, len(pop))
    #     # Clone the selected individuals
    #     offspring = list(map(toolbox.clone, offspring))
    
    #     # Apply crossover and mutation on the offspring
    #     for child1, child2 in zip(offspring[::2], offspring[1::2]):
    #         if random.random() < CXPB:
    #             toolbox.mate(child1, child2)
    #             del child1.fitness.values
    #             del child2.fitness.values

    #     for mutant in offspring:
    #         if random.random() < MUTPB:
    #             toolbox.mutate(mutant)
    #             del mutant.fitness.values
    
    #     # Evaluate the individuals with an invalid fitness
    #     invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
    #     fitnesses = map(toolbox.evaluate, invalid_ind)
    #     for ind, fit in zip(invalid_ind, fitnesses):
    #         ind.fitness.values = fit
        
    #     print("  Evaluated %i individuals" % len(invalid_ind))
        
    #     # The population is entirely replaced by the offspring
    #     pop[:] = offspring
        
    #     # Gather all the fitnesses in one list and print the stats
    #     fits = [ind.fitness.values[0] for ind in pop]
        
    #     length = len(pop)
    #     mean = sum(fits) / length
    #     sum2 = sum(x*x for x in fits)
    #     std = abs(sum2 / length - mean**2)**0.5
        
    #     print("  Min %s" % min(fits))
    #     print("  Max %s" % max(fits))
    #     print("  Avg %s" % mean)
    #     print("  Std %s" % std)







    ##$$$$$$$
    #:param population: A list of individuals.
    #:param toolbox: A :class:deap.base.Toolbox
    #:param cxpb: The probability of mating two individuals.
    #:param mutpb: The probability of mutating an individual.
    #:param ngen: The number of generation.
    #:param stats


#    bestests = tools.selBest(pop, k=3)
    bestests = hof
    
    count = 0

    lfile = open("log.txt", "w")
    lfile.write(" ".join(sys.argv)+"\n\n")
    lfile.write("\n".join(sys.argv)+"\n\n")

    lfile.write(str(log)+"\n")

    
    statlog = open("statlog.txt", "w")
    statlog.write(str(log))
    statlog.close()
    
    print(log)
                                                                                                                                                                                                                   
                                                                                                                                                                                                                   
    startdir =  os.getcwd()                                                                                                                                                                                
    for x in bestests:
      os.chdir(startdir)
      count = count +1  
      print(str(count)+"<----------------------------")
      lfile.write(str(count)+"--------------------------\n")
      dirs=[]
      venngroups=[]
      venngroupnames=[]

      if (sir1):
          print("ABC sir outputting ...\n");
          evalSIR(0,x)

          target_dir = "best_sir_"+str(count)
#          dirs = [d for d in os.listdir('.') if os.path.isdir(d)]
#          sorted_dirs = sorted(dirs, key=lambda x: os.path.getctime(x), reverse=True)
#          dc = 1
#          for dd in sorted_dirs[0:2]:
#              if re.search("tmpXSCALE",dd):
#                  shutil.copytree (dd,target_dir+"_xscale_"+str(dc))                   
#                  dc = dc +1
#              if re.search("tmpSHELX",dd):
#                  shutil.copytree (dd,target_dir+"_shelx_")                   

      for group in range(1,int(groups)+1):

           st = "Solution "+str(count)+" group "+str(group)
           HKLfilenames=[]
           HKLindices=[]
           groupstr=""
           if (sir1):
               pass
#               for dataset in range(0,len(sirfiles1)-1):
#                   if (x[dataset] == group):
#                       HKLfilenames.append(sirfiles1[dataset])
#2B                       (maxres_new1, Isig_inner, Rvalue_inner, Sigano_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,Anom_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas, resultdir, newfiles)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)

#               for dataset in range(0,len(sirfiles2)-1):
#                   if (x[dataset] == group):
#                       HKLfilenames.append(sirfiles1[dataset])
#2B                       (maxres_new1, Isig_inner, Rvalue_inner, Sigano_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,Anom_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas, resultdir, newfiles)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)



           else:
               for dataset in range(0,len(xdsfiles)-1):
                   if (x[dataset] == group):
                       HKLfilenames.append(xdsfiles[dataset])
                       HKLindices.append(dataset)


           print(HKLfilenames)
           lfile.write(st+"\n"+str(HKLfilenames))
           lfile.write("\n")
           maxres_new1 = 0.0
           maxres_new2 = 0.0
           maxres = 0.0
           Isig_inner =0.0
           Rvalue_inner =0.0
           SigAno_overall =0.0
           Completeness_overall =0.0
           resultdir = ""
           
           if (crystfel_mode):
#               (CCAno_inner,Isig_inner,Rsplit_inner,maxres,resultdir) = filenames_to_partialator(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
#               (CCAno_inner,resultdir) = filenames_to_partialator(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)

               (CCAno_inner,CCAno_overall,Isig_inner,Isigma_overall,Rvalue_inner,Rvalue_overall,maxres,resultdir) = filenames_to_partialator(HKLindices,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)
           else:
               (maxres_new1, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall, CCAno_overall, CCompleteness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,cc_table)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,0,0)

               print("RES cut 1 is "+str(maxres_new1))
               (maxres_new2, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall, CCAno_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,cc_table)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,maxres_new1,0)
               print("RES cut 2 is "+str(maxres_new2))
               (maxres, Isig_inner, Rvalue_inner, SigAno_inner, CCAno_inner, multiplicity_overall, Rvalue_overall,Isigma_overall,CChalf_overall,Rmeas_overall,SigAno_overall, CCAno_overall, Completeness_overall, MaxAnomRes,Rvalues,Isigmas,Rmeas,CChalf, resultdir, newfiles,cc_table)    =  filenames_to_xscale(HKLfilenames,SGN,cell, cutoffI, cutoffR, cutoffCC, cutoffCompl,maxres_new2,0)

           dirs.append(resultdir)    
           print("RES: "+str(maxres))
           print("Isig_inner "+str(Isig_inner))
           print("R inner "+str(Rvalue_inner))
           print("SigAnomalous "+str(SigAno_overall))
           print("Complete overall "+str(Completeness_overall))


           lfile.write("Max resolution: "+str(maxres)+"\n")
           lfile.write("Isig_inner "+str(Isig_inner)+"\n")
           lfile.write("R inner "+str(Rvalue_inner)+"\n")
           lfile.write("Complete overall "+str(Completeness_overall)+"\n")
           print("SAD PHASE "+str(sad_phase))

      lfile.write("--------------------------\n")
      print("-----------------------------")


# check to make sure at least one best_shelx
 #           print "Best shelx dataset does not exist -- this usually is only possible if you have set the minimum R value too optimistically. Try again with a higher -q level " +best_dir
 #           os.system("touch WARNING_NO_BEST_SHELX_DIR_Q_PARAM_TOO_HIGH")
      # now rename directories 
      print("Cleaning up...")
      for y in range(0,len(dirs)):
          target_dir = "best_solution_"+str(count)+"_group_"+str(y+1)
          shutil.rmtree(target_dir, ignore_errors=True)  # remove old versions of this dir
          if os.path.exists(dirs[y]):
              shutil.copytree (dirs[y],target_dir) 

          if os.path.exists(target_dir):    
              argfile = open(target_dir+"/args.txt", "w")
              if (argfile):
                  argfile.write(" ".join(sys.argv)+"\n\n")
                  argfile.close()



          if (sad_phase):
              print("Sad phasing ON")
              orig_dir = os.getcwd()
              os.chdir(target_dir)    
              os.system("pwd")
              additional_parameters = ""
              if (sad_reference is not None):
#                  additional_parameters = " --refine_ref --reference="+sad_reference+" --just_anode "
                  additional_parameters = " --refine_ref --reference="+sad_reference
              
              execstring =  "/opt/pxsoft/bin/simple_sad.pl -in merged.hkl -cell=\""+str(cell)+"\" -sg "+str(SGN)+" --nsites 35 --crank2 --autosharp -nocc 1"+additional_parameters+" &"
              print("SAD exec string "+str(execstring)+" in "+str(target_dir))
              os.system(execstring)
              os.chdir(orig_dir)    
          else:
              print("Sad phasing not selected")
              SS = open("sadstring.com","w")
              additional_parameters = ""
              if (sad_reference is not None):
                  additional_parameters = " --refine_ref --reference="+sad_reference
              execstring =  "/opt/pxsoft/bin/simple_sad.pl -in merged.hkl -cell=\""+str(cell)+"\" -sg "+str(SGN)+" --nsites 35 --autosharp -nocc 1"+additional_parameters
              SS.write(execstring+"\n")
              SS.close()
#      plt.figure(figsize=(4,4))
#      venn3(set(venngroups[0]), set(venngroups[1]), set(venngroups[2]),set_labels=venngroupnames)
#      plt.show()

    #################################  
    r = glob.glob("tmpXSCALE*")
    for i in r:
#        pass
        if (not sir1):
#            shutil.rmtree(str(i), ignore_errors=True)
            print("Deleting...")




    ###################### ggplot
    cwd = str(os.getcwd())
    lfile = open("codgas.R", "w")

    pscript = """library(ggplot2)
mtcars <-read.table("statlog.txt", header = TRUE, sep = "")
attach(mtcars)
png("stats.png",width=8, height=16, units="in", res=72)
p <- ggplot(mtcars, aes(gen, max ))
p <- p + geom_point(color="red", size=3,aes(gen,min))
p <- p + geom_point(color="black", size=3,aes(gen,avg))
p + geom_line(color="blue", size=3)
""" 


    lfile.write(pscript)

#    lfile.close()
    os.system("ssh debian7-test 'cd "+cwd+" ; /usr/bin/R --no-save <codgas.R'")
    print("ssh debian7-test 'cd "+cwd+" ; /usr/bin/R --no-save <codgas.R'")


#     # plot other stats
#     # I_inner R_inner AnomCC_overall Sigano_overall Fitness

#     plotcolumn(statfile,"I_inner")
#     plotcolumn(statfile,"R_inner")
#     plotcolumn(statfile,"AnomCC_overall")
#     plotcolumn(statfile,"Sigano_overall")
#     plotcolumn(statfile,"Fitness")







#     import matplotlib.pyplot as plt

#     gen = log.select("gen")
#     fit_maxs = numpy.array(log.chapters["fitness"].select("max"))
#     fit_avgs = numpy.array(log.chapters["fitness"].select("avg"))

#     fig, ax1 = plt.subplots()
#     line1 = ax1.plot(gen, fit_maxs, "b-", label="Max Fitness")
#     ax1.set_xlabel("Generation")
#     ax1.set_ylabel("Fitness", color="b")
#     for tl in ax1.get_yticklabels():
#         tl.set_color("b")

# #    ax2 = ax1.twinx()
# #    line2 = ax2.plot(gen, fir_avgs, "r-", label="Average Fitness")
# #    ax2.set_ylabel("Size", color="r")
# #    for tl in ax2.get_yticklabels():
# #        tl.set_color("r")

# #    lns = line1 + line2
#     lns = line1 
#     labs = [l.get_label() for l in lns]
#     ax1.legend(lns, labs, loc="center right")

# #    plt.show()
#     savefig('foo.png')


#    print "Plotting ..."

#    graph = networkx.DiGraph(history.genealogy_tree)
#    graph = graph.reverse()     # Make the grah top-down
#    colors = [toolbox.evaluate(history.genealogy_history[i])[0] for i in graph]
#    networkx.draw(graph, node_color=colors)
#    plt.savefig('codgas_history')
    endTime = time.time()
    print("Total real run time "+str(endTime-startTime))
    lfile.write("Total real run time "+str(endTime-startTime))
    lfile.close()

if __name__ == "__main__":
    main()








