#
# Copyright (c) European Molecular Biology Laboratory (EMBL)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = ['S. Basu']
__license__ = 'MIT'
__date__ = '2019/03/27'


import sys

from workflow_lib import ispyb
from workflow_lib import common
from workflow_lib import collect
from workflow_lib import edna_mxv1
from workflow_lib import collect_and_ispyb
from workflow_lib import workflow_logging, path
from autoCryst import crystfel_utils

def run(beamline, workflowParameters, directory, meshPositionFile, run_number, expTypePrefix, workflow_working_dir,
        prefix, suffix, workflow_id, grid_info, workflow_title, workflow_type,
        data_threshold, data_collection_max_positions, radius, list_node_id=None, firstImagePath=None,
        pyarch_html_dir=None, diffractionSignalDetection="Dozor", resultMeshPath=None, token=None, **kwargs):

    crystfel_result = {}
    try:
        sys.path.insert(0, '/opt/pxsoft/ccp4/v7.0/linux-x86_64/ccp4-7.0/lib/py2/cctbx/lib')
        # sys.path.insert(1, '/usr/local/BES/id30a2/workflow/autoCryst/src')
        logger = workflow_logging.getLogger(beamline)
        logger.info('PYTHONPATH:{}'.format(sys.path))
        # from autoCryst import crystfel_utils
        args = {}
        args['beamline'] = beamline
        # args['indexing_method'] = 'xgandalf'
        # suffix = 'cbf' or 'h5'
        prefix = expTypePrefix + prefix
        status, crystfel_result = crystfel_utils.__run__(directory, prefix, suffix, **args)
        if status:
           logger.info('crystFEL:{}'.format(crystfel_result))
        else:
           logger.info('crystFEL:{}'.format('crystfel pipeline has errors'))

    except Exception as err:
        common.logStackTrace(workflowParameters)
        raise err

    return crystfel_result

def check():
    try:
        sys.path.insert(0, '/opt/pxsoft/ccp4/v7.0/linux-x86_64/ccp4-7.0/lib/py2/cctbx/lib')
        sys.path.insert(1, '/usr/local/BES/id30a2/workflow/autoCryst/src')
        print 'PYTHONPATH:{}'.format(sys.path)
        import crystfel_utils
    except Exception as err:
        raise err

if __name__ == '__main__':
   check()
